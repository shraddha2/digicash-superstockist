package com.digicash.superstockist.ViewFundRequest;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

/**
 * Created by shraddha on 18-06-2018.
 */

public class MyFundRequestAdapter extends RecyclerView.Adapter<MyFundRequestAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3,txt_label4,txtTransactionRefId,txtRemark,txt_label5,txt_label6,txt_label7;

        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txtTransactionRefId = (TextView) view.findViewById(R.id.txtTransactionRefId);
            txtRemark = (TextView) view.findViewById(R.id.txtRemark);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txt_label6 = (TextView) view.findViewById(R.id.txt_label6);
            txt_label7 = (TextView) view.findViewById(R.id.txt_label7);

        }
    }

    public MyFundRequestAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_fund_request_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_label1.setText(movie.getLabel1()+"");
        holder.txt_label2.setText(movie.getLabel2()+"");
        holder.txt_label3.setText(movie.getLabel3()+"");
        holder.txt_label4.setText(movie.getLabel4()+"");
        holder.txtTransactionRefId.setText(movie.getLabel5());
        holder.txtRemark.setText(movie.getLabel6());
        holder.txt_label5.setText(movie.getLabel7()+"");
        holder.txt_label6.setText(movie.getLabel8()+"");
        holder.txt_label7.setText(movie.getLabel9()+"");

        if (movie.getLabel9().equalsIgnoreCase("declined")) {
            holder.txt_label3.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
            holder.txt_label7.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
        } else if (movie.getLabel9().equalsIgnoreCase("pending")){
            holder.txt_label3.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
            holder.txt_label7.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
        }else{
            holder.txt_label3.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
            holder.txt_label7.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
