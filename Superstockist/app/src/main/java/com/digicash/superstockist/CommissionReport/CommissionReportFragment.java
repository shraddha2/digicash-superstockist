package com.digicash.superstockist.CommissionReport;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Model.DividerItemDecoration;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.IllegalFormatConversionException;
import java.util.List;

public class CommissionReportFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener, View.OnClickListener  {
    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CommissionReportAdapter mAdapter;

    TextView txt_label2,txt_label3,txt_label4,txt_label5,txt_label6,txt_label7,txt_from_date,txt_to_date;
    DateFormat inputFormat,outputFormat;
    String start_date,end_date, token, request_type="recharge";;
    SharedPreferences sharedpreferences;
    ImageView btn_search;

    ConstraintLayout progress_linear,linear_container,rel_img_bg,rel_no_records,rel_no_internet;
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg;

    private Button[] btn = new Button[3];
    private Button btn_unfocus;
    private int[] btn_id = {R.id.btn0, R.id.btn1, R.id.btn2};

    public CommissionReportFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_commission_report, container, false);
        //String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle("DijiCash");

        for(int i = 0; i < btn.length; i++){
            btn[i] = (Button) rootView.findViewById(btn_id[i]);
            btn[i].setBackgroundColor(Color.rgb(255,255,255));
            btn[i].setOnClickListener(this);
        }

        btn_unfocus = btn[0];

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_commission_chart);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2= (TextView) rootView.findViewById(R.id.txt_label2);
        txt_label3= (TextView) rootView.findViewById(R.id.txt_label3);
        txt_label4= (TextView) rootView.findViewById(R.id.txt_label4);
        txt_label5= (TextView) rootView.findViewById(R.id.txt_label5);
        txt_label6= (TextView) rootView.findViewById(R.id.txt_label6);
        txt_label7= (TextView) rootView.findViewById(R.id.txt_label7);
        txt_label6= (TextView) rootView.findViewById(R.id.txt_label6);
        txt_label7= (TextView) rootView.findViewById(R.id.txt_label7);
        txt_from_date= (TextView) rootView.findViewById(R.id.txt_from_date);
        txt_to_date= (TextView) rootView.findViewById(R.id.txt_to_date);
        btn_search= (ImageView) rootView.findViewById(R.id.btn_search);

        inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        outputFormat = new SimpleDateFormat("dd MMM yyyy");

        txt_to_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        start_date=DefineData.getCurrentDate();
        end_date=start_date;
        mAdapter = new CommissionReportAdapter(movieList,getActivity());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_img_bg= (ConstraintLayout) rootView.findViewById(R.id.rel_img_bg);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        rel_img_bg.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        txt_to_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        start_date=DefineData.getCurrentDate();
        end_date=start_date;

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_date=DefineData.parseDateToyyyMMdd(txt_from_date.getText().toString());
                end_date=DefineData.parseDateToyyyMMdd(txt_to_date.getText().toString());
                if(checkConnection()) {
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.GONE);
                    rel_img_bg.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.GONE);
                    new FetchCommisionsReport().execute();
                }else{
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.GONE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.VISIBLE);
                }

            }
        });

        txt_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate= Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),R.style.MyCalendarStyle,new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;

                            /*from_date = inputFormat.parse(inputDateStr);
                            String outputDateStr = outputFormat.format(from_date);*/
                        txt_from_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));


                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("Start Date");
                mDatePicker.show();  }


        });


        txt_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate= Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),R.style.MyCalendarStyle,new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;

                           /* to_date = inputFormat.parse(inputDateStr);
                            String outputDateStr = outputFormat.format(to_date);*/
                        txt_to_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));


                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("End Date");
                mDatePicker.show();  }


        });

        setFocus(btn_unfocus, btn[0]);
        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.VISIBLE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            movieList.clear();
            new FetchCommisionsReport().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    @Override
    public void onClick(View v) {
        //setForcus(btn_unfocus, (Button) findViewById(v.getId()));
        //Or use switch
        switch (v.getId()){
            case R.id.btn0 :
                request_type="recharge";
                setFocus(btn_unfocus, btn[0]);
                //txt_label2.setText("Operator");
                txt_label2.setText("Operator");
                txt_label4.setText("Commission(₹)");
                break;

            case R.id.btn1 :
                request_type="mt";
                setFocus(btn_unfocus, btn[1]);
                //txt_label2.setText("Name");
                txt_label2.setText("Total Count");
                txt_label4.setText("Profit(₹)");
                break;

            case R.id.btn2 :
                request_type="bill-payment";
                setFocus(btn_unfocus, btn[2]);
                txt_label2.setText("Operator");
                txt_label4.setText("Commission(₹)");
                break;

        }

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchCommisionsReport().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }
    }

    private void setFocus(Button btn_unfocus, Button btn_focus){
        btn_unfocus.setTextColor(Color.rgb(49, 50, 51));
        btn_focus.setTextColor(Color.rgb(255,255,255));
        btn_unfocus.setBackgroundColor(Color.rgb(255,255,255));
        btn_focus.setBackgroundColor(Color.rgb(242, 109, 109));
        this.btn_unfocus = btn_focus;
    }

    private class FetchCommisionsReport extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);


        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("startDate", start_date);
                parameters.put("endDate", end_date);
                parameters.put("type", request_type);
                this.response = new JSONObject(service.POST(DefineData.FETCH_COMMISIONS_REPORT,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = response.getJSONArray("data");
                        JSONObject jsonObject = response.getJSONObject("totals");
                        String total_recharge_amt = jsonObject.getString("sales");
                        Double recharge_commission_total = jsonObject.getDouble("commission");
                        txt_label6.setText("₹ "+total_recharge_amt);
                        if(jsonArray.length()!=0) {
                            //String total_recharge_amt = response.getString("sales");
                            // Double recharge_commission_total = response.getDouble("commission");
                            // txt_label6.setText("₹ "+total_recharge_amt);
                            try {
                                txt_label7.setText("₹ " + String.format( "%.2f", recharge_commission_total ));
                            }catch (IllegalFormatConversionException e)
                            {
                                txt_label7.setText(" "+recharge_commission_total);
                            }
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    //Getting json
                                    json2 = jsonArray.getJSONObject(i);

                                    String commission = json2.getString("commission");

                                    String operator_id = json2.getString("operatorId");
                                    String operator_name = json2.getString("operatorName");
                                    String total_recharge = json2.getString("sales");
                                    Double total_commission = json2.getDouble("commission");

                                    superHero = new Item(operator_name,total_recharge, String.format( "%.2f", total_commission )+"");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setText("Error in parsing response");
                                    linear_container.setVisibility(View.GONE);
                                    rel_no_records.setVisibility(View.VISIBLE);
                                    rel_img_bg.setVisibility(View.GONE);
                                    progress_linear.setVisibility(View.GONE);

                                }

                                movieList.add(superHero);
                            }
                            mAdapter.notifyDataSetChanged();
                            linear_container.setVisibility(View.VISIBLE);
                            rel_no_records.setVisibility(View.GONE);
                            rel_img_bg.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }else{
                            txt_err_msgg.setText("No Records Found!");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            rel_img_bg.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Error in parsing response");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                rel_img_bg.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onResume() {

        super.onResume();
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
