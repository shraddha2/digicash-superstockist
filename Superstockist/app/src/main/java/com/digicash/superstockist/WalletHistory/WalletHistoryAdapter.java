package com.digicash.superstockist.WalletHistory;

/**
 * Created by shraddha on 18-04-2018.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;


public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_type,txt_time,txt_credit,txt_debit,txt_balance;

        public MyViewHolder(View view) {
            super(view);
            txt_type = (TextView) view.findViewById(R.id.txt_type);
            txt_credit = (TextView) view.findViewById(R.id.txt_credit);
            txt_debit = (TextView) view.findViewById(R.id.txt_debit);
            txt_time = (TextView) view.findViewById(R.id.txt_time);
            txt_balance= (TextView) view.findViewById(R.id.txt_balance);
        }
    }

    public WalletHistoryAdapter(List<Item> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.wallet_hist_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);
        holder.txt_type.setText(movie.getType());
        holder.txt_balance.setText(movie.getBalance());
         /* holder.txt_credit.setText(movie.getLabel2());
        holder.txt_debit.setText(movie.getLabel3());*/
        int type=movie.getCredit_type();
        if(type==1)
        {
            holder.txt_credit.setText(movie.getAmt());
            holder.txt_debit.setText("-");

        }else if(type==2)
        {
            holder.txt_debit.setText(movie.getAmt());
            holder.txt_credit.setText("-");
        }else{

        }

        holder.txt_time.setText(DefineData.parseDateToddMMyyyyhh(movie.getDattime()));

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
