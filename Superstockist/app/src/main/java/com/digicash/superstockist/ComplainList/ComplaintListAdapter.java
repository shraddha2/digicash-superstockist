package com.digicash.superstockist.ComplainList;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

/**
 * Created by shraddha on 27-06-2018.
 */

public class ComplaintListAdapter extends RecyclerView.Adapter<ComplaintListAdapter.MyViewHolder> {
    private List<Item> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_rech_no,txt_recharge_amt, txt_status, txt_msg,txt_solved_date,txt_complain_date,txt_comment,txt_admin_reply;
        public MyViewHolder(View view) {
            super(view);
            txt_rech_no = (TextView) view.findViewById(R.id.txt_rech_no);
            txt_recharge_amt =(TextView) view.findViewById(R.id.txt_recharge_amt);
            txt_status = (TextView) view.findViewById(R.id.txt_status);
            //txt_msg = (TextView) view.findViewById(R.id.txt_msg);
            txt_solved_date = (TextView) view.findViewById(R.id.txt_solved_date);
            txt_complain_date = (TextView) view.findViewById(R.id.txt_complain_date);
            txt_admin_reply = (TextView) view.findViewById(R.id.txt_admin_reply);
            txt_comment = (TextView) view.findViewById(R.id.txt_comment);
        }
    }

    public ComplaintListAdapter(List<Item> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.complaint_list_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);
        String msg=movie.getTrans_type()+"";
        String solved_date=movie.getTrans_amount()+"";
        holder.txt_rech_no.setText("Title: "+movie.getTrans_no()+"");
        holder.txt_comment.setText("Complaint: "+movie.getStatus()+"");
        holder.txt_complain_date.setText(movie.getTrans_datetime()+"");
        holder.txt_solved_date.setText(solved_date+"");
        holder.txt_status.setText(movie.getTrans_update_balance()+"");
        holder.txt_admin_reply.setText("Admin Reply: "+msg+"");

     /*   if(msg.equalsIgnoreCase(""))
        {
            holder.txt_msg.setVisibility(View.GONE);
        }else{
            holder.txt_msg.setVisibility(View.VISIBLE);
        }*/
        if (movie.getTrans_update_balance().equalsIgnoreCase("ignored")) {
            holder.txt_recharge_amt.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
            holder.txt_status.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
        } else if (movie.getTrans_update_balance().equalsIgnoreCase("Pending")){
            holder.txt_recharge_amt.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
            holder.txt_status.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
        }else{
            holder.txt_recharge_amt.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
            holder.txt_status.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
