package com.digicash.superstockist.AddDistributor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

/**
 * Created by shraddha on 31-05-2018.
 */

public class CitySelectAdapter extends RecyclerView.Adapter<CitySelectAdapter.MyViewHolder>{

    private List<Item> moviesList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label8,txt_label9, txt_label10;
        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txt_label8 = (TextView) view.findViewById(R.id.txt_label8);
            txt_label9 = (TextView) view.findViewById(R.id.txt_label9);
            txt_label10 = (TextView) view.findViewById(R.id.txt_label10);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String operator_name=txt_label8.getText().toString();
                    String id=txt_label9.getText().toString();
                    String state_id=txt_label10.getText().toString();
                    Intent intent=new Intent(ctx, SelectCityActivity.class);
                    intent.putExtra("operator_circle_name",operator_name);
                    intent.putExtra("operator_circle_id",id);
                    intent.putExtra("state_id",state_id);
                    ((Activity) ctx).setResult(6,intent);
                    ((Activity) ctx).finish();//finishing activity
                }
            });

        }
    }

    public CitySelectAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.select_city, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_label8.setText(movie.getStatus()+"");
        holder.txt_label9.setText(movie.getP_name()+"");
        holder.txt_label10.setText(movie.getTrans_update_balance()+"");

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
