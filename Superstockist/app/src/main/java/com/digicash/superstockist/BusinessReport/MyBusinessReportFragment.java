package com.digicash.superstockist.BusinessReport;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.superstockist.DijicashSuperStockists;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class MyBusinessReportFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    TextView txt_bal,txt_ret_no,txt_rech_total_bal,txt_rech_total_commission,txt_mt_total_bal,txt_mt_total_commission,txt_bp_total_bal,txt_bp_total_comm,txt_dth_total_bal,txt_dth_total_comm;
    String token;
    ImageView btn_search;
    SharedPreferences sharedpreferences;
    TextView txt_err_msgg,txt_network_msg;
    ConstraintLayout progress_linear,linear_container,rel_no_records,rel_no_internet;
    private FirebaseAnalytics mFirebaseAnalytics;
    TextView txt_from_date,txt_to_date;
    DateFormat inputFormat,outputFormat;
    String start_date,end_date;
    Date from_date,to_date;

    //Typeface font ;
    public MyBusinessReportFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_my_business_report, container, false);

        getActivity().setTitle("DijiCash");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

       /* txt_bal=(TextView) rootView.findViewById(R.id.txt_bal);
        txt_ret_no=(TextView) rootView.findViewById(R.id.txt_ret_no);*/
        txt_rech_total_bal=(TextView) rootView.findViewById(R.id.txt_recharge_sales);
        txt_rech_total_commission=(TextView) rootView.findViewById(R.id.txt_recharge_commission);
        txt_mt_total_bal=(TextView) rootView.findViewById(R.id.txt_mt_sales);
        txt_mt_total_commission=(TextView) rootView.findViewById(R.id.txt_mt_commission);
        txt_bp_total_bal=(TextView) rootView.findViewById(R.id.txt_bp_total_bal);
        txt_bp_total_comm=(TextView) rootView.findViewById(R.id.txt_bp_total_comm);
        //txt_dth_total_bal = (TextView) rootView.findViewById(R.id.txt_dth_total_bal);
        //txt_dth_total_comm = (TextView) rootView.findViewById(R.id.txt_dth_total_comm);
        txt_from_date = (TextView) rootView.findViewById(R.id.txt_from_date);
        txt_to_date = (TextView) rootView.findViewById(R.id.txt_to_date);
        btn_search= (ImageView) rootView.findViewById(R.id.btn_search);

        inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        outputFormat = new SimpleDateFormat("dd MMM yyyy");

        txt_to_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        start_date=DefineData.getCurrentDate();
        end_date=start_date;


        txt_to_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        start_date=DefineData.getCurrentDate();
        end_date=start_date;

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_date=DefineData.parseDateToyyyMMdd(txt_from_date.getText().toString());
                end_date=DefineData.parseDateToyyyMMdd(txt_to_date.getText().toString());

                if(checkConnection()) {
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.GONE);
                    new FetchData().execute();
                }else{
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.VISIBLE);
                }
            }
        });

        txt_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),R.style.MyCalendarStyle,new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;

                        txt_from_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));


                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("Start Date");
                mDatePicker.show();  }


        });


        txt_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),R.style.MyCalendarStyle,new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;

                        txt_to_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));


                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("End Date");
                mDatePicker.show();  }


        });

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchData().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }
        return rootView;
    }

    private class FetchData extends AsyncTask<Void, Void, Void> {
        JSONObject response;

        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try {
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("startDate", start_date);
                parameters.put("endDate",  end_date);
                this.response = new JSONObject(service.POST(DefineData.FETCH_BUSINESS_REPORT,parameters, token));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (response != null) {
                try {
                    if (response.getBoolean("error")) {
                        txt_err_msgg.setText("oops ! Server Error");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {

                        JSONObject json_data=response.getJSONObject("data");

                        JSONObject json_rech = json_data.getJSONObject("recharge");
                        Double totalBusiness_ret= json_rech.getDouble("totalSales");
                        Double totalComm_ret= json_rech.getDouble("totalComm");

                        JSONObject json_mt = json_data.getJSONObject("moneyTransfer");
                        Double totalBusiness_mt= json_mt.getDouble("totalSales");
                        Double totalComm_mt= json_mt.getDouble("totalComm");

                        JSONObject json_bill_payment = json_data.getJSONObject("billPayment");
                        Double bp_total_bal= json_bill_payment.getDouble("totalSales");
                        Double bp_total_comm= json_bill_payment.getDouble("totalComm");

                      /*  JSONObject json_dth_booking = json_data.getJSONObject("dthBooking");
                        Double dth_total_bal= json_dth_booking.getDouble("totalSales");
                        Double dth_total_comm= json_dth_booking.getDouble("totalComm");*/

                        txt_rech_total_bal.setText("₹ "+String.format( "%.2f", totalBusiness_ret)+"");
                        txt_rech_total_commission.setText("₹ "+String.format( "%.2f", totalComm_ret)+"");

                        txt_mt_total_bal.setText("₹ "+String.format( "%.2f", totalBusiness_mt)+"");
                        txt_mt_total_commission.setText("₹ "+String.format( "%.2f", totalComm_mt)+"");

                        txt_bp_total_bal.setText("₹ "+String.format( "%.2f", bp_total_bal)+"");
                        txt_bp_total_comm.setText("₹ "+String.format( "%.2f", bp_total_comm)+"");

                        /*txt_dth_total_bal.setText("₹ "+String.format( "%.2f", dth_total_bal)+"");
                        txt_dth_total_comm.setText("₹ "+String.format( "%.2f",dth_total_comm)+"");*/

                        linear_container.setVisibility(View.VISIBLE);
                        rel_no_records.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    txt_err_msgg.setText("oops ! Server Error");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);

                }
            } else {
                txt_err_msgg.setText("oops ! Server Error");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);

            }
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onResume() {

        super.onResume();
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
}
