package com.digicash.superstockist.Login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.ForgotPassword.ForgotPasswordActivity;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class EnterPasswordActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    TextView edt_mobile_no,txtError,txt_forgot_password;
    EditText edt_pass_word;
    Button btn_login;
    String mobileNo,pass_word,pwd,storePwd;
    ConstraintLayout loading;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_password_activity);
Log.d("enterpwdactivty","ghuvhurh");
        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        editor = sharedpreferences.edit();

        //mobileNo=sharedpreferences.getString(DefineData.KEY_USER_NAME,"");
        mobileNo=sharedpreferences.getString(DefineData.REGISTERED_MOBILE_NO,"");
        edt_mobile_no = (TextView) findViewById(R.id.edt_mobile_no);
        edt_mobile_no.setText(mobileNo+" ");
        Log.d("trgtg",mobileNo);

        loading = (ConstraintLayout) findViewById(R.id.loading) ;
        txtError = (TextView) findViewById(R.id.txtError);

        pwd=sharedpreferences.getString(DefineData.KEY_PASSWORD,"");
        Log.d("fnd",pwd+" ");
        edt_pass_word = (EditText) findViewById(R.id.edt_pass_word);
        edt_pass_word.setText(pwd+"");

        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pass_word = edt_pass_word.getText().toString();
                boolean empty = false;

                if (null == pass_word || pass_word.length() == 0 || pass_word == "") {
                    edt_pass_word.setError("Please enter your correct password");
                    empty = true;
                }

                if(!empty) {
                    if(checkConnection()) {
                        storePwd = sharedpreferences.getString(DefineData.KEY_PASSWORD, storePwd);
                        if (sharedpreferences.getBoolean(DefineData.KEY_REMEMBER_PIN,true)){
                            if (pass_word .matches(storePwd)) {
                                startActivity(new Intent(EnterPasswordActivity.this, HomeActivity.class));
                                finish();
                            }
                            else{
                                txtError.setVisibility(View.VISIBLE);
                                txtError.setText("Entered password is incorrect! Try again");
                            }
                        }
                        else
                        {
                            //edtEnterPin.setError("Entered pin is incorrect! Try again");
                        }
                    }
                }

            }
        });

        txt_forgot_password = (TextView) findViewById(R.id.txt_forgot_password);
        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isError=false;
                if(!isError) {
                    if (checkConnection()) {
                        new ForgotPassword().execute();
                    }else {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText("No Internet Connection");
                    }
                }

            }
        });
    }

    private class ForgotPassword extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("phoneNumber", mobileNo);
                this.response = new JSONObject(service.POST(DefineData.FORGOT_PASSWORD,parameters,""));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(response.getString("data"));
                        loading.setVisibility(View.GONE);
                    } else {
                        Intent i=new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                        editor.putString(DefineData.REGISTERED_MOBILE_NO, mobileNo);
                        editor.commit();
                        i.putExtra("phoneNumber",mobileNo);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setText("Error in parsing response");
                    txtError.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);
                }
            }else{
                txtError.setText("Empty Server Response");
                txtError.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
            }
        }
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
