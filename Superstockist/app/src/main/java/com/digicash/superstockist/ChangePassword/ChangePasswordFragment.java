package com.digicash.superstockist.ChangePassword;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Change your Password
  Mandatory Fields: Otp, New Password*/

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Login.MainActivity;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ChangePasswordFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edt_new_pass,edt_confirm_pass;
    Button btn_submit;
    String token="",mobile_no;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    TextView txt_title,txt_label1,txt_network_msg,txt_label2,txt_error;
    ConstraintLayout progress_linear,linear_container,rel_no_internet;
    String otp_up="",new_password="",confirm_password="";
    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_update_password, container, false);
        //String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle("DijiCash");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        mobile_no=sharedpreferences.getString(DefineData.REGISTERED_MOBILE_NO,"");

        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2= (TextView) rootView.findViewById(R.id.txt_label2);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);
        edt_new_pass= (EditText) rootView.findViewById(R.id.edt_new_pass);
        edt_confirm_pass= (EditText) rootView.findViewById(R.id.edt_confirm_pass);
        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new_password=edt_new_pass.getText().toString();
                confirm_password=edt_confirm_pass.getText().toString();
                boolean isError=false;
                if(null==new_password||new_password.length()==0||new_password.length()<6)
                {
                    isError=true;

                    edt_new_pass.setError("Password Cannot be less than 6-letters");

                }
                if(null==confirm_password||confirm_password.length()==0||!confirm_password.equalsIgnoreCase(new_password))
                {
                    isError=true;
                    edt_confirm_pass.setError("Password Not Matching");
                }
                if(!isError) {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new UpdatePasswordOtp().execute();
                    }else{
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        return rootView;
    }

    public void showChangeLangDialog(boolean isError,String msg) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.cust_edit_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

        if(isError)
        {
            edt.setError(msg+"");
        }

        dialogBuilder.setTitle("OTP Dialog");
        dialogBuilder.setMessage("Enter OTP");
        dialogBuilder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                otp_up=edt.getText().toString();
                dialog.dismiss();
                new UpdatePassword().execute();

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                dialog.dismiss();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


    }

    private class UpdatePassword extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("otp", otp_up);
                parameters.put("newPassword", new_password);
                this.response = new JSONObject(service.POST(DefineData.OTP_UPDATE_PASSWORD,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {

            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("data");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        showChangeLangDialog(true,msg);
                    } else {
                        editor.putString(DefineData.TOKEN_KEY, "");
                        editor.putBoolean(DefineData.KEY_LOGIN_SUCCESS, false);
                        editor.putString(DefineData.KEY_DISTRIBUTOR_NAME, "");
                        editor.putString(DefineData.KEY_WALLET_BALANCE, "");
                        editor.putBoolean(DefineData.KEY_REMEMBER_ME, false);
                        editor.commit();
                        Toast.makeText(getActivity(),"Password Updated Successfully", Toast.LENGTH_LONG).show();
                        editor.putString(DefineData.REGISTERED_MOBILE_NO, mobile_no);
                        editor.commit();
                        Intent i=new Intent(getActivity(), MainActivity.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing responser");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {

        super.onResume();
        edt_new_pass.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_new_pass.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_confirm_pass.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_confirm_pass.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }


    private class UpdatePasswordOtp extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {

            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{

                this.response = new JSONObject(service.POST(DefineData.UPDATE_PASSWORD,token));
            }catch (Exception e) {
                e.printStackTrace();

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText(response.getString("data"));

                    }
                    progress_linear.setVisibility(View.GONE);
                    linear_container.setVisibility(View.VISIBLE);
                    showChangeLangDialog(false,"");
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing response");
                    txt_error.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    linear_container.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                txt_error.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                linear_container.setVisibility(View.VISIBLE);
            }

        }
    }
}
