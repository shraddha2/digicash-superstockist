package com.digicash.superstockist.DistributorList;

/*Edited By: Shraddha Shetkar
  Date: 18/04/2018
  Des: Add balance for the retailers
  Mandatory Fields: Start and End Date*/

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AddBalanceFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edit_amount,edit_comments;
    TextView txt_label,txt_title,txt_error,txt_network_msg, txt_distributor_name, txt_balance;
    String amount,comments;
    Button btn_submit;
    String token="", trans_type="";
    String id="", name="",balance="";
    SharedPreferences sharedpreferences;
    ConstraintLayout progress_linear,linear_container,rel_no_internet;
    RadioGroup radioGroup;
    RadioButton rb_rec,rb_credit;

    public AddBalanceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_add_balance, container, false);
        //String mess = getResources().getString(R.string.app_name);
        Bundle bundle = getArguments();
        getActivity().setTitle("DijiCash");

        id=bundle.getString("id");
        name=bundle.getString("name");
        balance = bundle.getString("balance");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_distributor_name= (TextView) rootView.findViewById(R.id.txt_distributor_name);
        txt_balance= (TextView) rootView.findViewById(R.id.txt_balance);
        txt_label= (TextView) rootView.findViewById(R.id.txt_label);
        radioGroup=(RadioGroup) rootView.findViewById(R.id.radioGroup);
        rb_rec=(RadioButton) rootView.findViewById(R.id.rb_rec);
        rb_credit=(RadioButton) rootView.findViewById(R.id.rb_credit);
        edit_amount= (EditText) rootView.findViewById(R.id.edit_amount);
        edit_comments= (EditText) rootView.findViewById(R.id.edit_comments);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);
        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        txt_distributor_name.setText(name + "");
        txt_balance.setText("Current Balance: " +  balance + "");

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount=edit_amount.getText().toString();
                comments=edit_comments.getText().toString();
                int  selectedValueId = radioGroup.getCheckedRadioButtonId();
                //checking the id of the selected radio
                if(selectedValueId == rb_rec.getId())
                {
                    trans_type="paid";
                }
                else if(selectedValueId == rb_credit.getId())
                {
                    trans_type="credit";
                }else{
                    trans_type="";
                }
                if (checkConnection()) {
                    boolean isError=false;
                    if(null==trans_type||trans_type.length()==0||trans_type.equalsIgnoreCase(""))
                    {
                        isError=true;
                        rb_credit.setError("Field Cannot be Blank");
                    }
                  /*  if(null==amount||amount.length()>=10||amount.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edit_amount.setError("Field Cannot be Blank");
                    }*/
                    if(null==amount||amount.length()<2||amount.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edit_amount.setError("Invalid Amount");
                        if(amount.length()<2)
                        {
                            edit_amount.setError("Amount must be equal or greater than Rs.10");
                        }
                    }
                    if(null==comments||comments.length()==0||comments.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edit_comments.setError("Field Cannot be Blank");
                    }
                    if(!isError) {
                        if (checkConnection()) {

                            custdialog();

                        }else{
                            txt_error.setText("No Internet Connection");
                            txt_error.setTextColor(ContextCompat.getColor(getActivity(), R.color.status_fail));
                            txt_error.setVisibility(View.VISIBLE);
                        }
                    }
                }
                else{
                    linear_container.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.VISIBLE);
                }

            }
        });
        return rootView;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


    }

    private class AddBalance extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("distributorId", id);
                parameters.put("amount", amount);
                parameters.put("mode", trans_type);
                parameters.put("remark", comments);
                this.response = new JSONObject(service.POST(DefineData.ADD_BALANCE,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("trggg",id+" "+amount+" "+trans_type+" "+comments+" ");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("data");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    } else {
                        String msg=response.getString("data");
                        //Toast.makeText(getActivity(),"Complain Added Successfully", Toast.LENGTH_LONG).show();
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        android.app.Fragment frg=new DistributorListFragment();
                        ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg)
                                .addToBackStack(null)
                                .commit();


                      /*  txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText("Complaint Send Successfully");*/

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing responser");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {

        super.onResume();
        radioGroup.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    radioGroup.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edit_amount.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_amount.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edit_comments.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_comments.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new DistributorListFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }

    private void custdialog(){
        final View dialogView = View.inflate(getActivity(),R.layout.confirm_add_balance,null);

        final Dialog dialog = new Dialog(getActivity(),R.style.FullScreenDialogStyle);
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        TextView txt_entered_rname=(TextView)dialog.findViewById(R.id.txt_entered_rname);
        TextView txt_mode=(TextView)dialog.findViewById(R.id.txt_mode);
        TextView txt_amount=(TextView)dialog.findViewById(R.id.txt_amount);
        Button btn_confirm = (Button) dialog.findViewById(R.id.btn_confirm);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        txt_entered_rname.setText(name);
        txt_mode.setText(trans_type);
        txt_amount.setText("₹ "+amount);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new AddBalance().execute();
            }
        });
        dialog.show();
    }
}
