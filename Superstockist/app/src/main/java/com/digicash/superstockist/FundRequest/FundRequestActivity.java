package com.digicash.superstockist.FundRequest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.ViewFundRequest.MyFundRequestFragment;

import org.json.JSONException;
import org.json.JSONObject;

public class FundRequestActivity extends AppCompatActivity {

    TextView txt_balance,txt_dijicash;
    String balance="", token;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    boolean loggedin=false;
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_request);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(false);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)
        txt_balance = (TextView) toolbar.findViewById(R.id.txt_balance);
        txt_dijicash = (TextView) toolbar.findViewById(R.id.txt_scom);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        loggedin=sharedpreferences.getBoolean(DefineData.KEY_LOGIN_SUCCESS,false);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        new FetchBalance().execute();
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    FundRequestFragment tab1 = new FundRequestFragment();
                    return tab1;
                case 1:
                    MyFundRequestFragment tab2 = new MyFundRequestFragment();
                    return tab2;

                default:
                    return null;

            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "FUND REQUEST";
                case 1:
                    return "MY FUND REQUESTS";
            }
            return null;
        }
    }

    private class FetchBalance extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{

                this.response = new JSONObject(service.POST(DefineData.FETCH_BALANCE,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                //String balance="";
                try {
                    if (response.getBoolean("error")) {
                        balance="Balance : ₹ 0";
                    } else {
                        balance=response.getString("balance");
                    }
                    txt_balance.setText("₹ "+balance+"");
                    editor.putString(DefineData.KEY_WALLET_BALANCE, balance);
                    editor.commit();

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }else{

            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent =new Intent(this, HomeActivity.class);
                startActivity(intent);
                finish();
                //onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent =new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();//finishing activity

    }
}
