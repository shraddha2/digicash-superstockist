package com.digicash.superstockist.DistributorList;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.ArrayList;
import java.util.List;

public class RetailerAdapter extends RecyclerView.Adapter<RetailerAdapter.MyViewHolder> implements Filterable {

    private List<Item> moviesList;
    private List<Item> mFilteredList;
    Context ctx;
    String frg_name="";
    String token="";
    SharedPreferences sharedpreferences;

    public RetailerAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.mFilteredList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
        sharedpreferences = ctx.getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token= sharedpreferences.getString(DefineData.TOKEN_KEY,"");
    }

    @Override
    public RetailerAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.retailer_row, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RetailerAdapter.MyViewHolder viewHolder, int i) {
        //Item movie = moviesList.get(position);
        viewHolder.txt_id.setText(mFilteredList.get(i).getType()+"");
        viewHolder.txt_name.setText(mFilteredList.get(i).getAmt()+"");
        viewHolder.txt_balance.setText("₹. "+mFilteredList.get(i).getBalance()+"");
        viewHolder.txtMobileNo.setText(mFilteredList.get(i).getDebit_credit_type()+"");
        viewHolder.txtLastFundsTaken.setText(mFilteredList.get(i).getDattime()+"");

        if (Double.parseDouble(mFilteredList.get(i).getBalance().toString())<= 500){
            viewHolder.txt_balance.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
        }else{
            viewHolder.txt_balance.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_id, txt_name,txt_balance,txtMobileNo,txtLastFundsTaken;

        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txt_id = (TextView) view.findViewById(R.id.txt_id);
            txt_name = (TextView) view.findViewById(R.id.txt_name);
            txt_balance = (TextView) view.findViewById(R.id.txt_balance);
            txtMobileNo = (TextView) view.findViewById(R.id.txtMobileNo);
            txtLastFundsTaken = (TextView) view.findViewById(R.id.txtLastFundsTaken);
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = moviesList;
                } else {

                    ArrayList<Item> filteredList = new ArrayList<>();

                    for (Item androidVersion : moviesList) {

                        if (androidVersion.getAmt().toLowerCase().contains(charString) || androidVersion.getDebit_credit_type().toLowerCase().contains(charString) ) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Item>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
