package com.digicash.superstockist.CommissionChart;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CommissionChartAdapter extends RecyclerView.Adapter<CommissionChartAdapter.MyViewHolder> {

    private List<Item> moviesList;
    String req_type="prepaid";
    Context ctx;
    //Typeface font ;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_operator_name, txt_commision,txt_operator_id;
        ImageView img_operator_logo;

        public MyViewHolder(View view) {
            super(view);
            txt_operator_name = (TextView) view.findViewById(R.id.txt_operator_name);
            txt_commision = (TextView) view.findViewById(R.id.txt_commision);
            txt_operator_id= (TextView) view.findViewById(R.id.txt_operator_id);
            img_operator_logo = (ImageView) view.findViewById(R.id.img_operator_logo);
            if(req_type.equalsIgnoreCase("mt")||req_type.equalsIgnoreCase("bill-payment"))
            {
                img_operator_logo.setVisibility(View.GONE);
            }else{
                img_operator_logo.setVisibility(View.VISIBLE);
            }
        }
    }

    public CommissionChartAdapter(List<Item> moviesList, Context ctx, String req_type) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.req_type=req_type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.commission_chart_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);
        holder.txt_operator_name.setText(movie.getOperator_name());
        holder.txt_commision.setText(movie.getProfit_amt());
        holder.txt_operator_id.setText(movie.getSale_amt());

        switch(movie.getSale_amt())
        {
            case "1":
                Picasso.with(ctx).load(R.drawable.ic_airtel).fit().into(holder.img_operator_logo);
                break;
            case "2":
                Picasso.with(ctx).load(R.drawable.ic_aircel).fit().into(holder.img_operator_logo);
                break;
            case "3":
                Picasso.with(ctx).load(R.drawable.ic_idea_logo).fit().into(holder.img_operator_logo);
                break;
            case "4":
                Picasso.with(ctx).load(R.drawable.ic_vodafone).fit().into(holder.img_operator_logo);
                break;
            case "5":
                Picasso.with(ctx).load(R.drawable.ic_bsnl).fit().into(holder.img_operator_logo);
                break;
            case "6":
                Picasso.with(ctx).load(R.drawable.ic_bsnl).fit().into(holder.img_operator_logo);
                break;
            case "7":
                Picasso.with(ctx).load(R.drawable.ic_telenor).fit().into(holder.img_operator_logo);
                break;
            case "8":
                Picasso.with(ctx).load(R.drawable.ic_telenor).fit().into(holder.img_operator_logo);
                break;
            case "9":
                Picasso.with(ctx).load(R.drawable.ic_jio).fit().into(holder.img_operator_logo);
                break;
            case "10":
                Picasso.with(ctx).load(R.drawable.ic_docomo).fit().into(holder.img_operator_logo);
                break;
            case "11":
                Picasso.with(ctx).load(R.drawable.ic_docomo).fit().into(holder.img_operator_logo);
                break;
            case "12":
                Picasso.with(ctx).load(R.drawable.ic_mtnl).fit().into(holder.img_operator_logo);
                break;
            case "13":
                Picasso.with(ctx).load(R.drawable.ic_mtnl).fit().into(holder.img_operator_logo);
                break;
            case "14":
                Picasso.with(ctx).load(R.drawable.ic_airtel).fit().into(holder.img_operator_logo);
                break;
            case "15":
                Picasso.with(ctx).load(R.drawable.ic_idea_logo).fit().into(holder.img_operator_logo);
                break;
            case "16":
                Picasso.with(ctx).load(R.drawable.ic_vodafone).fit().into(holder.img_operator_logo);
                break;
            case "17":
                Picasso.with(ctx).load(R.drawable.ic_aircel).fit().into(holder.img_operator_logo);
                break;
            case "18":
                Picasso.with(ctx).load(R.drawable.ic_docomo).fit().into(holder.img_operator_logo);
                break;
            case "19":
                Picasso.with(ctx).load(R.drawable.ic_airtel_tv).fit().into(holder.img_operator_logo);
                break;
            case "20":
                Picasso.with(ctx).load(R.drawable.ic_tata_sky).fit().into(holder.img_operator_logo);
                break;
            case "21":
                Picasso.with(ctx).load(R.drawable.ic_dish_tv).fit().into(holder.img_operator_logo);
                break;
            case "22":
                Picasso.with(ctx).load(R.drawable.ic_videocon).fit().into(holder.img_operator_logo);
                break;
            case "23":
                Picasso.with(ctx).load(R.drawable.ic_airtel_tv).fit().into(holder.img_operator_logo);
                break;
            case "24":
                Picasso.with(ctx).load(R.drawable.ic_sun_direct).fit().into(holder.img_operator_logo);
                break;
            case "25":
                Picasso.with(ctx).load(R.drawable.ic_bbps_logo).fit().into(holder.img_operator_logo);
                break;
            case "26":
                Picasso.with(ctx).load(R.drawable.ic_bsnl).fit().into(holder.img_operator_logo);
                break;
            case "27":
                Picasso.with(ctx).load(R.drawable.ic_msedc).fit().into(holder.img_operator_logo);
                break;
            case "28":
                Picasso.with(ctx).load(R.drawable.ic_torent).fit().into(holder.img_operator_logo);
                break;
            case "29":
                Picasso.with(ctx).load(R.drawable.ic_tata_indicom).fit().into(holder.img_operator_logo);
                break;
            case "30":
                Picasso.with(ctx).load(R.drawable.tata_power).fit().into(holder.img_operator_logo);
                break;
            case "31":
                Picasso.with(ctx).load(R.drawable.ic_best).fit().into(holder.img_operator_logo);
                break;
            case "32":
                Picasso.with(ctx).load(R.drawable.adani_power).fit().into(holder.img_operator_logo);
                break;
            case "33":
                Picasso.with(ctx).load(R.drawable.ic_sndl).fit().into(holder.img_operator_logo);
                break;
            case "34":
                Picasso.with(ctx).load(R.drawable.tikona_broadband).fit().into(holder.img_operator_logo);
                break;
            case "35":
                Picasso.with(ctx).load(R.drawable.hathway_broadband_logo).fit().into(holder.img_operator_logo);
                break;
            case "36":
                Picasso.with(ctx).load(R.drawable.connectbroadbandlogo).fit().into(holder.img_operator_logo);
                break;
            case "37":
                Picasso.with(ctx).load(R.drawable.act_broadband_logo).fit().into(holder.img_operator_logo);
                break;
            case "38":
                Picasso.with(ctx).load(R.drawable.ic_zing).fit().into(holder.img_operator_logo);
                break;
            default:  Picasso.with(ctx).load(R.drawable.ic_nooperator).fit().into(holder.img_operator_logo);
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
