package com.digicash.superstockist.DthBooking;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

public class DthBookingAdapter extends RecyclerView.Adapter<DthBookingAdapter.MyViewHolder> {

    private List<Item> billPaymentList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtBusinessName,txtAmount,txtOperatorName,txtBookingId,txtCustomerName,txtCustomerMobNo,txtCommission,txtCreatedAt,txtStatus;
        ImageView ImgArrow;
        ConstraintLayout ContentMoreInfo;
        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txtBusinessName = (TextView) view.findViewById(R.id.txtBusinessName);
            txtAmount = (TextView) view.findViewById(R.id.txtAmount);
            txtOperatorName = (TextView) view.findViewById(R.id.txtOperatorName);
            txtBookingId = (TextView) view.findViewById(R.id.txtBookingId);
            txtCustomerName = (TextView) view.findViewById(R.id.txtCustomerName);
            txtCustomerMobNo = (TextView) view.findViewById(R.id.txtCustomerMobNo);
            txtCommission = (TextView) view.findViewById(R.id.txtCommission);
            txtCreatedAt = (TextView) view.findViewById(R.id.txtCreatedAt);
            txtStatus = (TextView) view.findViewById(R.id.txtStatus);
            ImgArrow= (ImageView) view.findViewById(R.id.ImgArrow);
            ContentMoreInfo= (ConstraintLayout) view.findViewById(R.id.ContentMoreInfo);

            ImgArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility =ContentMoreInfo.getVisibility();
                    if (visibility == View.VISIBLE){
                        ContentMoreInfo.setVisibility(View.GONE);
                        ImgArrow.setImageResource(R.drawable.ic_expand_more);
                    }else{
                        ContentMoreInfo.setVisibility(View.VISIBLE);
                        ImgArrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });
        }
    }

    public DthBookingAdapter(List<Item> billPaymentList, Context ctx, String frg_name) {
        this.billPaymentList = billPaymentList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public DthBookingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dth_booking_layout, parent, false);

        return new DthBookingAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DthBookingAdapter.MyViewHolder holder, int position) {
        Item movie = billPaymentList.get(position);

        holder.txtBusinessName.setText(movie.getLabel1()+"");
        holder.txtAmount.setText(movie.getLabel2()+"");
        holder.txtOperatorName.setText(movie.getLabel3());
        holder.txtBookingId.setText(movie.getLabel4());
        holder.txtCustomerName.setText(movie.getLabel5()+"");
        holder.txtCustomerMobNo.setText(movie.getLabel6()+"");
        holder.txtCommission.setText(movie.getLabel7()+"");
        holder.txtCreatedAt.setText(movie.getLabel8()+"");
        holder.txtStatus.setText(movie.getLabel9()+"");

        if (movie.getLabel9().equalsIgnoreCase("Failed")) {
            holder.txtAmount.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
            holder.txtStatus.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
        } else if (movie.getLabel9().equalsIgnoreCase("Pending")){
            holder.txtAmount.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
            holder.txtStatus.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
        }else{
            holder.txtAmount.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
            holder.txtStatus.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
        }
    }

    @Override
    public int getItemCount() {
        return billPaymentList.size();
    }
}
