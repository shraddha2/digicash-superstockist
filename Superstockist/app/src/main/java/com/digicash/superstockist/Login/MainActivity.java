package com.digicash.superstockist.Login;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: User Login*/

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.ForgotPassword.ForgotPasswordActivity;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;
import com.digicash.superstockist.SplashScreen.SplashScreenActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    TextView edt_mobile_no,txt_forgot_password,txtError;
    Button btn_login;
    EditText edt_pass_word;
    CheckBox cb_remember;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    String mobile_no,pass_word;
    boolean loggedin=false,key_rember=false,doubleBackToExitPressedOnce = false;
    ConstraintLayout loading,container;
    CoordinatorLayout coordinateLayout;
    boolean setPin=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        coordinateLayout= (CoordinatorLayout)findViewById(R.id.coordinateLayout);
        Log.d("tryreuree",setPin+"");
        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        editor = sharedpreferences.edit();

        loggedin=sharedpreferences.getBoolean(DefineData.KEY_LOGIN_SUCCESS,false);
        key_rember=sharedpreferences.getBoolean(DefineData.KEY_REMEMBER_ME,false);
        loading= (ConstraintLayout) findViewById(R.id.loading);
        container= (ConstraintLayout) findViewById(R.id.container);
        btn_login = (Button) findViewById(R.id.btn_login);
        edt_mobile_no= (TextView) findViewById(R.id.edt_mobile_no);
        edt_pass_word= (EditText) findViewById(R.id.edt_pass_word);
        txtError= (TextView) findViewById(R.id.txtError);
        txt_forgot_password= (TextView) findViewById(R.id.txt_forgot_password);
        cb_remember= (CheckBox) findViewById(R.id.cb_remember);

        mobile_no=sharedpreferences.getString(DefineData.REGISTERED_MOBILE_NO,"");
        //mobile_no=getIntent().getStringExtra("mobileNo");
        edt_mobile_no.setText("+91 "+mobile_no+" ");

        if(key_rember) {
            mobile_no = sharedpreferences.getString(DefineData.KEY_USER_NAME, mobile_no);
            pass_word = sharedpreferences.getString(DefineData.KEY_PASSWORD, pass_word);
            edt_mobile_no.setText(mobile_no);
            edt_pass_word.setText(pass_word);
            cb_remember.setChecked(key_rember);
        }

        loading.setVisibility(View.GONE);

        setPin=sharedpreferences.getBoolean(DefineData.KEY_REMEMBER_PIN,false);
        Log.d("rrrrr",setPin+" ");
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pass_word = edt_pass_word.getText().toString();
                boolean empty = false;

                if (null == pass_word || pass_word.length() == 0 || pass_word == "") {
                    edt_pass_word.setError("Please enter your password");
                    empty = true;
                }
                if(!empty) {
                    if(checkConnection()) {
                        editor.putBoolean(DefineData.KEY_REMEMBER_PIN, true);
                        Log.d("nnnnnnn",setPin+"");
                        editor.commit();
                        new Login().execute();
                    }
                }
            }
       });

        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //mobile_no=edt_mobile_no.getText().toString();
                boolean isError=false;
               /* if(null==mobile_no||mobile_no.length()<10||mobile_no.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_mobile_no.setError("Enter Mobile No");
                }*/
                if(!isError) {
                    if (checkConnection()) {
                        new ForgotPassword().execute();
                    }else {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText("No Internet Connection");
                    }
                }
            }
        });

        cb_remember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    key_rember=true;
                }else{
                    key_rember=false;
                }
            }
        });

    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent =new Intent(this, SplashScreenActivity.class);
        startActivity(intent);
        finish();
        //Checking for fragment count on backstack
        /*if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this,"Please click BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
            return;
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        if(!checkConnection()) {

            Snackbar mSnackBar = Snackbar.make(coordinateLayout, "Internet not available", Snackbar.LENGTH_INDEFINITE);
            View view = mSnackBar.getView();
            view.setBackgroundColor(Color.RED);
            /*mSnackBar.setAction("CLICK HERE", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(MainActivity.this,OfflineModeActivity.class);
                    startActivity(i);
                }
            }).setActionTextColor(Color.WHITE).show();*/
            TextView mainTextView = (TextView) (view).findViewById(android.support.design.R.id.snackbar_text);
            mainTextView.setTextColor(Color.WHITE);
            mSnackBar.show();

        }
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private class Login extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            container.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("phone", mobile_no);
                parameters.put("password", pass_word);
                this.response = new JSONObject(service.POST(DefineData.LOGIN_URL,parameters,""));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("sdfs",response+" "+mobile_no+ " "+ pass_word+" ");
            if(response!=null) {

                try {
                    if (response.getBoolean("error")) {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(response.getString("data"));
                        loading.setVisibility(View.GONE);
                        container.setVisibility(View.VISIBLE);
                    } else {

                        JSONObject json_data=response.getJSONObject("data");
                        String retailer_phoneno=json_data.getString("phone");
                        String retailer_name=json_data.getString("businessName");
                        //String current_balance=json_data.getString("current_balance");
                        String token=json_data.getString("token");
                        editor.putString(DefineData.TOKEN_KEY, token);
                        editor.putBoolean(DefineData.KEY_LOGIN_SUCCESS, true);
                        editor.putString(DefineData.KEY_DISTRIBUTOR_PHONENO, retailer_phoneno);
                        editor.putString(DefineData.KEY_DISTRIBUTOR_NAME, retailer_name);
                        //editor.putString(DefineData.KEY_WALLET_BALANCE, current_balance);
                        editor.putString(DefineData.KEY_USER_NAME, mobile_no);
                        editor.putString(DefineData.KEY_PASSWORD, pass_word);


                        if(!sharedpreferences.getBoolean(DefineData.KEY_IS_CIRCLE_SAVE,false)) {
                            //prepaid
                            editor.putBoolean(DefineData.KEY_IS_CIRCLE_SAVE, false);
                            editor.putString(DefineData.KEY_OPERATOR_CIRCLE, "Maharashtra/Goa");
                            editor.putString(DefineData.KEY_OPERATOR_CIRCLE_ID, "8");
                        }

                        if(!sharedpreferences.getBoolean(DefineData.KEY_IS_CIRCLE_SAVE_PP,false)) {
                            //postpaid
                            editor.putBoolean(DefineData.KEY_IS_CIRCLE_SAVE_PP, false);
                            editor.putString(DefineData.KEY_OPERATOR_CIRCLE_PP, "Maharashtra/Goa");
                            editor.putString(DefineData.KEY_OPERATOR_CIRCLE_ID_PP, "8");
                        }

                        if(!sharedpreferences.getBoolean(DefineData.KEY_IS_CIRCLE_SAVE_DTH,false)) {
                            //DTH
                            editor.putBoolean(DefineData.KEY_IS_CIRCLE_SAVE_DTH, false);
                            editor.putString(DefineData.KEY_OPERATOR_CIRCLE_DTH, "Maharashtra/Goa");
                            editor.putString(DefineData.KEY_OPERATOR_CIRCLE_ID_DTH, "8");
                        }

                        if (key_rember)
                        {
                            editor.putBoolean(DefineData.KEY_REMEMBER_ME, true);

                        }else{
                            editor.putBoolean(DefineData.KEY_REMEMBER_ME, false);
                        }
                        editor.commit();

                        Intent i=new Intent(getApplicationContext(),HomeActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setText("Invalid Mobile No. or Password");
                    txtError.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);
                    container.setVisibility(View.VISIBLE);
                }
            }else{
                txtError.setText("Empty Server Response");
                txtError.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
            }
        }
    }

    private class ForgotPassword extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            container.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("phoneNumber", mobile_no);
                this.response = new JSONObject(service.POST(DefineData.FORGOT_PASSWORD,parameters,""));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(response.getString("data"));
                        loading.setVisibility(View.GONE);
                        container.setVisibility(View.VISIBLE);
                    } else {
                        Intent i=new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                        editor.putString(DefineData.REGISTERED_MOBILE_NO, mobile_no);
                        editor.commit();
                        i.putExtra("phoneNumber",mobile_no);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setText("Error in parsing response");
                    txtError.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);
                    container.setVisibility(View.VISIBLE);
                }
            }else{
                txtError.setText("Empty Server Response");
                txtError.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
            }
        }
    }
}
