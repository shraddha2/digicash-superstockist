package com.digicash.superstockist.TransactionHistory;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

/**
 * Created by shraddha on 20-06-2018.
 */

public class MTHistoryAdapter extends RecyclerView.Adapter<MTHistoryAdapter.MyViewHolder> {
    // Typeface font ;
    private List<Item> moviesList;
    Context ctx;
    String frg_name="",token="";
    SharedPreferences sharedpreferences;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_label5,txt_label6,txt_label7,txt_label8;
        ImageView img_arrow;
        ConstraintLayout linear_more_data;

        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txt_label6 = (TextView) view.findViewById(R.id.txt_label6);
            txt_label7 = (TextView) view.findViewById(R.id.txt_label7);
            txt_label8 = (TextView) view.findViewById(R.id.txt_label8);
            img_arrow= (ImageView) view.findViewById(R.id.img_arrow);
            linear_more_data= (ConstraintLayout) view.findViewById(R.id.linear_more_data);

            img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility =linear_more_data.getVisibility();
                    if (visibility == View.VISIBLE){
                        linear_more_data.setVisibility(View.GONE);
                        img_arrow.setImageResource(R.drawable.ic_expand_more);
                    }else{

                        linear_more_data.setVisibility(View.VISIBLE);
                        img_arrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });
        }
    }


    public MTHistoryAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
        this.sharedpreferences = ctx.getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        this.token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        // font = Typeface.createFromAsset(ctx.getAssets(), "font/Montserrat-Regular.ttf");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_history_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_label1.setText(movie.getTrans_no()+"");
        holder.txt_label2.setText(movie.getTrans_type()+"");
        holder.txt_label3.setText(movie.getTrans_amount()+"");
        holder.txt_label4.setText(movie.getTrans_datetime()+"");
        holder.txt_label5.setText(movie.getTrans_update_balance()+"");
        holder.txt_label6.setText(movie.getStatus()+"");
        holder.txt_label7.setText(movie.getP_name()+"");
        holder.txt_label8.setText(movie.getAcno()+"");


        if (movie.getAcno().equalsIgnoreCase("Failed")) {
            holder.txt_label2.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
            holder.txt_label8.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
        } else if (movie.getAcno().equalsIgnoreCase("Pending")){
            holder.txt_label2.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
            holder.txt_label8.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));

        }else if (movie.getAcno().equalsIgnoreCase("Response Pending")){
            holder.txt_label2.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
            holder.txt_label8.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
        }else{
            holder.txt_label2.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
            holder.txt_label8.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
