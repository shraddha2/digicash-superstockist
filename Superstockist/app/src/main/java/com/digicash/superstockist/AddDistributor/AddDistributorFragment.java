package com.digicash.superstockist.AddDistributor;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Will Add an Distributor
  Mandatory Fields: First Name,Last Name,Email,Phone No.,Business Name,Address,Aadhar_no,MtScheme,Postpaid_Scheme,Prepaid_Scheme*/

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddDistributorFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edit_fname,edt_lname,edit_email,edit_phone,edit_business_nm,edit_address,edit_aadhar_no,edit_pan_no,edt_state, edt_city;
    TextView edit_mtScheme,edit_prepaidScheme,edit_postpaidScheme,txt_err_msgg;
    String fname,lname,email,phone,business_nm,address,aadhar_no,pan_no,state_select,select_city,mt_Scheme,prepaid_Scheme,postpaid_Scheme;
    Spinner mtScheme,prepaidScheme,postpaidScheme, billPaymentScheme;
    private ArrayList<Item> mList = new ArrayList<>();
    private ArrayList<Item> postList = new ArrayList<>();
    private ArrayList<Item> preList = new ArrayList<>();
    private ArrayList<Item> billPaymentList = new ArrayList<>();
    String scheme_type="--Select--",mt_Scheme_id="0",prepaid_Scheme_id="0", postpaid_Scheme_id="0", billPayment_Scheme_id="0";
    Button btn_submit;
    String token="";
    List<Item> list_comp_type;
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_label1,txt_network_msg,txt_label2,txt_error;
    ConstraintLayout progress_linear,linear_container,city,rel_no_records,rel_no_internet;
    String operator_circle_id, operator_circle_name, bank_id, bank_name;
    String mtSchemeId="",prepareSchemeId="",postpaidSchemeId="",BbpsScheme="";

    public AddDistributorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_add_distributor, container, false);
        //String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle("DijiCash");


        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");


        //mtAdapter = new AddRetailerAdapter(mList,postList,preList,getActivity(),"mSchemes");
        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(),R.layout.activity_state_list_adapter, mList);

        list_comp_type=new ArrayList<>();
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2= (TextView) rootView.findViewById(R.id.txt_label2);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);

        edit_fname= (EditText) rootView.findViewById(R.id.edit_fname);
        edt_lname= (EditText) rootView.findViewById(R.id.edt_lname);
        edit_email= (EditText) rootView.findViewById(R.id.edit_email);
        edit_phone= (EditText) rootView.findViewById(R.id.edit_phone);
        edit_business_nm= (EditText) rootView.findViewById(R.id.edit_business_nm);
        edit_address= (EditText) rootView.findViewById(R.id.edit_address);
        edt_state=  (EditText) rootView.findViewById(R.id.edt_state);
        edt_city=  (EditText) rootView.findViewById(R.id.edt_city);
        edit_aadhar_no= (EditText) rootView.findViewById(R.id.edit_aadhar_no);
        edit_pan_no= (EditText) rootView.findViewById(R.id.edit_pan_no);
        edit_mtScheme= (TextView) rootView.findViewById(R.id.edit_mtScheme);
        edit_prepaidScheme= (TextView) rootView.findViewById(R.id.edit_prepaidScheme);
        edit_postpaidScheme= (TextView) rootView.findViewById(R.id.edit_postpaidScheme);

        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        city= (ConstraintLayout) rootView.findViewById(R.id.city);
        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);

        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        //rel_img_bg.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);
        txt_error.setVisibility(View.GONE);
        city.setVisibility(View.GONE);

        //edt_state.setText(operator_circle_name + "");
        edt_state.setInputType(InputType.TYPE_NULL);
        edt_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),StateSelectActivity.class);
                intent.putExtra("source","rech");
                startActivityForResult(intent, 3);

            }
        });

        edt_city.setInputType(InputType.TYPE_NULL);
        edt_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),SelectCityActivity.class);
                intent.putExtra("source","rech");
                intent.putExtra("operator_circle_id",operator_circle_id);
                startActivityForResult(intent, 6);

            }
        });

        mtScheme= (Spinner) rootView.findViewById(R.id.mtScheme);
        prepaidScheme= (Spinner) rootView.findViewById(R.id.prepaidScheme);
        postpaidScheme= (Spinner) rootView.findViewById(R.id.postpaidScheme);
        billPaymentScheme= (Spinner) rootView.findViewById(R.id.billPaymentScheme);

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            //rel_img_bg.setVisibility(View.VISIBLE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new loadSpinnerData().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            //rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

        mtScheme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                scheme_type = ((TextView)view.findViewById(R.id.txt_operator_name)).getText().toString();
                mt_Scheme_id =((TextView)view.findViewById(R.id.txt_id)).getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        prepaidScheme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                scheme_type = ((TextView)view.findViewById(R.id.txt_operator_name)).getText().toString();
                prepaid_Scheme_id =((TextView)view.findViewById(R.id.txt_id)).getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        postpaidScheme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                scheme_type = ((TextView)view.findViewById(R.id.txt_operator_name)).getText().toString();
                postpaid_Scheme_id =((TextView)view.findViewById(R.id.txt_id)).getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        billPaymentScheme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                scheme_type = ((TextView)view.findViewById(R.id.txt_operator_name)).getText().toString();
                billPayment_Scheme_id =((TextView)view.findViewById(R.id.txt_id)).getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                state_select=edt_state.getText().toString();
                select_city=edt_city.getText().toString();
                fname=edit_fname.getText().toString();
                lname=edt_lname.getText().toString();
                email=edit_email.getText().toString();
                phone=edit_phone.getText().toString();
                business_nm=edit_business_nm.getText().toString();
                address=edit_address.getText().toString();
                aadhar_no=edit_aadhar_no.getText().toString();
                pan_no=edit_pan_no.getText().toString();

                boolean isError=false;
                if(null==fname||fname.length()==0||fname.equalsIgnoreCase(""))
                {
                    isError=true;
                    edit_fname.setError("Please Enter First Name");

                }
                if(null==lname||lname.length()==0||lname.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_lname.setError("Please Enter Last Name");

                }
                // if(email.isEmpty()||!DefineData.isValidEmail(email))
                if(email.isEmpty()||!DefineData.isValidEmail(email))
                {
                    isError=true;
                    edit_email.setError("Please insert a valid email address");

                }
                if(phone.isEmpty()||phone.length()<10)
                {
                    isError=true;
                    edit_phone.setError("Please enter valid mobile number");

                }
                if(null==business_nm||business_nm.length()==0||business_nm.equalsIgnoreCase(""))
                {
                    isError=true;
                    edit_business_nm.setError("Please Enter Business Name");

                }
                if(null==address||address.length()==0||address.equalsIgnoreCase(""))
                {
                    isError=true;
                    edit_address.setError("Please Enter valid Address");

                }
                if(null==state_select||state_select.length()==0||state_select.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_state.setError("Please Select State");

                }
                if(null==select_city||select_city.length()==0||select_city.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_city.setError("Please Select City");

                }
                if(aadhar_no.isEmpty()||aadhar_no.length()<12)
                {
                    isError=true;
                    edit_aadhar_no.setError("Please enter valid Aadhar Number");

                }

                if(!isError) {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new AddRetailer().execute();
                    }else{
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                    }
                }

            }
        });
        return rootView;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private class loadSpinnerData extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            mList.clear();
            postList.clear();
            preList.clear();
            billPaymentList.clear();
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.SCHEMES,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {

            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText(msg+"");
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONObject resjson = response.getJSONObject("data");
                        JSONArray jsonArray=resjson.getJSONArray("mtSchemes");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {

                                    json2 = jsonArray.getJSONObject(i);

                                    String id = json2.getString("id");
                                    String name = json2.getString("name");
                                    mtSchemeId=id;
                                    superHero = new Item(name,id);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_error.setVisibility(View.VISIBLE);
                                    txt_error.setText("Error in parsing response");
                                    linear_container.setVisibility(View.VISIBLE);
                                    progress_linear.setVisibility(View.GONE);
                                }
                                //Adding the superhero object to the list
                                mList.add(superHero);
                            }

                            SpinnerAdapter adapter1 = new SpinnerAdapter(getActivity(),R.layout.activity_state_list_adapter, mList);
                            mtScheme.setAdapter(adapter1);
                            linear_container.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }
                        JSONArray jsonArray2=resjson.getJSONArray("prepaidSchemes");
                        if(jsonArray2.length()!=0) {
                            for (int i = 0; i < jsonArray2.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {

                                    json2 = jsonArray2.getJSONObject(i);

                                    String id = json2.getString("id");
                                    String name = json2.getString("name");
                                    prepareSchemeId=id;
                                    superHero = new Item(name,id);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_error.setVisibility(View.VISIBLE);
                                    txt_error.setText("Error in parsing response");
                                    linear_container.setVisibility(View.VISIBLE);
                                    progress_linear.setVisibility(View.GONE);
                                }
                                preList.add(superHero);
                            }
                            SpinnerAdapter adapter2 = new SpinnerAdapter(getActivity(),R.layout.activity_state_list_adapter, preList);
                            prepaidScheme.setAdapter(adapter2);


                            linear_container.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }
                        JSONArray jsonArray3=resjson.getJSONArray("postpaidSchemes");
                        if(jsonArray3.length()!=0) {
                            for (int i = 0; i < jsonArray3.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {

                                    json2 = jsonArray3.getJSONObject(i);

                                    String id = json2.getString("id");
                                    String name = json2.getString("name");
                                    postpaidSchemeId=id;
                                    superHero = new Item(name,id);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_error.setVisibility(View.VISIBLE);
                                    txt_error.setText("Error in parsing response");
                                    linear_container.setVisibility(View.VISIBLE);
                                    progress_linear.setVisibility(View.GONE);
                                }
                                //Adding the superhero object to the list
                                postList.add(superHero);
                            }
                            SpinnerAdapter adapter3 = new SpinnerAdapter(getActivity(),R.layout.activity_state_list_adapter, postList);
                            postpaidScheme.setAdapter(adapter3);
                            linear_container.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }

                        JSONArray jsonArray4=resjson.getJSONArray("bbpsSchemes");
                        if(jsonArray4.length()!=0) {
                            for (int i = 0; i < jsonArray4.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {

                                    json2 = jsonArray4.getJSONObject(i);

                                    String id = json2.getString("id");
                                    String name = json2.getString("name");
                                    BbpsScheme=id;
                                    superHero = new Item(name,id);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_error.setVisibility(View.VISIBLE);
                                    txt_error.setText("Error in parsing response");
                                    linear_container.setVisibility(View.VISIBLE);
                                    progress_linear.setVisibility(View.GONE);
                                }
                                //Adding the superhero object to the list
                                billPaymentList.add(superHero);
                            }
                            SpinnerAdapter adapter4 = new SpinnerAdapter(getActivity(),R.layout.activity_state_list_adapter, billPaymentList);
                            billPaymentScheme.setAdapter(adapter4);
                            linear_container.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    private class AddRetailer extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("firstName", fname);
                parameters.put("lastName", lname);
                parameters.put("email", email);
                parameters.put("phone", phone);
                parameters.put("businessName", business_nm);
                parameters.put("address", address);
                parameters.put("aadharNumber", aadhar_no);
                parameters.put("stateId", operator_circle_id);
                parameters.put("cityId", bank_id);
                parameters.put("panNumber", pan_no);
                parameters.put("mtScheme", mtSchemeId);
                parameters.put("prepaidScheme", prepareSchemeId);
                parameters.put("postpaidScheme", postpaidSchemeId);
                parameters.put("bbpsScheme", BbpsScheme);
                this.response = new JSONObject(service.POST(DefineData.ADD_DISTRIBUTOR,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("AddDistributor",response+ " "+mtSchemeId+" "+prepareSchemeId+" "+postpaidSchemeId+" "+BbpsScheme+" "+DefineData.ADD_DISTRIBUTOR);
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("data");
                        txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText(msg+"");
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    } else {
                        String msg=response.getString("data");
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        Intent i=new Intent(getActivity(), HomeActivity.class);
                        startActivity(i);
                        getActivity().finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {

        super.onResume();
        edit_fname.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_fname.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_lname.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_lname.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edit_email.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_email.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edit_phone.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_phone.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edit_business_nm.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_business_nm.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edit_address.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_address.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_state.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_state.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_city.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_city.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        //edit_fname,edt_lname,edit_email,edit_phone,edit_address,edit_aadhar_no,edit_pan_no,edit_password;
        edit_aadhar_no.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_aadhar_no.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edit_pan_no.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_pan_no.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        mtScheme.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    mtScheme.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        prepaidScheme.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    prepaidScheme.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        postpaidScheme.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    postpaidScheme.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        billPaymentScheme.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    billPaymentScheme.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case 3:
                if (null!=data) {
                    operator_circle_id = data.getStringExtra("operator_circle_id");
                    operator_circle_name = data.getStringExtra("operator_circle_name");
                    edt_state.setText(operator_circle_name + "");
                    city.setVisibility(View.VISIBLE);

                }
                break;
            case 6:
                if (null!=data) {
                    bank_id = data.getStringExtra("operator_circle_id");
                    bank_name = data.getStringExtra("operator_circle_name");
                    //operator_circle_id = data.getStringExtra("operator_circle_id");
                    //Log.d("ryet", bank_id + " ");
                    edt_city.setText(bank_name + "");

                }
                break;
        }
    }
}
