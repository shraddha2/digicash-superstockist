package com.digicash.superstockist.BankDetails;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class EditBankActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edit_id,edit_bank_name,edit_ifsc,edit_account_no;
    TextView txt_retname,txt_balance,txt_label,txt_title,txt_error,txt_network_msg;
    String bank_name,ifsc,account_no;
    Button btn_submit;
    String token="", trans_type="";
    String id="", name="",balance="";
    SharedPreferences sharedpreferences;
    ConstraintLayout progress_linear,linear_container,rel_no_internet;
    RadioGroup radioGroup;
    RadioButton rb_rec,rb_credit;

    public EditBankActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_edit_bank);
        //getActivity().setTitle("DijiCash");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String appName = getResources().getString(R.string.app_name);
        getSupportActionBar().setTitle(appName);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        id=getIntent().getStringExtra("id");

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_title= (TextView) findViewById(R.id.txt_title);
        txt_label= (TextView) findViewById(R.id.txt_label);
        edit_id= (EditText) findViewById(R.id.edit_id);
        edit_bank_name= (EditText) findViewById(R.id.edit_bank_name);
        edit_ifsc= (EditText) findViewById(R.id.edit_ifsc);
        edit_account_no= (EditText) findViewById(R.id.edit_account_no);
        btn_submit= (Button) findViewById(R.id.btn_submit);
        txt_error= (TextView) findViewById(R.id.txt_error);
        txt_network_msg= (TextView) findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) findViewById(R.id.loading);
        linear_container= (ConstraintLayout) findViewById(R.id.container);
        rel_no_internet= (ConstraintLayout) findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchBankDetails().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bank_name=edit_bank_name.getText().toString();
                ifsc=edit_ifsc.getText().toString();
                account_no=edit_account_no.getText().toString();

                if (checkConnection()) {
                    boolean isError=false;
                    if(null==bank_name||bank_name.length()==0||bank_name.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edit_bank_name.setError("Field Cannot be Blank");
                    }
                    if(null==ifsc||ifsc.length()==0||ifsc.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edit_ifsc.setError("Field Cannot be Blank");
                    }
                    if(null==account_no||account_no.length()==0||account_no.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edit_account_no.setError("Field Cannot be Blank");
                    }
                    if(!isError) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new EditBank().execute();
                    }
                }
                else{
                    linear_container.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.VISIBLE);
                }

            }
        });

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


    }

    private class FetchBankDetails extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);


        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("id", id);
                this.response = new JSONObject(service.POST(DefineData.FETCH_BANK_DETAILS,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONObject json = response.getJSONObject("data");
                        if(json.length()!=0) {

                            try {
                                String bankName = json.getString("bankName");
                                String ifscCode = json.getString("ifscCode");
                                String accountNumber = json.getString("accountNumber");
                                edit_bank_name.setText(bankName+"");
                                edit_ifsc.setText(ifscCode+"");
                                edit_account_no.setText(accountNumber+"");

                                linear_container.setVisibility(View.VISIBLE);
                                progress_linear.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                linear_container.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);
                            }
                        }else{
                            linear_container.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    linear_container.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                linear_container.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    private class EditBank extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("id", id);
                parameters.put("bankName", bank_name);
                parameters.put("ifsc", ifsc);
                parameters.put("accountNumber", account_no);
                this.response = new JSONObject(service.POST(DefineData.EDIT_BANK,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {


            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("data");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    } else {
                        String msg=response.getString("data");
                        //Toast.makeText(getActivity(),"Complain Added Successfully", Toast.LENGTH_LONG).show();
                        Toast.makeText(EditBankActivity.this,msg+"", Toast.LENGTH_LONG).show();
                        Intent i=new Intent(EditBankActivity.this, BankDetailsActivity.class);
                        startActivity(i);
                        finish();

                      /*  txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText("Complaint Send Successfully");*/

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing responser");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent =new Intent(this, BankDetailsActivity.class);
                startActivity(intent);
                finish();
                //return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent =new Intent(this, BankDetailsActivity.class);
        startActivity(intent);
        finish();
    }
}
