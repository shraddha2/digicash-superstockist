package com.digicash.superstockist.OutstandingReport;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

public class CollectOutstandingFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edit_amount,edit_comments, txt_from_date;
    TextView txt_retname,txt_balance,txt_label,txt_title,txt_error,txt_network_msg, edt_my_bank;
    String amount,comments;
    Button btn_submit;
    String token="", trans_type="", start_date;
    String id="",balance="",name= "", trans_type2 = "", bank_id = "", bank_name;
    SharedPreferences sharedpreferences;
    ConstraintLayout progress_linear,linear_container, layout_bank_transfer,rel_no_internet;
    RadioGroup radioGroup, radioGroup2;
    RadioButton cash, bank;

    public CollectOutstandingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_collect_outstanding, container, false);
        //String mess = getResources().getString(R.string.app_name);
        Bundle bundle = getArguments();
        getActivity().setTitle("DijiCash");

        id=bundle.getString("id");
        balance = bundle.getString("balance");
        name = bundle.getString("name");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_retname= (TextView) rootView.findViewById(R.id.txt_retname);
        txt_balance= (TextView) rootView.findViewById(R.id.txt_balance);
        txt_label= (TextView) rootView.findViewById(R.id.txt_label);
        txt_balance= (TextView) rootView.findViewById(R.id.txt_balance);
        edit_amount= (EditText) rootView.findViewById(R.id.edit_amount);
        txt_from_date = (EditText) rootView.findViewById(R.id.txt_from_date);
        radioGroup2 = (RadioGroup) rootView.findViewById(R.id.radioGroup2);
        cash = (RadioButton) rootView.findViewById(R.id.cash);
        bank = (RadioButton) rootView.findViewById(R.id.bank);
        layout_bank_transfer = (ConstraintLayout) rootView.findViewById(R.id.layout_bank_transfer);
        edt_my_bank = (TextView) rootView.findViewById(R.id.edt_my_bank);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);
        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);
        layout_bank_transfer.setVisibility(View.GONE);
        edt_my_bank.setInputType(InputType.TYPE_NULL);
        edt_my_bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (movie.getLabel8().equalsIgnoreCase("Failed")) {
                //request_type="recharge"
                Intent intent = new Intent(getActivity(), MyBankDetailsActivity.class);
                //intent.putExtra("source","fundReq");
                startActivityForResult(intent, 6);
            }
        });

        txt_retname.setText(name+"");
        txt_balance.setText("Outstanding Amount: "+"₹ "+balance+"");

        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        start_date = DefineData.getCurrentDate();
        txt_from_date.setInputType(InputType.TYPE_NULL);
        txt_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), R.style.MyCalendarStyle, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth = selectedmonth + 1;
                        String inputDateStr = selectedyear + "-" + selectedmonth + "-" + selectedday;

                        txt_from_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));


                    }
                }, mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("Start Date");
                mDatePicker.show();
            }


        });

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId2) {
                // checkedId is the RadioButton selected

                switch (checkedId2) {
                    case R.id.cash:
                        // switch to fragment 1
                        layout_bank_transfer.setVisibility(View.GONE);
                        break;

                    case R.id.bank:
                        // Fragment 2
                        layout_bank_transfer.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_date = DefineData.parseDateToyyyMMdd(txt_from_date.getText().toString());

                int selectedValueId2 = radioGroup2.getCheckedRadioButtonId();
                if (selectedValueId2 == cash.getId()) {
                    trans_type2 = "cash"; //bank
                } else if (selectedValueId2 == bank.getId()) {
                    trans_type2 = "bank"; //cash
                } else {
                    trans_type2 = "";
                }

                amount=edit_amount.getText().toString();
                //checking the id of the selected radio

                if (checkConnection()) {
                    boolean isError=false;
                    if(null==amount||amount.length()==0||amount.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edit_amount.setError("Field Cannot be Blank");

                    }

                    if (null == trans_type2 || trans_type2.length() == 0 || trans_type2.equalsIgnoreCase("")) {
                        isError = true;
                        bank.setError("Field Cannot be Blank");
                    }
                    if (layout_bank_transfer.getVisibility() == View.VISIBLE) {
                        final String bank_name = edt_my_bank.getText().toString();
                        if (!isValidmybank(bank_name)) {
                            edt_my_bank.setError("Field Cannot be Blank");
                            isError = true;
                        }
                        else {
                            isError = false;
                    }
                    }

                    if(!isError) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new CollectOutstanding().execute();
                    }
                }
                else{
                    linear_container.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.VISIBLE);
                }
            }
        });
        return rootView;
    }


    //validate my bank
    private boolean isValidmybank(String bank_name) {
        if (bank_name == null || bank_name.length() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private class CollectOutstanding extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("distributorId", id);
                parameters.put("amount", amount);
                parameters.put("paymentDate", start_date);
                parameters.put("paymentMode", trans_type2);
                parameters.put("bankDetails", bank_id);
                this.response = new JSONObject(service.POST(DefineData.COLLECT_OUTSTANDING,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("data");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    } else {

                        String msg=response.getString("data");
                        //Toast.makeText(getActivity(),"Complain Added Successfully", Toast.LENGTH_LONG).show();
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        Fragment frg=new OutstandingReportFragment();
                        ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg)
                                .addToBackStack(null)
                                .commit();

                      /*  txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText("Complaint Send Successfully");*/

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing responser");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {

        super.onResume();

        edit_amount.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_amount.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        txt_from_date.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    txt_from_date.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    Fragment frg=new OutstandingReportFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case 6:
                if (null != data) {
                    bank_id = data.getStringExtra("operator_circle_id");
                    bank_name = data.getStringExtra("operator_circle_name");
                    edt_my_bank.setText(bank_name + "");
                }
                break;
        }
    }
}
