package com.digicash.superstockist.AddComplain;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Add an Complain
  Mandatory Fields: Title, Description*/

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AddComplainFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edt_new_pass,edt_confirm_pass;
    Button btn_submit;
    String token="";
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_label1,txt_network_msg,txt_label2,txt_error;
    ConstraintLayout progress_linear,linear_container,rel_no_internet;
    String otp_up="",new_password="",confirm_password="";
    public AddComplainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_add_complain, container, false);
        //String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle("DijiCash");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2= (TextView) rootView.findViewById(R.id.txt_label2);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);
        edt_new_pass= (EditText) rootView.findViewById(R.id.edt_new_pass);
        edt_confirm_pass= (EditText) rootView.findViewById(R.id.edt_confirm_pass);
        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new_password=edt_new_pass.getText().toString();
                confirm_password=edt_confirm_pass.getText().toString();
                boolean isError=false;
                if(null==new_password||new_password.length()==0||new_password.length()<4)
                {
                    isError=true;

                    edt_new_pass.setError("Field Cannot be Blank");

                }
                if(null==confirm_password||confirm_password.length()==0||confirm_password.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_confirm_pass.setError("Field Cannot be Blank");
                }
                if(!isError) {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new AddComplain().execute();
                    }else{
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        return rootView;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


    }

    private class AddComplain extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("title", new_password);
                parameters.put("description", confirm_password);
                this.response = new JSONObject(service.POST(DefineData.ADD_COMPLAIN,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {


            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("data");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    } else {
                        String msg=response.getString("data");
                        //Toast.makeText(getActivity(),"Complain Added Successfully", Toast.LENGTH_LONG).show();
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        Intent i=new Intent(getActivity(), HomeActivity.class);
                        startActivity(i);
                        getActivity().finish();

                      /*  txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText("Complaint Send Successfully");*/

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing responser");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {

        super.onResume();
        edt_new_pass.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_new_pass.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_confirm_pass.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_confirm_pass.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }
}
