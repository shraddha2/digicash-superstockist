package com.digicash.superstockist.SearchNumbers;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Will search the transactions based on mobile no. and request type
  Mandatory Fields: Mobile No. or Request Type*/

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.superstockist.DijicashSuperStockists;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Model.DividerItemDecoration;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchNumberFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener, View.OnClickListener {
    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SearchNumberAdapter mAdapter;
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg;
    String token, request_type="recharge";
    SharedPreferences sharedpreferences;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_img_bg,rel_no_records,rel_no_internet;
    private FirebaseAnalytics mFirebaseAnalytics;
    EditText edt_search_number;
    ImageView img_search;

    private Button[] btn = new Button[3];
    private Button btn_unfocus;
    private int[] btn_id = {R.id.btn0, R.id.btn1, R.id.btn2};

    public SearchNumberFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView= inflater.inflate(R.layout.fragment_search_number, container, false);
        ///String mess = getResources().getString(R.string.app_name);
        //getActivity().setTitle(mess);
        getActivity().setTitle("DijiCash");
        //setHasOptionsMenu(true);

        for(int i = 0; i < btn.length; i++){
            btn[i] = (Button) rootView.findViewById(btn_id[i]);
            btn[i].setBackgroundColor(Color.rgb(255,255,255));
            btn[i].setOnClickListener(this);
        }

        btn_unfocus = btn[0];

        edt_search_number=(EditText)rootView.findViewById(R.id.edt_search_number);
        img_search=(ImageView)rootView.findViewById(R.id.img_search);


        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_recharge_history);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        rel_img_bg= (RelativeLayout) rootView.findViewById(R.id.rel_img_bg);
        rel_no_records= (RelativeLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (RelativeLayout) rootView.findViewById(R.id.rel_no_internet);

        txt_title.setText("Search Recharges");

        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        rel_img_bg.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);


        mAdapter = new SearchNumberAdapter(movieList,getActivity(),"Search Number");
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query=edt_search_number.getText().toString();
                boolean isError=false;

                if(null==query||query.length()==0||query.length()<10)
                {
                    isError=true;
                    edt_search_number.setError("Invalid Mobile Number");
                }


                if(!isError) {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new FetchRecharges().execute(query);
                    }else{
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        setFocus(btn_unfocus, btn[0]);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        //setForcus(btn_unfocus, (Button) findViewById(v.getId()));
        //Or use switch
        switch (v.getId()){
            case R.id.btn0 :
                request_type="recharge";
                setFocus(btn_unfocus, btn[0]);
                break;

            case R.id.btn1 :
                request_type="mt";
                setFocus(btn_unfocus, btn[1]);
                break;

            case R.id.btn2 :
                request_type="bbps";
                setFocus(btn_unfocus, btn[2]);
                break;

        }

    }

    private void setFocus(Button btn_unfocus, Button btn_focus){
        btn_unfocus.setTextColor(Color.rgb(49, 50, 51));
        btn_unfocus.setBackgroundColor(Color.rgb(255,255,255));
        btn_focus.setTextColor(Color.rgb(255,255,255));
        btn_focus.setBackgroundColor(Color.rgb(255, 155, 0));
        this.btn_unfocus = btn_focus;
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private class FetchRecharges extends AsyncTask<String, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);


        }
        @Override
        protected Void doInBackground(String... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("phoneNumber", params[0]);
                parameters.put("type", request_type);
                this.response = new JSONObject(service.POST(DefineData.SEARCH_RECHARGE,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = response.getJSONArray("data");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    //Getting json
                                    json2 = jsonArray.getJSONObject(i);

                                    String id = json2.getString("id");
                                    String mobileno = json2.getString("mobileNo");
                                    String amount = json2.getString("amount");
                                    String transaction_no = json2.getString("transactionNo");
                                    String commission = json2.getString("commission");
                                    String status = json2.getString("status");
                                    String operator_id = json2.getString("operator_id");
                                    String operator_name = json2.getString("operator_name");
                                    String retailer_name = json2.getString("retailerName");
                                    String time = json2.getString("time");
                                    superHero = new Item(mobileno, "₹ " + amount, transaction_no, commission, id, operator_name, time, status,
                                            operator_id,retailer_name);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setText("Error in parsing response");
                                    linear_container.setVisibility(View.GONE);
                                    rel_no_records.setVisibility(View.VISIBLE);
                                    rel_img_bg.setVisibility(View.GONE);
                                    progress_linear.setVisibility(View.GONE);

                                }

                                movieList.add(superHero);
                            }
                            mAdapter.notifyDataSetChanged();
                            linear_container.setVisibility(View.VISIBLE);
                            rel_no_records.setVisibility(View.GONE);
                            rel_img_bg.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }else{
                            txt_err_msgg.setText("No Records Found!");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            rel_img_bg.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Error in parsing response");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                rel_img_bg.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onResume() {

        super.onResume();
        edt_search_number.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_search_number.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }
    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
}
