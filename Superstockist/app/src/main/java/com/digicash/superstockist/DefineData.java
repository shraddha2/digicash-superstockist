package com.digicash.superstockist;

import android.text.TextUtils;
import android.util.Patterns;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DefineData {

    public static final String LOGIN_MINKSPAY_PREFERENCE = "logindijicashdistpref" ;
    public static final String TOKEN_KEY = "token_key";
    public static final String KEY_LOGIN_SUCCESS = "key_login";
    public static final String KEY_REMEMBER_ME = "remember_me_key";
    public static final String KEY_DISTRIBUTOR_PHONENO = "key_distributor_phoneno";
    public static final String KEY_DISTRIBUTOR_NAME = "key_distributor_name";
    public static final String KEY_WALLET_BALANCE = "key_wallet_balance";
    public static final String KEY_USER_NAME = "key_username";
    public static final String KEY_PASSWORD = "key_password";
    public static final String KEY_PERCENTAGE = "key_percentage_data";
    public static final String KEY_REMEMBER_PIN = "remember_pin_key";
    public static final String KEY_MOBILENO = "key_mobile_no";
    public static final String KEY_PWD = "key_pwd";
    public static final String REGISTERED_MOBILE_NO = "registered_mobile_no";

    public static String RETAILER_SHRPREF="DC_retailer_det";
    public static String KEY_RETAILER_ID="key_ret_id";
    public static String KEY_FNAME="key_fname";
    public static String KEY_LNAME="key_lname";
    public static String KEY_MNAME="key_mname";
    public static String KEY_DOB="key_dob";
    public static String KEY_GENDER="key_gender";
    public static String KEY_PROFILE_PIC="key_profile_pic";
    public static String KEY_PROFILE_PIC_UP="key_profile_pic_up";

    public static String KEY_PHNO="key_phno";
    public static String KEY_EMAIL="key_email";
    public static String KEY_ADDRESS="key_addr";
    public static String KEY_PIN="key_pin";
    public static String KEY_STATE="key_state";
    public static String KEY_CITY="key_city";
    public static String KEY_STATE_ID="key_state_id";
    public static String KEY_CITY_ID="key_city_id";


    public static String KEY_STORE_NAME="key_store_name";
    public static String KEY_STORE_TYPE="key_store_type";
    public static String KEY_LAT="key_lat";
    public static String KEY_LON="key_lon";
    public static String KEY_BUSINESS_TYPE="key_business_type";
    public static String KEY_STORE_PIC="key_store_pic";
    public static String KEY_BUSINESS_TYPE_ID="key_bs_tye_id";
    public static String KEY_STORE_PIC_UP="key_store_pic_up";

    public static String KEY_ADHAAR_NO="key_adhaar_no";
    public static String KEY_PAN_CARD_PIC_UP="key_pan_card_up";
    public static String KEY_PAN_CARD_NO="key_pan_card_no";

    public static String KEY_PANCARD_PIC="key_pancard_pic";
    public static String KEY_ADHAAR_PIC="key_adhaar_pic";
    public static String KEY_ADHAAR_PIC_UP="key_aadhar_pic_up";

    //offline
    public static final String KEY_OPERATOR_CIRCLE_OFFLINE = "key_operator_circle_offline";
    public static final String KEY_IS_CIRCLE_SAVE_OFFLINE = "key_oc_save_offline";
    public static final String KEY_OPERATOR_CIRCLE_ID_OFFLINE = "key_oc_save_id_offline";

    public static final String OFFLINE_MOBILE_NUMBER = "9970822776";

    //prepaid
    public static final String KEY_OPERATOR_CIRCLE = "key_operator_circle";
    public static final String KEY_IS_CIRCLE_SAVE = "key_oc_save";
    public static final String KEY_OPERATOR_CIRCLE_ID = "key_oc_save_id";

    //postpaid
    public static final String KEY_OPERATOR_CIRCLE_PP = "key_operator_circle_pp";
    public static final String KEY_IS_CIRCLE_SAVE_PP  = "key_oc_save_pp";
    public static final String KEY_OPERATOR_CIRCLE_ID_PP = "key_oc_save_id_pp";

    //DTH
    public static final String KEY_OPERATOR_CIRCLE_DTH = "key_operator_circle_dth";
    public static final String KEY_IS_CIRCLE_SAVE_DTH  = "key_oc_save_dth";
    public static final String KEY_OPERATOR_CIRCLE_ID_DTH= "key_oc_save_id_dth";

    // private static final String prefix="http://192.168.1.107/scom_localhost/public/";
    //private static final String prefix="http://test-whitelabel.minkspay.com/";
    //private static final String mid_prefix="api/distributor/";
    //private static final String prefix="http://192.168.0.114/";
    //private static final String prefix="https://app.dijicash.info/";
    //http://192.168.0.115/api/retailer/login
    private static final String prefix="https://app.dijicash.info/";
    private static final String mid_prefix="api/super/";

    public static final String GENERATE_OTP =prefix+"api/user/login-send-otp";
    public static final String AUTHENTICATE_OTP =prefix+"api/user/validate-otp";
    public static final String CHECK_VERSION=prefix+"api/validate-version";
    public static final String CONTACT_US=prefix+"api/get-contact-us-details";

    public static final String FETCH_VERSION=prefix+"superstockist-get-current-app-version";

    public static final String LOGIN_URL =prefix+mid_prefix+"login";
    public static final String FETCH_BALANCE=prefix+mid_prefix+"get-balance";
    public static final String FETCH_NOTIFICATION=prefix+mid_prefix+"check-for-notification";
    public static final String FETCH_PERCENTAGE=prefix+mid_prefix+"get-percentage-sales";
    public static final String FETCH_PROFILE=prefix+mid_prefix+"get-profile";
    public static final String FETCH_WALLET_HISTORY =prefix+mid_prefix+"get-wallet-history";
    public static final String FORGOT_PASSWORD=prefix+mid_prefix+"forgot-password";
    public static final String OTP_FORGOT_PASSWORD=prefix+mid_prefix+"forgot-password-otp";
    public static final String UPDATE_PASSWORD=prefix+mid_prefix+"change-password";
    public static final String OTP_UPDATE_PASSWORD=prefix+mid_prefix+"change-password-otp";

    public static final String FETCH_COMMISIONS=prefix+mid_prefix+"get-commission-chart";
    public static final String FETCH_COMMISIONS_REPORT=prefix+mid_prefix+"get-commission-report";
    public static final String ADD_COMPLAIN=prefix+mid_prefix+"add-complaint";
    public static final String FETCH_COMPLAINT=prefix+mid_prefix+"get-complaint-list";
    public static final String SEARCH_RECHARGE=prefix+mid_prefix+"search-transaction";
    public static final String SEARCH_FUND_REQUEST =prefix+mid_prefix+"get-banks";
    public static final String SUBMIT_DETAILS=prefix+mid_prefix+"request-funds-submit";
    public static final String ADD_BANK=prefix+mid_prefix+"add-bank";
    public static final String MY_BANK_DETAILS=prefix+mid_prefix+"get-my-banks";
    public static final String VIEW_FUND_REQUEST=prefix+mid_prefix+"view-fund-requests";
    public static final String FETCH_BUSINESS_REPORT =prefix+mid_prefix+"business-report";
    public static final String FETCH_ACCOUNT_REPORT =prefix+mid_prefix+"get-account-ledger";
    public static final String EDIT_PERSONAL_DETAILS =prefix+mid_prefix+"edit-personal";
    public static final String EDIT_KYC_DETAILS =prefix+mid_prefix+"edit-kyc";
    public static final String ADD_DISTRIBUTOR =prefix+mid_prefix+"add-distributor";
    public static final String SCHEMES =prefix+mid_prefix+"get-schemes";
    public static final String FETCH_DISTRIBUTOR =prefix+mid_prefix+"list-distributors";
    public static final String FETCH_RETAILERS = prefix+mid_prefix+"list-retailer";
    public static final String ADD_BALANCE =prefix+mid_prefix+"add-funds";
    public static final String FETCH_CITY =prefix+mid_prefix+"get-cities";
    public static final String SEARCH_RECHARGE_HISTORY=prefix+mid_prefix+"recharge-history";
    public static final String SEARCH_DTHBOOKING_HISTORY=prefix+mid_prefix+"dth-booking-history";
    public static final String GET_MT_TRANS_HIST=prefix+mid_prefix+"money-transfer-history";
    public static final String ACCEPT_FUNDS_REQUEST=prefix+mid_prefix+"accept-fund-request";
    public static final String REJECT_FUNDS=prefix+mid_prefix+"reject-fund-request";
    public static final String MY_FUND_REQUEST=prefix+mid_prefix+"view-sent-fund-requests";
    public static final String FETCH_OUTSTANDING_REPORT=prefix+mid_prefix+"get-payment-report";
    public static final String FETCH_BILL_HISTORY=prefix+mid_prefix+"bill-payment-history";

    public static final String OUTSTANDING_REPORT =prefix+mid_prefix+"outstanding-report";
    public static final String COLLECT_OUTSTANDING =prefix+mid_prefix+"outstanding-collect";
    public static final String FETCH_OUTSTANDING_DETAILS=prefix+mid_prefix+"outstanding-show-payments";
    public static final String EDIT_BANK =prefix+mid_prefix+"edit-bank";
    public static final String FETCH_PERSONAL_DETAILS=prefix+mid_prefix+"fetch-edit-personal";
    public static final String FETCH_KYC_DETAILS=prefix+mid_prefix+"fetch-edit-kyc";
    public static final String FETCH_BANK_DETAILS=prefix+mid_prefix+"fetch-edit-bank";
   // public static final String FETCH_RETAILER=prefix+mid_prefix+"";

    public static final String OLD_DATA_URL="http://13.126.134.145/distributor/login";
    public static final String REVERT_BALANCE =prefix+mid_prefix+"distributor-deduct-balance-from-retailer";
    public static final String FETCH_RETAILER_RECHARGES =prefix+mid_prefix+"distributor-get-retailer-recharges";
    public static final String FETCH_RETAILER_RECHARGES_DATE =prefix+mid_prefix+"distributor-get-retailer-recharges-by-date";
    public static final String FETCH_RECHARGES_REPORT =prefix+mid_prefix+"distributor-get-recharge-report";
    public static final String FETCH_RECHARGES_REPORT_DATE =prefix+mid_prefix+"distributor-get-recharge-report-by-date";
    public static final String FETCH_WALLET_HISTORY_DATE =prefix+mid_prefix+"distributor-wallet-history-by-date";
    public static final String MONEY_TRANSFER =prefix+mid_prefix+"distributor-get-mt-reports";
    public static final String MONEY_TRANSFER_DATE =prefix+mid_prefix+"distributor-get-mt-reports-by-date";

    public static final String FETCH_ACCOUNT_REPORT_DATE =prefix+mid_prefix+"distributor-get-account-report-by-date";

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    public static String parseDateToyyyMMdd(String time) {
        String outputPattern = "yyyy-MM-dd";
        String inputPattern= "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseDateToddMMyyyyhh(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd ");
        String strDate = mdformat.format(calendar.getTime());

       return strDate;
    }


}
