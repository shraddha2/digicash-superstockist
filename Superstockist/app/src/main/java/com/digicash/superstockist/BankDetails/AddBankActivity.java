package com.digicash.superstockist.BankDetails;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Add Bank details
  Mandatory Fields: Bank Name,Branch Name,IFSC,Account Holder,Account Type,Account No.*/

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AddBankActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edt_bank_name,edt_branch_name,edt_ifsc,edt_account_holder,edt_account_no;
    String bank_name,branch_name,ifsc,account_holder,account_type,account_no;
    Button btn_submit;
    String token="", trans_type="";
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_label1,txt_network_msg,txt_label2,txt_error;
    ConstraintLayout progress_linear,linear_container,rel_no_internet;
    RadioGroup rg_trans_type;
    RadioButton current,savings;

    public AddBankActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_bank);
        //getActivity().setTitle("DijiCash");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String appName = getResources().getString(R.string.app_name);
        getSupportActionBar().setTitle(appName);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_title= (TextView) findViewById(R.id.txt_title);
        txt_label1= (TextView) findViewById(R.id.txt_label1);
        txt_label2= (TextView) findViewById(R.id.txt_label2);
        btn_submit= (Button) findViewById(R.id.btn_submit);
        edt_bank_name= (EditText) findViewById(R.id.edt_bank_name);
        edt_branch_name= (EditText) findViewById(R.id.edt_branch_name);
        edt_ifsc= (EditText) findViewById(R.id.edt_ifsc);
        edt_account_holder= (EditText) findViewById(R.id.edt_account_holder);
        edt_account_no= (EditText) findViewById(R.id.edt_account_no);
        txt_error= (TextView) findViewById(R.id.txt_error);
        txt_network_msg= (TextView) findViewById(R.id.txt_network_msg);
        rg_trans_type=(RadioGroup) findViewById(R.id.radioGroup);
        current=(RadioButton) findViewById(R.id.current);
        savings=(RadioButton) findViewById(R.id.savings);

        progress_linear= (ConstraintLayout) findViewById(R.id.loading);
        linear_container= (ConstraintLayout) findViewById(R.id.container);
        rel_no_internet= (ConstraintLayout) findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int  selectedValueId = rg_trans_type.getCheckedRadioButtonId();
                //checking the id of the selected radio
                if(selectedValueId == current.getId())
                {
                    trans_type="Current";
                }
                else if(selectedValueId == savings.getId())
                {
                    trans_type="Savings";
                }else{
                    trans_type="";
                }

                bank_name=edt_bank_name.getText().toString();
                branch_name=edt_branch_name.getText().toString();
                ifsc=edt_ifsc.getText().toString();
                account_holder=edt_account_holder.getText().toString();
                account_no=edt_account_no.getText().toString();

                boolean isError=false;
                if(null==trans_type||trans_type.length()==0||trans_type.equalsIgnoreCase(""))
                {
                    isError=true;
                    savings.setError("Field Cannot be Blank");
                }
                if(null==bank_name||bank_name.length()==0||bank_name.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_bank_name.setError("Field Cannot be Blank");

                }
                if(null==branch_name||branch_name.length()==0||branch_name.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_branch_name.setError("Field Cannot be Blank");

                }
                if(null==ifsc||ifsc.length()==0||ifsc.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_ifsc.setError("Field Cannot be Blank");

                }
                if(null==account_holder||account_holder.length()==0||account_holder.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_account_holder.setError("Field Cannot be Blank");

                }
                if(null==account_no||account_no.length()==0||account_no.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_account_no.setError("Field Cannot be Blank");

                }
                if(!isError) {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new AddBank().execute();
                    }else{
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


    }

    private class AddBank extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("bankName", bank_name);
                parameters.put("branch", branch_name);
                parameters.put("ifsc", ifsc);
                parameters.put("accountHolder", account_holder);
                parameters.put("accountType", trans_type);
                parameters.put("accountNumber", account_no);
                this.response = new JSONObject(service.POST(DefineData.ADD_BANK,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {


            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("data");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    } else {
                        String msg=response.getString("data");
                        //Toast.makeText(getActivity(),"Complain Added Successfully", Toast.LENGTH_LONG).show();
                        Toast.makeText(AddBankActivity.this,msg+"", Toast.LENGTH_LONG).show();
                        Intent i=new Intent(AddBankActivity.this, BankDetailsActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent =new Intent(this, BankDetailsActivity.class);
                startActivity(intent);
                finish();
                //return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent =new Intent(this, BankDetailsActivity.class);
        startActivity(intent);
        finish();
    }
}
