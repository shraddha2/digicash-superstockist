package com.digicash.superstockist.TransactionHistory;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by shraddha on 20-06-2018.
 */

public class RechargeHistoryAdapter extends RecyclerView.Adapter<RechargeHistoryAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_label5,txt_label6,txt_label7,txt_label8,txt_label9;
        ImageView img_arrow,img_logo;
        ConstraintLayout linear_more_data;
        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txt_label6 = (TextView) view.findViewById(R.id.txt_label6);
            txt_label7 = (TextView) view.findViewById(R.id.txt_label7);
            txt_label8 = (TextView) view.findViewById(R.id.txt_label8);
            txt_label9 = (TextView) view.findViewById(R.id.txt_label9);
            img_logo= (ImageView) view.findViewById(R.id.img_logo);
            img_arrow= (ImageView) view.findViewById(R.id.img_arrow);
            linear_more_data= (ConstraintLayout) view.findViewById(R.id.linear_more_data);
            txt_label5.setVisibility(View.GONE);

            img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility =linear_more_data.getVisibility();
                    if (visibility == View.VISIBLE){
                        linear_more_data.setVisibility(View.GONE);
                        img_arrow.setImageResource(R.drawable.ic_expand_more);
                    }else{
                        linear_more_data.setVisibility(View.VISIBLE);
                        img_arrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });
        }
    }

    public RechargeHistoryAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recharge_history_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_label1.setText(movie.getLabel1()+"");
        holder.txt_label2.setText(movie.getLabel2()+"");
        holder.txt_label9.setText("Retailer: "+movie.getLabel10());
        holder.txt_label3.setText("Trans. No.: "+movie.getLabel3());
        holder.txt_label4.setText("Commission.: "+movie.getLabel4()+"");
        holder.txt_label5.setText(movie.getLabel5()+"");
        holder.txt_label6.setText("Operator : "+movie.getLabel6()+"");
        holder.txt_label7.setText(DefineData.parseDateToddMMyyyyhh(movie.getLabel7())+"");
        holder.txt_label8.setText(movie.getLabel8()+"");

        switch(movie.getLabel9())
        {
            case "1":
                Picasso.with(ctx).load(R.drawable.ic_airtel).into(holder.img_logo);
                break;
            case "2":
                Picasso.with(ctx).load(R.drawable.ic_aircel).into(holder.img_logo);
                break;
            case "3":
                Picasso.with(ctx).load(R.drawable.ic_idea_logo).into(holder.img_logo);
                break;
            case "4":
                Picasso.with(ctx).load(R.drawable.ic_vodafone).into(holder.img_logo);
                break;
            case "5":
                Picasso.with(ctx).load(R.drawable.ic_bsnl).into(holder.img_logo);
                break;
            case "6":
                Picasso.with(ctx).load(R.drawable.ic_bsnl).into(holder.img_logo);
                break;
            case "7":
                Picasso.with(ctx).load(R.drawable.ic_telenor).into(holder.img_logo);
                break;
            case "8":
                Picasso.with(ctx).load(R.drawable.ic_telenor).into(holder.img_logo);
                break;
            case "9":
                Picasso.with(ctx).load(R.drawable.ic_jio).into(holder.img_logo);
                break;
            case "10":
                Picasso.with(ctx).load(R.drawable.ic_docomo).into(holder.img_logo);
                break;
            case "11":
                Picasso.with(ctx).load(R.drawable.ic_docomo).into(holder.img_logo);
                break;
            case "12":
                Picasso.with(ctx).load(R.drawable.ic_mtnl).into(holder.img_logo);
                break;
            case "13":
                Picasso.with(ctx).load(R.drawable.ic_mtnl).into(holder.img_logo);
                break;
            case "14":
                Picasso.with(ctx).load(R.drawable.ic_airtel).into(holder.img_logo);
                break;
            case "15":
                Picasso.with(ctx).load(R.drawable.ic_idea_logo).into(holder.img_logo);
                break;
            case "16":
                Picasso.with(ctx).load(R.drawable.ic_vodafone).into(holder.img_logo);
                break;
            case "17":
                Picasso.with(ctx).load(R.drawable.ic_aircel).into(holder.img_logo);
                break;
            case "18":
                Picasso.with(ctx).load(R.drawable.ic_docomo).into(holder.img_logo);
                break;
            case "19":
                Picasso.with(ctx).load(R.drawable.ic_airtel_tv).into(holder.img_logo);
                break;
            case "20":
                Picasso.with(ctx).load(R.drawable.ic_tata_sky).into(holder.img_logo);
                break;
            case "21":
                Picasso.with(ctx).load(R.drawable.ic_dish_tv).into(holder.img_logo);
                break;
            case "22":
                Picasso.with(ctx).load(R.drawable.ic_videocon).into(holder.img_logo);
                break;
            case "23":
                Picasso.with(ctx).load(R.drawable.ic_big_tv).into(holder.img_logo);
                break;
            case "24":
                Picasso.with(ctx).load(R.drawable.ic_sun_direct).into(holder.img_logo);
                break;
            default:  Picasso.with(ctx).load(R.drawable.ic_airtel).into(holder.img_logo);
        }

        if (movie.getLabel8().equalsIgnoreCase("Failed")) {
            holder.txt_label2.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
            holder.txt_label8.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
        } else if (movie.getLabel8().equalsIgnoreCase("Pending")){
            holder.txt_label2.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
            holder.txt_label8.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
        }else{
            holder.txt_label2.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
            holder.txt_label8.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
