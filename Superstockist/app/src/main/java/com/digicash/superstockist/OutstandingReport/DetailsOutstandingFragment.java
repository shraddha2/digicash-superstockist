package com.digicash.superstockist.OutstandingReport;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.DividerItemDecoration;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


        public class DetailsOutstandingFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener{
        private List<Item> movieList = new ArrayList<>();
        private RecyclerView recyclerView;
        private OutstandingDetailAdapter mAdapter;
        String token,request_type="recharge";
        SharedPreferences sharedpreferences;
        TextView txt_retname,txt_balance,txt_title,txt_label1,txt_err_msgg,txt_network_msg,txt_label2;
        ConstraintLayout progress_linear,linear_container,rel_no_records,rel_no_internet;
        String id="",balance="",name= "";

        public DetailsOutstandingFragment() {
        // Required empty public constructor
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_details_outstanding, container, false);
        //String mess = getResources().getString(R.string.app_name);
        Bundle bundle = getArguments();
        getActivity().setTitle("DijiCash");
        // setHasOptionsMenu(true);

        id=bundle.getString("id");
        balance = bundle.getString("balance");
        name = bundle.getString("name");

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_commission_chart);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2= (TextView) rootView.findViewById(R.id.txt_label2);
        txt_retname= (TextView) rootView.findViewById(R.id.txt_retname);
        txt_balance= (TextView) rootView.findViewById(R.id.txt_balance);

        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_retname.setText(name+"");
        txt_balance.setText("Outstanding Amount: "+"₹ "+balance+"");

        if(checkConnection()) {
        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);
        new FetchDetails().execute();
        }else{
        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.VISIBLE);
        }


        return rootView;
        }

        private class FetchDetails extends AsyncTask<Void, Void, Void> {

            JSONObject response;
            @Override
            protected void onPreExecute() {
                movieList.clear();
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.GONE);
                progress_linear.setVisibility(View.VISIBLE);


            }
            @Override
            protected Void doInBackground(Void... params) {
                HTTPURLConnection service = new HTTPURLConnection();
                try{
                    HashMap<String, String> parameters = new HashMap<String, String>();
                    parameters.put("distributorId", id);
                    this.response = new JSONObject(service.POST(DefineData.FETCH_OUTSTANDING_DETAILS,parameters,token));
                }catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                if(response!=null) {
                    try {
                        if (response.getBoolean("error")) {
                            String msg="Error";
                            if(response.has("message")) {
                                msg = response.getString("message");
                            }
                            txt_err_msgg.setText(msg+"");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        } else {
                            JSONArray jsonArray = response.getJSONArray("data");
                            if(jsonArray.length()!=0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Item superHero = null;
                                    JSONObject json2 = null;
                                    try {
                                        //Getting json
                                        json2 = jsonArray.getJSONObject(i);

                                        String commission = json2.getString("amount");
                                        String operator_name = json2.getString("paymentDate");
                                        superHero = new Item(operator_name, "", commission);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        txt_err_msgg.setText("Error in parsing response");
                                        linear_container.setVisibility(View.GONE);
                                        rel_no_records.setVisibility(View.VISIBLE);
                                        progress_linear.setVisibility(View.GONE);
                                    }

                                    movieList.add(superHero);
                                }

                                mAdapter = new OutstandingDetailAdapter(movieList,getActivity(),request_type);
                                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(mAdapter);
                                mAdapter.notifyDataSetChanged();
                                linear_container.setVisibility(View.VISIBLE);
                                rel_no_records.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);
                            }else{
                                txt_err_msgg.setText("No Records Found!");
                                linear_container.setVisibility(View.GONE);
                                rel_no_records.setVisibility(View.VISIBLE);
                                progress_linear.setVisibility(View.GONE);
                            }



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        txt_err_msgg.setText("Error in parsing response");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    }
                }else{
                    txt_err_msgg.setText("Empty Server Response");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }

            }
        }

        @Override
        public void onNetworkConnectionChanged(boolean isConnected) {

        }

        @Override
        public void onResume() {

            super.onResume();
            DijicashSuperStockists.getInstance().setConnectivityListener(this);
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                        Fragment frg=new OutstandingReportFragment();
                        ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg,"Home")
                                .addToBackStack(null)
                                .commit();
                        return true;
                    }

                    return false;
                }
            });
        }

        // Method to manually check connection status
        private boolean checkConnection() {
            boolean isConnected = ConnectivityReceiver.isConnected();
            return isConnected;
        }
}
