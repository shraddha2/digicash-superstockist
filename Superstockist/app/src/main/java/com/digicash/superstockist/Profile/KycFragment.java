package com.digicash.superstockist.Profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class KycFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edt_aadhar_number,edt_pan_number;
    String aadhar_number,pan_number;
    Button btn_submit;
    String token="";
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_network_msg,txt_error;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_no_internet;
    public KycFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_kyc, container, false);
        //String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle("DijiCash");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);
        edt_aadhar_number= (EditText) rootView.findViewById(R.id.edt_aadhar_number);
        edt_pan_number= (EditText) rootView.findViewById(R.id.edt_pan_number);

        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        rel_no_internet= (RelativeLayout) rootView.findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        if(checkConnection()) {
            linear_container.setVisibility(View.VISIBLE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchKYCDetails().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aadhar_number=edt_aadhar_number.getText().toString();
                pan_number=edt_pan_number.getText().toString();
                boolean isError=false;
                if(null==aadhar_number||aadhar_number.length()==0||aadhar_number.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_aadhar_number.setError("Field Cannot be Blank");
                }
                if(null==pan_number||pan_number.length()==0||pan_number.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_pan_number.setError("Field Cannot be Blank");
                }

                if(!isError) {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new EditKYCDetails().execute();
                    }else{
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        return rootView;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


    }

    private class FetchKYCDetails extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);


        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.FETCH_KYC_DETAILS,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {

            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONObject json = response.getJSONObject("data");
                        if(json.length()!=0) {

                            try {
                                String aadharNumber = json.getString("aadharNumber");
                                String panNumber = json.getString("panNumber");
                                edt_aadhar_number.setText(aadharNumber+"");
                                edt_pan_number.setText(panNumber+"");

                                linear_container.setVisibility(View.VISIBLE);
                                progress_linear.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                linear_container.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);

                            }


                        }else{
                            linear_container.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    linear_container.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                linear_container.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    private class EditKYCDetails extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("aadharNumber", aadhar_number);
                parameters.put("panNumber", pan_number);
                this.response = new JSONObject(service.POST(DefineData.EDIT_KYC_DETAILS,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {


            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("data");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    } else {
                        String msg=response.getString("data");
                        //Toast.makeText(getActivity(),"Complain Added Successfully", Toast.LENGTH_LONG).show();
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        // Intent i=new Intent(getActivity(), HomeFragment.class);
                        android.app.Fragment frg=new ProfileFragment();
                        ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg)
                                .addToBackStack(null)
                                .commit();

                      /*  txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText("Complaint Send Successfully");*/

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing responser");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {

        super.onResume();
        edt_aadhar_number.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_aadhar_number.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_pan_number.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_pan_number.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });

        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new ProfileFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }
}
