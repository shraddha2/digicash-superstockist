package com.digicash.superstockist.Contact;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018 */

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ContactUsFragment extends Fragment {


    TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_addr,txt_email,txt_contact,txt_title;
    // Typeface font ;
    public ContactUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_contact_us, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);

        new FetchContactUsDetails().execute();
        //Log.d("dhcdui","eryfhye");

        txt_label1 = (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2 = (TextView) rootView.findViewById(R.id.txt_label2);
        txt_label3 = (TextView) rootView.findViewById(R.id.txt_label3);
        txt_label4 = (TextView) rootView.findViewById(R.id.txt_label4);
        txt_addr =   (TextView) rootView.findViewById(R.id.txt_addr);
        txt_email =  (TextView) rootView.findViewById(R.id.txt_email);
        txt_contact = (TextView) rootView.findViewById(R.id.txt_contact);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);

        return rootView;
    }

    private class FetchContactUsDetails extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                //Log.d("dvsgdv","dcfsytdcc");
                this.response = new JSONObject(service.GET(DefineData.CONTACT_US));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("dgvcdgv","dcgvcgd"+response+" ");
            if(response!=null) {
                try {
                    //Log.d("dscsgd","yetytru");
                    String Name = response.getString("name");
                    String Email= response.getString("email")+"";
                    String CustomerCare= response.getString("customer_care")+"";
                    String Address= response.getString("address")+"";
                    //Log.d("Contactus: ",Name+" "+Email+" "+CustomerCare+" "+Address+" ");

                    txt_addr.setText(": "+Address+"");
                    txt_email.setText(": "+Email+"");
                    txt_contact.setText(": "+CustomerCare+"");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;
                }
                return false;
            }
        });
    }
}
