package com.digicash.superstockist.Login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class GenerateOtpActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edt_mobile_no;
    TextView txtError;
    Button btnGenerateOtp;
    String mobileNo;
    ConstraintLayout loading,content;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_otp);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        editor = sharedpreferences.edit();

        edt_mobile_no = (EditText) findViewById(R.id.edt_mobile_no);
        txtError = (TextView) findViewById(R.id.txtError);

        loading = (ConstraintLayout) findViewById(R.id.loading);
        content = (ConstraintLayout) findViewById(R.id.content);

        btnGenerateOtp = (Button) findViewById(R.id.btnGenerateOtp);
        btnGenerateOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mobileNo=edt_mobile_no.getText().toString();
                boolean isError=false;
                if(null==mobileNo||mobileNo.length()<10||mobileNo.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_mobile_no.setError("Enter 10 digit mobile no");
                }

                if(!isError) {
                    if (checkConnection()) {
                        new GenerateOTP().execute();
                    }else {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText("No Internet Connection");
                    }
                }
            }
        });
    }

    private class GenerateOTP extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("mobile_no", mobileNo);
                parameters.put("user_type", "super_stockist");
                Log.d("fjbv",mobileNo+" ");
                this.response = new JSONObject(service.POST(DefineData.GENERATE_OTP,parameters,""));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("GenerateOtp",response+"");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(response.getString("data"));
                        loading.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);
                    } else {
                        String msg=response.getString("data");
                        Toast.makeText(GenerateOtpActivity.this,msg+"", Toast.LENGTH_LONG).show();
                        Intent i=new Intent(getApplicationContext(),AuthenticateUserActivity.class);
                        i.putExtra("mobileNo",mobileNo);
                        editor.putString(DefineData.REGISTERED_MOBILE_NO, mobileNo);
                        editor.commit();
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setText("Oops! Something Went Wrong");
                    txtError.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                }
            }else{
                txtError.setText("Something Went Wrong! Try Again Later");
                txtError.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

}