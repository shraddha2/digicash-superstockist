package com.digicash.superstockist.Login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AuthenticateUserActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{

    EditText edtOtp;
    TextView txtMobileNo,txtResend,txtError,txtCheckTime,textView13;
    String mobileNo,otp;
    Button btnVerify;
    public int counter;
    ConstraintLayout loading,content;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authenticate_user_activity);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        editor = sharedpreferences.edit();

        edtOtp = (EditText) findViewById(R.id.edtOtp);
        txtError = (TextView) findViewById(R.id.txtError);
        loading = (ConstraintLayout) findViewById(R.id.loading);
        content = (ConstraintLayout) findViewById(R.id.content);

        txtMobileNo = (TextView) findViewById(R.id.txtMobileNo);
        mobileNo=sharedpreferences.getString(DefineData.REGISTERED_MOBILE_NO,"");
        Log.d("tttt",mobileNo+"");
        //mobileNo=getIntent().getStringExtra("mobileNo");
        txtMobileNo.setText("+91 "+mobileNo+" ");

        txtCheckTime = (TextView) findViewById(R.id.txtCheckTime);
        textView13 = (TextView) findViewById(R.id.textView13);
        txtCheckTime.setVisibility(View.GONE);
        txtResend = (TextView) findViewById(R.id.txtResend);
        txtResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  boolean isError=false;
                if(!isError) {
                    if (checkConnection()) {

                    }else {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText("No Internet Connection");
                    }
                }*/
                new ResendOTP().execute();
                new CountDownTimer(30000, 1000){
                    public void onTick(long millisUntilFinished){
                        txtCheckTime.setVisibility(View.VISIBLE);
                        txtCheckTime.setText("Resend OTP after "+String.valueOf(counter)+" seconds");
                        txtCheckTime.setTextColor(Color.RED);
                        txtResend.setVisibility(View.GONE);
                        textView13.setVisibility(View.GONE);
                        counter++;
                    }
                    public  void onFinish(){
                        txtCheckTime.setVisibility(View.GONE);
                        txtResend.setVisibility(View.VISIBLE);
                        textView13.setVisibility(View.VISIBLE);
                    }
                }.start();
            }
        });

        btnVerify = (Button) findViewById(R.id.btnVerify);
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otp=edtOtp.getText().toString();
                boolean isError=false;

                if(null==otp||otp.length()==0)
                {
                    isError=true;
                    edtOtp.setError("Please enter correct OTP, send on registered mobile no.");
                }
                if(!isError) {
                    if (checkConnection()) {
                        new AuthenticateOtp().execute();
                    }else {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText("No Internet Connection");
                    }
                }
            }
        });
    }

    private class ResendOTP extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("mobile_no", mobileNo);
                parameters.put("user_type", "super_stockist");
                Log.d("fjbv",mobileNo+" ");
                this.response = new JSONObject(service.POST(DefineData.GENERATE_OTP,parameters,""));
            }catch (Exception e) {
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("ResendOTP",response+"");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(response.getString("data"));
                    } else {
                        String msg=response.getString("data");
                        Toast.makeText(AuthenticateUserActivity.this,msg+"", Toast.LENGTH_LONG).show();
                       /* Intent i=new Intent(getApplicationContext(),AuthenticateUserActivity.class);
                        i.putExtra("mobileNo",mobileNo);
                        editor.putString(DefineData.KEY_MOBILENO, mobileNo);
                        editor.commit();
                        startActivity(i);
                        finish();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setText("Oops! Something Went Wrong");
                    txtError.setVisibility(View.VISIBLE);
                }
            }else{
                txtError.setText("Something Went Wrong! Try Again Later");
                txtError.setVisibility(View.VISIBLE);
            }
        }
    }

    private class AuthenticateOtp extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("mobile_no", mobileNo);
                parameters.put("user_type", "super_stockist");
                parameters.put("otp", otp);
                Log.d("Authennnn",mobileNo+" "+otp+" ");
                this.response = new JSONObject(service.POST(DefineData.AUTHENTICATE_OTP,parameters,""));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("AuthenticateOtp",response+"");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(response.getString("data"));
                    } else {
                        String msg=response.getString("data");
                        Toast.makeText(AuthenticateUserActivity.this,msg+"", Toast.LENGTH_LONG).show();
                        Intent i=new Intent(getApplicationContext(),MainActivity.class);
                        editor.putString(DefineData.REGISTERED_MOBILE_NO, mobileNo);
                        editor.commit();
                        i.putExtra("mobileNo",mobileNo);
                        startActivity(i);
                        finish();
                    }
                    loading.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setText("Oops! Something Went Wrong");
                    txtError.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                }
            }else{
                txtError.setText("Something Went Wrong! Try Again Later");
                txtError.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
