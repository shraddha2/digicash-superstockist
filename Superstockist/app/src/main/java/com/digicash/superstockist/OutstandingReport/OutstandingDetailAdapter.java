package com.digicash.superstockist.OutstandingReport;
/**
 * Created by shraddha on 20-04-2018.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

public class OutstandingDetailAdapter extends RecyclerView.Adapter<OutstandingDetailAdapter.MyViewHolder>{

    private List<Item> moviesList;
    String req_type="prepaid";
    Context ctx;
    //Typeface font ;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_operator_name, txt_commision;

        public MyViewHolder(View view) {
            super(view);
            txt_operator_name = (TextView) view.findViewById(R.id.txt_operator_name);
            txt_commision = (TextView) view.findViewById(R.id.txt_commision);

        }
    }


    public OutstandingDetailAdapter(List<Item> moviesList, Context ctx, String req_type) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.req_type=req_type;
    }

    @Override
    public OutstandingDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.outstanding_detail_layout, parent, false);

        return new OutstandingDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OutstandingDetailAdapter.MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);
        holder.txt_operator_name.setText(movie.getOperator_name());
        holder.txt_commision.setText(movie.getProfit_amt());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
