package com.digicash.superstockist.HomePage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by shraddha on 17-04-2018.
 */

public class GridRechargeAdapter extends BaseAdapter {
    // Typeface font ;
    private final Context mContext;
    List<Item> horizontalList = Collections.emptyList();

    // 1
    public GridRechargeAdapter(List<Item> horizontalList, Context context) {
        this.horizontalList = horizontalList;
        this.mContext = context;
        //  font = Typeface.createFromAsset(context.getAssets(), "font/Montserrat-Regular.ttf");

    }

    // 2
    @Override
    public int getCount() {
        return horizontalList.size();
    }

    // 3
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // 4
    @Override
    public Object getItem(int position) {
        return null;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 1
        final Item book = horizontalList.get(position);

        // 2
        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.vertical_menu, null);
        }

        // 3
        final ImageView imageView = (ImageView)convertView.findViewById(R.id.imageview);
        final TextView nameTextView = (TextView)convertView.findViewById(R.id.txtview);
        final TextView txt_id = (TextView)convertView.findViewById(R.id.txt_id);
        //nameTextView.setTypeface(font);

        Picasso.with(mContext).load(horizontalList.get(position).imageId).fit().into(imageView);
        //imageView.setImageResource();
        nameTextView.setText(horizontalList.get(position).txt);
        return convertView;
    }

    // Your "view holder" that holds references to each subview
    private class ViewHolder {
        private final TextView nameTextView;
        private final TextView txt_id;
        private final ImageView imageViewFavorite;
        //Typeface font ;
        public ViewHolder(TextView nameTextView, TextView txt_id, ImageView imageViewFavorite) {
            //font = Typeface.createFromAsset(mContext.getAssets(), "font/Montserrat-Regular.ttf");
            // nameTextView.setTypeface(font);
            this.nameTextView = nameTextView;
            this.txt_id = txt_id;
            this.imageViewFavorite = imageViewFavorite;
        }

    }

}
