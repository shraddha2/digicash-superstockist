package com.digicash.superstockist.BillHistory;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

public class BillHistoryAdapter extends RecyclerView.Adapter<BillHistoryAdapter.MyViewHolder> {

    private List<Item> billPaymentList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtOperatorName,txtAmount,txtRetailerName,txtConsumerId,txtBillPaymentId,txtTime,txtStatus;
        ImageView img_arrow;
        ConstraintLayout linear_more_data;
        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txtOperatorName = (TextView) view.findViewById(R.id.txtOperatorName);
            txtAmount = (TextView) view.findViewById(R.id.txtAmount);
            txtRetailerName = (TextView) view.findViewById(R.id.txtRetailerName);
            txtConsumerId = (TextView) view.findViewById(R.id.txtConsumerId);
            txtBillPaymentId = (TextView) view.findViewById(R.id.txtBillPaymentId);
            txtTime = (TextView) view.findViewById(R.id.txtTime);
            txtStatus = (TextView) view.findViewById(R.id.txtStatus);
            img_arrow= (ImageView) view.findViewById(R.id.img_arrow);
            linear_more_data= (ConstraintLayout) view.findViewById(R.id.linear_more_data);

            img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility =linear_more_data.getVisibility();
                    if (visibility == View.VISIBLE){
                        linear_more_data.setVisibility(View.GONE);
                        img_arrow.setImageResource(R.drawable.ic_expand_more);
                    }else{
                        linear_more_data.setVisibility(View.VISIBLE);
                        img_arrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });
        }
    }

    public BillHistoryAdapter(List<Item> billPaymentList, Context ctx, String frg_name) {
        this.billPaymentList = billPaymentList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public BillHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bill_history_layout, parent, false);

        return new BillHistoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BillHistoryAdapter.MyViewHolder holder, int position) {
        Item movie = billPaymentList.get(position);

        holder.txtOperatorName.setText(movie.getTrans_no()+"");
        holder.txtAmount.setText(movie.getTrans_type()+"");
        holder.txtRetailerName.setText("Retailer Name: "+movie.getTrans_amount());
        holder.txtConsumerId.setText("Consumer Id: "+movie.getTrans_datetime());
        holder.txtBillPaymentId.setText("Bill Payment Id: "+movie.getTrans_update_balance()+"");
        holder.txtTime.setText(movie.getStatus()+"");
        holder.txtStatus.setText(movie.getP_name()+"");

        if (movie.getP_name().equalsIgnoreCase("Failed")) {
            holder.txtAmount.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
            holder.txtStatus.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_fail));
        } else if (movie.getP_name().equalsIgnoreCase("Pending")){
            holder.txtAmount.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
            holder.txtStatus.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_initiated));
        }else{
            holder.txtAmount.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
            holder.txtStatus.setTextColor(ContextCompat.getColor(DijicashSuperStockists.getInstance(),R.color.status_success));
        }
    }

    @Override
    public int getItemCount() {
        return billPaymentList.size();
    }
}
