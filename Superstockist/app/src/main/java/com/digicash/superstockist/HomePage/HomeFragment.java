package com.digicash.superstockist.HomePage;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Home Page which includes fragment classes as: My Business Report, Retailer List, Wallet History, Fund Request, Account Report, Commission Chart, Add Retailer, Outstanding Report, Commission Report */

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.digicash.superstockist.TransactionHistory.TransactionHistoryActivity;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.digicash.superstockist.AccountReport.PaymentReportActivity;
import com.digicash.superstockist.BusinessReport.MyBusinessReportFragment;
import com.digicash.superstockist.CommissionReport.CommissionReportFragment;
import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DistributorList.DistributorListFragment;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.OutstandingReport.OutstandingReportFragment;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;
import com.digicash.superstockist.WalletHistory.WalletHistoryFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment {

    GridView gridView;
    private List<Item> data;
    Context ctx;
    SharedPreferences sharedpreferences;
    String token,notification="";
    TextView txt_title, txt_wallet_balance,txtNotification ;
    String current_balance, rechargePercent, mtPercent;
    SharedPreferences.Editor editor;
    TextView btn_more_details;
    private PieChart mChart;
    Double retailerPercentage=0.0, mtPercentage=0.0,bpPercentage=0.0;
    ImageView imgRefreshChart;
    ProgressBar progressBar;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_home, container, false);

        getActivity().setTitle("DijiCash");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        current_balance=sharedpreferences.getString(DefineData.KEY_WALLET_BALANCE,"");
        rechargePercent=sharedpreferences.getString(DefineData.KEY_PERCENTAGE,"");
        mtPercent=sharedpreferences.getString(DefineData.KEY_PERCENTAGE,"");
        //txt_wallet_balance=(TextView) rootView.findViewById(R.id.txt_wallet_balance);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txtNotification = (TextView) rootView.findViewById(R.id.txtNotification);
        txtNotification.setSelected(true);

        //txt_wallet_balance.setText("...");
        btn_more_details = (TextView) rootView.findViewById(R.id.btn_more_details);

        gridView = (GridView) rootView.findViewById(R.id.gridview);
        txt_title=(TextView)rootView.findViewById(R.id.txt_title);
        // setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        gridView = (GridView) rootView.findViewById(R.id.gridview);

        new FetchBalance().execute();
        new FetchNotification().execute();
        new ShowChart().execute();
        /*imgRefreshChart = (ImageView) rootView.findViewById(R.id.imgRefreshChart);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        imgRefreshChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("sdcsg","sdgcsyu");
                new ShowChart().execute();
            }
        });*/

        data = fill_with_data();
        ctx=getActivity();
        GridRechargeAdapter booksAdapter = new GridRechargeAdapter(data, getActivity());
        gridView.setAdapter(booksAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String list = data.get(position).txt.toString();

                if(list.equalsIgnoreCase("Transaction History"))
                {
                    Intent i=new Intent(getActivity().getApplicationContext(), TransactionHistoryActivity.class);
                    startActivity(i);
                }
                if(list.equalsIgnoreCase("Wallet History"))
                {
                    android.app.Fragment frg=new WalletHistoryFragment();

                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
                if(list.equalsIgnoreCase("Distributor List"))
                {
                    android.app.Fragment frg=new DistributorListFragment();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
                if(list.equalsIgnoreCase("Payment Report"))
                {
                    Intent i=new Intent(getActivity().getApplicationContext(), PaymentReportActivity.class);
                    startActivity(i);
                }
                if(list.equalsIgnoreCase("Outstanding Report"))
                {
                    android.app.Fragment frg=new OutstandingReportFragment();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
                if(list.equalsIgnoreCase("Commission Report"))
                {
                    android.app.Fragment frg=new CommissionReportFragment();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
                /*if(list.equalsIgnoreCase("Bill History"))
                {
                    android.app.Fragment frg=new BillHistoryFragment();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
                if(list.equalsIgnoreCase("DthBooking\nHistory"))
                {
                    Fragment frg=new DthBookingFragment();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }*/
            }
        });

        /*-- Pie Chart Code --*/
        mChart = (PieChart) rootView.findViewById(R.id.chart1);
        //mChart.setBackgroundColor(Color.WHITE);

        moveOffScreen();

        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setDrawHoleEnabled(false);

        //To the make the chart half
        mChart.setMaxAngle(180);
        mChart.setRotationAngle(180);
        mChart.setCenterTextOffset(0, -20);
        mChart.setRotationEnabled(false); //this will stop rotating the pie chart

        //setData(2, 100); //this will set data to 4 parts of 100%

        //Animation
        mChart.animateY(1000, Easing.EasingOption.EaseInBack.EaseInOutCubic);  //animate with 1000 ms

        //set legend(i.e. display the category in the box at top to specify which country belong to which color)
        /*Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setYOffset(40f); //this will display the category 50f down*/

        //li.setBackgroundColor(Color.rgb(226, 11, 11));
        mChart.setEntryLabelColor(Color.WHITE);
        mChart.setEntryLabelTextSize(12f);

        btn_more_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment frg=new MyBusinessReportFragment();
                ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                        .replace(R.id.frg_replace, frg)
                        .addToBackStack(null)
                        .commit();
            }
        });

        return rootView;
    }

    public List<Item> fill_with_data() {

        List<Item> data = new ArrayList<>();

        data.add(new Item( R.drawable.ic_transaction_history, "Transaction History","1"));
        data.add(new Item( R.drawable.ic_wallet_history, "Wallet History","2"));
        data.add(new Item( R.drawable.ic_retailer_list, "Distributor List","3"));
        data.add(new Item( R.drawable.account_report, "Payment Report","4"));
        data.add(new Item( R.drawable.outstanding, "Outstanding Report","5"));
        data.add(new Item( R.drawable.commission, "Commission Report","6"));

        return data;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).hideKeyboard(getActivity());
        ((HomeActivity) getActivity()).selectNavigationDrawerItem(0);

    }

    private class FetchBalance extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.FETCH_BALANCE,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                String balance="";
                try {
                    if (response.getBoolean("error")) {
                        balance="...";
                    } else {
                        //balance= String.format( "%.2f", response.getDouble("balance"))+"";
                        balance=response.getString("balance");
                    }
                    current_balance=balance;

                    ((HomeActivity) getActivity()).updateWalletBalance(balance);
                    editor.putString(DefineData.KEY_WALLET_BALANCE, balance);
                    editor.commit();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class FetchNotification extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("userType", "ss");
                this.response = new JSONObject(service.POST(DefineData.FETCH_NOTIFICATION,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                String announcement="";
                try {
                    if (response.getBoolean("error")) {

                    } else {
                        announcement= response.getString("data")+"";
                    }
                    notification=announcement;
                    txtNotification.setText(notification+"");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class ShowChart extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            //progressBar.setVisibility(View.VISIBLE);
            //mChart.setVisibility(View.GONE);
            //imgRefreshChart.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                //Log.d("dchsgc","ccxccxcxc");
                this.response = new JSONObject(service.POST(DefineData.FETCH_PERCENTAGE,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                retailerPercentage=0.0;
                mtPercentage=0.0;
                bpPercentage=0.0;
                try {
                    if (response.getBoolean("error")) {
                        //progressBar.setVisibility(View.GONE);
                        //mChart.setVisibility(View.VISIBLE);
                        //imgRefreshChart.setVisibility(View.VISIBLE);
                    } else {
                        retailerPercentage= response.getDouble("rechargePercentage");
                        mtPercentage= response.getDouble("mtPercentage");
                        bpPercentage= response.getDouble("bpPercentage");
                        setData(3, 100); //this will set data to 4 parts of 100%
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //progressBar.setVisibility(View.GONE);
                    //mChart.setVisibility(View.VISIBLE);
                    //imgRefreshChart.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    /*-- Pie Chart Code --*/
    String[] countries = new String[]{"Recharge", "MT", "BBPS", "DTH", "BUS"};  //Array of string
    private void setData(int count, int range){
        ArrayList<PieEntry> values = new ArrayList<>();

        values.add(new PieEntry(Float.valueOf(String.valueOf(retailerPercentage)), countries[0]));
        values.add(new PieEntry(Float.valueOf(String.valueOf(mtPercentage)), countries[1]));
        values.add(new PieEntry(Float.valueOf(String.valueOf(bpPercentage)), countries[2]));

        PieDataSet dataSet = new PieDataSet(values, "Sales");  //partner is the label of the pie chart
        dataSet.setSelectionShift(5f);  //gape between the chart chart nd on click of that
        dataSet.setSliceSpace(3f);
        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);  //color of the set

        //pie data
        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter()); //Will Display value in percentage
        data.setValueTextSize(15f);
        data.setValueTextColor(Color.WHITE);

        mChart.setData(data);
        mChart.invalidate();
    }

    //moving the pie chart (i.e. top, bottom nd so on)
    private void moveOffScreen(){
        Display display = getActivity().getWindowManager().getDefaultDisplay(); //Display Size
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);  //pass the matrix in the window
        int height = metrics.heightPixels;    //Display height of the matrix

        //int offset = (int) (height*-0.2); //this chart will becm on top of the display)
        int offset = (int) (height*0.5); //this chart will be somewhere middle of the display(i.e. half(0.5))
        mChart.setExtraOffsets(0,10,0,0);
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams)mChart.getLayoutParams();//Relative layout parameter(params)
        params.setMargins(0, 0, 0, -offset); //this will move the pie from the middle to the end
        mChart.setLayoutParams(params);

    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
}