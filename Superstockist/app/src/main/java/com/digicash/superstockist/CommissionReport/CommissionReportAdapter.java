package com.digicash.superstockist.CommissionReport;
/**
 * Created by shraddha on 18-04-2018.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

public class CommissionReportAdapter extends RecyclerView.Adapter<CommissionReportAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    //Typeface font ;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_operator_name,txt_sales,txt_profit;



        public MyViewHolder(View view) {
            super(view);
            txt_operator_name = (TextView) view.findViewById(R.id.txt_operator_name);
            txt_sales = (TextView) view.findViewById(R.id.txt_sales);
            txt_profit = (TextView) view.findViewById(R.id.txt_profit);

        }
    }


    public CommissionReportAdapter(List<Item> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.commission_report_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);
        holder.txt_operator_name.setText(movie.getOperator_name());
        holder.txt_sales.setText(movie.getSale_amt());
        holder.txt_profit.setText(movie.getProfit_amt());


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
