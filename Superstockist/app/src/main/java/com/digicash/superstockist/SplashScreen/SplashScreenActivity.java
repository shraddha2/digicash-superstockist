package com.digicash.superstockist.SplashScreen;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Splash Screen which will appear for 3000 seconds*/

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.Login.EnterPasswordActivity;
import com.digicash.superstockist.Login.GenerateOtpActivity;
import com.digicash.superstockist.Login.MainActivity;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.digicash.superstockist.DefineData.LOGIN_MINKSPAY_PREFERENCE;

public class SplashScreenActivity extends AppCompatActivity {

    ImageView myImage;
    TextView txt_label,txtErrorMsg;
    private static int SPLASH_TIME_OUT = 3000;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    boolean loggedin=false,key_rember=false;
    private FirebaseAnalytics mFirebaseAnalytics;
    String token;
    int latest_version=3,current_version=4;
    int currentVersion=3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        sharedpreferences = getSharedPreferences(LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        loggedin=sharedpreferences.getBoolean(DefineData.KEY_LOGIN_SUCCESS,false);
        key_rember=sharedpreferences.getBoolean(DefineData.KEY_REMEMBER_ME,false);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        myImage = (ImageView)findViewById(R.id.img_logo);
        txt_label = (TextView)findViewById(R.id.txt_app_label);

        latest_version=current_version;

        final Animation myRotation2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide);
        txt_label.startAnimation(myRotation2);

        new FetchBalance().execute();

        txtErrorMsg= (TextView) findViewById(R.id.txtErrorMsg);
        txtErrorMsg.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               /* Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();*/
                new SubmitDetails().execute();

            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private class SubmitDetails extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("app_key", "ss");
                this.response = new JSONObject(service.POST(DefineData.CHECK_VERSION,parameters,""));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("data");
                        txtErrorMsg.setVisibility(View.VISIBLE);
                        txtErrorMsg.setText(message+"");
                    } else {
                        JSONObject json_data=response.getJSONObject("data");
                        String version=json_data.getString("version");

                        if (Integer.parseInt(version.toString()) > currentVersion){
                            txtErrorMsg.setVisibility(View.VISIBLE);
                            //txtErrorMsg.setText("Please Update the app from play store");
                            txtErrorMsg.setMovementMethod(LinkMovementMethod.getInstance());
                        }
                        else {
                            /*Intent i = new Intent(getApplicationContext(), GenerateOtpActivity.class);
                            startActivity(i);
                            finish();*/
                            if(! sharedpreferences.getBoolean(DefineData.KEY_REMEMBER_PIN,false)) {
                                logout();
                                Log.d("mmmmmm","serdfsfxfzafrsdf");
                                Intent mainIntent = new Intent(SplashScreenActivity.this, GenerateOtpActivity.class);
                                startActivity(mainIntent);
                                finish();
                            }
                            else
                            {
                                Log.d("uuuuuu","fdjndjk");
                                startActivity(new Intent(SplashScreenActivity.this, HomeActivity.class));
                                finish();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtErrorMsg.setVisibility(View.VISIBLE);
                    txtErrorMsg.setText("Error in parsing response");
                }
            }else{
            }
        }
    }

    public void logout() {
        SharedPreferences sharedPreferences = SplashScreenActivity.this.getSharedPreferences(LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    private class FetchBalance extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{

                this.response = new JSONObject(service.GET(DefineData.FETCH_VERSION));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {

                    } else {
                        latest_version=response.getInt("version");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}