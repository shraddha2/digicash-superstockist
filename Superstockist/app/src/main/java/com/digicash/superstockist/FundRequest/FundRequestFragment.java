package com.digicash.superstockist.FundRequest;

/*Edited By: Shraddha Shetkar
  Date: 18/04/2018
  Des: Form which allows to send the request
  Mandatory Fields: Select Beneficiary Bank, Deposit At, Amount and Comment*/

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.ForgotPassword.ForgotPasswordActivity;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

public class FundRequestFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    TextView edt_benf_name_admin,txt_label, txt_label2, txt_label3, txt_label4, txt_label5, txt_label6, txt_error, edt_acct_no, txt_label7;
    EditText  txt_from_date, edt_deposit_at, edt_remark, edt_amount,edtTransactionRefId;
    String benf_name, from_date, deposit_at, remark, amount, trans_type="", trans_type2="",transactionRefId;
    RadioGroup radioGroup, radioGroup2;
    RadioButton admin,distributor,cash,bank_transfer,online_transfer;
    Button btn_submit;
    SharedPreferences sharedpreferences;
    ConstraintLayout progress_linear,linear_container,layout_bank_transfer,rel_no_records,rel_no_internet;
    TextView txt_msgg,txt_err_msgg,txt_network_msg;
    String token, bank_id="", bank_name, start_date;

    public FundRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_fund_request, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);
        setHasOptionsMenu(true);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        radioGroup=(RadioGroup) rootView.findViewById(R.id.radioGroup);
        layout_bank_transfer= (ConstraintLayout) rootView.findViewById(R.id.layout_bank_transfer);
        radioGroup2=(RadioGroup) rootView.findViewById(R.id.radioGroup2);
        cash=(RadioButton) rootView.findViewById(R.id.cash);
        bank_transfer=(RadioButton) rootView.findViewById(R.id.bank_transfer);
        online_transfer=(RadioButton) rootView.findViewById(R.id.online_transfer);
        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        //edt_acct_no= (TextView) rootView.findViewById(R.id.edt_acct_no);
        txt_label3=  (TextView) rootView.findViewById(R.id.txt_label3);
        edt_benf_name_admin=  (TextView) rootView.findViewById(R.id.edt_benf_name_admin);
        edtTransactionRefId = (EditText) rootView.findViewById(R.id.edtTransactionRefId);
        txt_from_date =  (EditText) rootView.findViewById(R.id.txt_from_date);
        //edt_deposit_at = (EditText) rootView.findViewById(R.id.edt_deposit_at);
        edt_remark = (EditText) rootView.findViewById(R.id.edt_remark);
        edt_amount = (EditText) rootView.findViewById(R.id.edt_amount);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);

        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        //rel_img_bg= (RelativeLayout) rootView.findViewById(R.id.rel_img_bg);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        txt_error.setVisibility(View.GONE);
        //linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        //rel_img_bg.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

/*
        if(checkConnection()) {
            txt_error.setVisibility(View.GONE);
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            //rel_img_bg.setVisibility(View.VISIBLE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new SubmitDetails().execute();
        }else{
            txt_error.setVisibility(View.GONE);
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            //rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }*/

        layout_bank_transfer.setVisibility(View.GONE);

        edt_benf_name_admin.setInputType(InputType.TYPE_NULL);
        edt_benf_name_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (movie.getLabel8().equalsIgnoreCase("Failed")) {
                //request_type="recharge"
                Intent intent=new Intent(getActivity(),FundBankDetails.class);
                //intent.putExtra("source","fundReq");
                startActivityForResult(intent, 6);
            }
        });

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId2) {
                // checkedId is the RadioButton selected

                switch(checkedId2) {
                    case R.id.cash:
                        // switch to fragment 1
                        layout_bank_transfer.setVisibility(View.GONE);
                        break;

                    case R.id.bank_transfer:
                        // Fragment 2
                        layout_bank_transfer.setVisibility(View.VISIBLE);
                        break;

                    case R.id.online_transfer:
                        layout_bank_transfer.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        start_date=DefineData.getCurrentDate();
        txt_from_date.setInputType(InputType.TYPE_NULL);
        txt_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),R.style.MyCalendarStyle,new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;

                        txt_from_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));


                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("Start Date");
                mDatePicker.show();  }

        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_date=DefineData.parseDateToyyyMMdd(txt_from_date.getText().toString());

                int  selectedValueId2 = radioGroup2.getCheckedRadioButtonId();
                if(selectedValueId2 == cash.getId())
                {
                    trans_type2="cash"; //bank_transfer
                }
                else if(selectedValueId2 == bank_transfer.getId())
                {
                    trans_type2="bank_transfer"; //cash
                }
                else if(selectedValueId2 == online_transfer.getId())
                {
                    trans_type2="online_transfer";
                }
                else{
                    trans_type2="";
                }

                benf_name=edt_benf_name_admin.getText().toString();
                from_date=txt_from_date.getText().toString();
                //deposit_at=edt_deposit_at.getText().toString();
                remark=edt_remark.getText().toString();
                amount=edt_amount.getText().toString();
                transactionRefId = edtTransactionRefId.getText().toString();

                boolean isError=false;
                if(layout_bank_transfer.getVisibility()==View.VISIBLE)
                {
                    final String admin_bank_name = edt_benf_name_admin.getText().toString();
                    if (!isValidadminbank(admin_bank_name)) {
                        edt_benf_name_admin.setError("Field Cannot be Blank");
                        isError=true;
                    }
                    if (edtTransactionRefId.getVisibility()==View.VISIBLE){
                        if (validTransactionRefId(transactionRefId)) {
                            edtTransactionRefId.setError("Field Cannot be Blank");
                            isError=true;
                        }
                    }
                } else{
                    isError=false;
                }
                if(null==trans_type2||trans_type2.length()==0||trans_type2.equalsIgnoreCase(""))
                {
                    isError=true;
                    cash.setError("Field Cannot be Blank");
                }
                if(null==remark||remark.length()==0||remark.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_remark.setError("Field Cannot be Blank");
                }
                if(null==amount||amount.length()==0||amount.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_amount.setError("Field Cannot be Blank");
                }
                if(!isError)
                {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        new SubmitDetails().execute();
                    }else{
                        txt_error.setText("No internet Connection");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    }
                }
            }
        });
        return rootView;
    }

    //validate admin bank
    private boolean isValidadminbank(String admin_bank_name) {
        if (admin_bank_name == null || admin_bank_name.length() > 0) {
            return true;
        }
        return false;
    }
    private boolean validTransactionRefId(String transactionRefId) {
        if (transactionRefId == null || transactionRefId.length() == 0) {
            return true;
        }
        return false;
    }

    private class SubmitDetails extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("bankId", bank_id);
                parameters.put("amount", amount);
                parameters.put("depositedAt", start_date);
                parameters.put("payment_mode", trans_type2);//payment_mode
                parameters.put("transactionRefId", transactionRefId);
                parameters.put("remarks", remark);
                this.response = new JSONObject(service.POST(DefineData.SUBMIT_DETAILS,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("dfvndx",response+" "+bank_id+" "+amount+" "+start_date+" "+trans_type2+" "+transactionRefId+" "+remark+" ");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("message");
                        txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText(message+"");
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        String msg=response.getString("data");
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        Intent i=new Intent(getActivity(), HomeActivity.class);
                        getActivity().startActivity(i);
                        getActivity().finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                txt_error.setVisibility(View.VISIBLE);
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().clearFocus();

        edt_benf_name_admin.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_benf_name_admin.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        txt_from_date.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    txt_from_date.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_amount.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_amount.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_remark.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_remark.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });

    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case 6:
                if (null!=data) {
                    bank_id = data.getStringExtra("operator_circle_id");
                    bank_name = data.getStringExtra("operator_circle_name");
                    edt_benf_name_admin.setText(bank_name + "");
                }
                break;
        }
    }
}