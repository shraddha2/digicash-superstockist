package com.digicash.superstockist.AccountReport;

// credit=1 debit=2

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.DividerItemDecoration;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class DistributorPaymentReportFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    private List<Item> outstandingList = new ArrayList<>();
    private RecyclerView recyclerView;
    private OutstandingReportAdapter mAdapter;

    TextView txt_from_date,txt_to_date,Label1,Label2,Label3,Label4;
    DateFormat inputFormat,outputFormat;
    String start_date,end_date, token;
    SharedPreferences sharedpreferences;
    ImageView btn_search;

    ConstraintLayout progress_linear,linear_container,rel_img_bg,rel_no_records,rel_no_internet;
    TextView txt_err_msgg,txt_network_msg;

    public DistributorPaymentReportFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_distributor_payment_report, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rvOutstandingReport);
        txt_from_date= (TextView) rootView.findViewById(R.id.txt_from_date);
        txt_to_date= (TextView) rootView.findViewById(R.id.txt_to_date);
        Label1=(TextView) rootView.findViewById(R.id.Label1);
        Label2=(TextView) rootView.findViewById(R.id.Label2);
        Label3=(TextView) rootView.findViewById(R.id.Label3);
        Label4=(TextView) rootView.findViewById(R.id.Label4);
        btn_search= (ImageView) rootView.findViewById(R.id.btn_search);

        inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        outputFormat = new SimpleDateFormat("dd MMM yyyy");

        txt_to_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        start_date=DefineData.getCurrentDate();
        end_date=start_date;
        mAdapter = new OutstandingReportAdapter(outstandingList,getActivity());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_img_bg= (ConstraintLayout) rootView.findViewById(R.id.rel_img_bg);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        rel_img_bg.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        txt_to_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        start_date=DefineData.getCurrentDate();
        end_date=start_date;

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_date=DefineData.parseDateToyyyMMdd(txt_from_date.getText().toString());
                end_date=DefineData.parseDateToyyyMMdd(txt_to_date.getText().toString());
                if(checkConnection()) {
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.GONE);
                    rel_img_bg.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.GONE);
                    new FetchCommisionsReport().execute();
                }else{
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.GONE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.VISIBLE);
                }

            }
        });

        txt_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate= Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),R.style.MyCalendarStyle,new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;
                        txt_from_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));
                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("Start Date");
                mDatePicker.show();  }

        });

        txt_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate= Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),R.style.MyCalendarStyle,new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;
                        txt_to_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));
                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("End Date");
                mDatePicker.show();  }

        });

      /*  if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.VISIBLE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            outstandingList.clear();
            new FetchCommisionsReport().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }*/
        return rootView;
    }

    private class FetchCommisionsReport extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            outstandingList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("startDate", start_date);
                parameters.put("endDate", end_date);
                this.response = new JSONObject(service.POST(DefineData.FETCH_OUTSTANDING_REPORT,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("uriesdf",start_date+" "+end_date+" "+ response);
            if(response!=null) if(response!=null) {
                try {
                   /* if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {*/

                    JSONArray jsonArray = response.getJSONArray("data");
                    if (jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject json1 = null;
                            try {
                                json1 = jsonArray.getJSONObject(i);
                                JSONArray payments = json1.getJSONArray("payments");
                                String total_outstanding = json1.getString("totalOutstanding");
                                String total_paid = json1.getString("totalPaid");
                                String total_left = json1.getString("totalLeft");
                                String paid_during_period = json1.getString("paidDuringPeriod");

                                Label1.setText("Total Outstanding : ₹. "+total_outstanding);
                                Label2.setText("Total Paid : ₹. "+total_paid);
                                Label3.setText("Total Left : ₹. "+total_left);
                                Label4.setText("Paid During Period : ₹. "+paid_during_period);

                                Log.d("eiytiu",total_outstanding+" "+total_paid+" "+total_left+" "+paid_during_period);
                                if (payments.length() != 0) {
                                    for (int j = 0; j < payments.length(); j++) {
                                        Item superHero = null;
                                        JSONObject json2 = null;
                                        try {
                                            //Getting json
                                            json2 = payments.getJSONObject(j);
                                            String Id = json2.getString("id");
                                            String Amount = json2.getString("amount");
                                            String PaymentDate = json2.getString("payment_date");
                                            String PaymentMode = json2.getString("payment_mode");
                                            String Bank = json2.getString("bank");
                                            superHero = new Item(Id,"Mode: " + PaymentMode ,"₹."+ Amount,"Date of Payment:"+ PaymentDate, "Bank: " + Bank);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            txt_err_msgg.setText("Something Went Wrong! Try Again");
                                            linear_container.setVisibility(View.GONE);
                                            rel_no_records.setVisibility(View.VISIBLE);
                                            rel_img_bg.setVisibility(View.GONE);
                                            progress_linear.setVisibility(View.GONE);
                                        }
                                        outstandingList.add(superHero);
                                    }
                                }
                                mAdapter.notifyDataSetChanged();
                                linear_container.setVisibility(View.VISIBLE);
                                rel_no_records.setVisibility(View.GONE);
                                rel_img_bg.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                txt_err_msgg.setText("Something Went Wrong! Try Again");
                                linear_container.setVisibility(View.GONE);
                                rel_no_records.setVisibility(View.VISIBLE);
                                rel_img_bg.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);
                            }
                        }
                    }
                    /*}*/
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Something Went Wrong! Try Again");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                }

            }else{
                txt_err_msgg.setText("Empty server response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                rel_img_bg.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
    }
}
