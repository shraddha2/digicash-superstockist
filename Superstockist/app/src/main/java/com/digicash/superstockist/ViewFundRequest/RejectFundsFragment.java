package com.digicash.superstockist.ViewFundRequest;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RejectFundsFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edit_comment;
    TextView txt_error,txt_network_msg;
    String comment;
    Button btn_submit;
    String token="", trans_type="";
    String id="",balance="",name= "";
    SharedPreferences sharedpreferences;
    ConstraintLayout progress_linear,linear_container,rel_no_internet;
    RadioGroup radioGroup;
    RadioButton rb_rec,rb_credit;

    public RejectFundsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_reject_funds, container, false);
        //String mess = getResources().getString(R.string.app_name);
        Bundle bundle = getArguments();
        getActivity().setTitle("DijiCash");

        id=bundle.getString("id");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        edit_comment= (EditText) rootView.findViewById(R.id.edit_comment);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);
        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment=edit_comment.getText().toString();
                //checking the id of the selected radio

                if (checkConnection()) {
                    boolean isError=false;
                    if(null==comment||comment.length()==0||comment.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edit_comment.setError("Field Cannot be Blank");

                    }

                    if(!isError) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new RejectFunds().execute();
                    }
                }
                else{
                    linear_container.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.VISIBLE);
                }

            }
        });
        return rootView;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


    }

    private class RejectFunds extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("fundRequestId", id);
                parameters.put("comments", comment);
                this.response = new JSONObject(service.POST(DefineData.REJECT_FUNDS,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {

            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("message");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    } else {

                        String msg=response.getString("message");
                        //Toast.makeText(getActivity(),"Complain Added Successfully", Toast.LENGTH_LONG).show();
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        Fragment frg=new ViewFundRequestFragment();
                        ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg)
                                .addToBackStack(null)
                                .commit();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing responser");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {

        super.onResume();
        edit_comment.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_comment.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });

        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new ViewFundRequestFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;
                }
                return false;
            }
        });
    }
}
