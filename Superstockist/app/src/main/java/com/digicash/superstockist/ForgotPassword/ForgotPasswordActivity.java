package com.digicash.superstockist.ForgotPassword;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Updating of the password
  Mandatory Fields: Otp, New Password*/

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Login.MainActivity;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ForgotPasswordActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edt_new_pass,edt_confirm_pass,edt_otp;
    Button btn_submit;
    String new_password,confirm_password,otp;
    String mobile_no;
    TextView txt_error;
    LinearLayout progress_linear,linear_container;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        editor = sharedpreferences.edit();

        getSupportActionBar().setTitle("DijiCash");

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //mobile_no=getIntent().getStringExtra("phoneNumber");
        mobile_no=sharedpreferences.getString(DefineData.REGISTERED_MOBILE_NO,"");
        btn_submit= (Button) findViewById(R.id.btn_submit);
        edt_otp= (EditText) findViewById(R.id.edt_otp);
        edt_new_pass= (EditText) findViewById(R.id.edt_new_pass);
        edt_confirm_pass= (EditText) findViewById(R.id.edt_confirm_pass);
        progress_linear= (LinearLayout) findViewById(R.id.loding);
        linear_container= (LinearLayout) findViewById(R.id.container);
        txt_error= (TextView) findViewById(R.id.txt_error);
        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new_password=edt_new_pass.getText().toString();
                confirm_password=edt_confirm_pass.getText().toString();
                otp=edt_otp.getText().toString();
                boolean isError=false;
                if(null==otp||otp.length()==0)
                {
                    isError=true;
                    edt_otp.setError("Field Cannot be Blank");
                }
                if(null==new_password||new_password.length()==0||new_password.length()<6)
                {
                    isError=true;

                    edt_new_pass.setError("Password Cannot be less than 6-letters");

                }
                if(null==confirm_password||confirm_password.length()==0||!confirm_password.equalsIgnoreCase(new_password))
                {
                    isError=true;
                    edt_confirm_pass.setError("Password Not Matching");
                }
                if(!isError) {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        new ForgotPassword().execute();
                    }else{
                        txt_error.setText("No internet Connection");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent =new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                //return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
    }

    private class ForgotPassword extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("otp", otp);
                parameters.put("phoneNumber", mobile_no);
                parameters.put("newPassword", new_password);
                this.response = new JSONObject(service.POST(DefineData.OTP_FORGOT_PASSWORD,parameters,""));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("data");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(getApplicationContext(),"Password Updated Successfully", Toast.LENGTH_LONG).show();
                        editor.putString(DefineData.REGISTERED_MOBILE_NO, mobile_no);
                        editor.commit();
                        Intent i=new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
    @Override
    public void onResume() {
        super.onResume();

        // register connection status listener
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent =new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}