package com.digicash.superstockist.CommissionChart;

/*Edited By: Shraddha Shetkar
  Date: 18/04/2018
  Des: display the commission chart based on the request type i.e. recharge or money transfer*/

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Model.DividerItemDecoration;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommissionChartFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener,View.OnClickListener {
    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CommissionChartAdapter mAdapter;
    String token,request_type="prepaid";
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg,txt_label2;
    ConstraintLayout progress_linear,linear_container,rel_no_records,rel_no_internet;
    private Button[] btn = new Button[5];
    private Button btn_unfocus;
    private int[] btn_id = {R.id.btn0, R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4};

    public CommissionChartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_commission_chart, container, false);
        //String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle("DijiCash");
        // setHasOptionsMenu(true);

        for(int i = 0; i < btn.length; i++){
            btn[i] = (Button) rootView.findViewById(btn_id[i]);
            btn[i].setBackgroundColor(Color.rgb(255,255,255));
            btn[i].setOnClickListener(this);
        }

        btn_unfocus = btn[0];

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_commission_chart);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2= (TextView) rootView.findViewById(R.id.txt_label2);

        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        setFocus(btn_unfocus, btn[0]);
        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchCommisions().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }


        return rootView;
    }


    @Override
    public void onClick(View v) {
        //setForcus(btn_unfocus, (Button) findViewById(v.getId()));
        //Or use switch
        switch (v.getId()){
            case R.id.btn0 :
                request_type="prepaid";
                setFocus(btn_unfocus, btn[0]);
                txt_label1.setText("Operator");
                txt_label2.setText("Commission (%)");
                break;
            case R.id.btn1 :
                request_type="postpaid";
                setFocus(btn_unfocus, btn[1]);
                txt_label1.setText("Operator");
                txt_label2.setText("Commission (₹)");
                break;
            case R.id.btn2 :
                request_type="mt";
                setFocus(btn_unfocus, btn[2]);
                txt_label1.setText("Range");
                txt_label2.setText("Commission");
                break;
            case R.id.btn3 :
                request_type="bill-payment";
                setFocus(btn_unfocus, btn[3]);
                txt_label1.setText("Operator");
                txt_label2.setText("Commission");
                break;
            case R.id.btn4 :
                request_type="dth-booking";
                setFocus(btn_unfocus, btn[4]);
                txt_label1.setText("Operator");
                txt_label2.setText("Commission (₹)");
                break;
        }

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchCommisions().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }
    }

    private void setFocus(Button btn_unfocus, Button btn_focus){
        btn_unfocus.setTextColor(Color.rgb(49, 50, 51));
        btn_focus.setTextColor(Color.rgb(255,255,255));
        btn_unfocus.setBackgroundColor(Color.rgb(255,255,255));
        btn_focus.setBackgroundColor(Color.rgb(242, 109, 109));
        this.btn_unfocus = btn_focus;
    }

    private class FetchCommisions extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("type", request_type);
                this.response = new JSONObject(service.POST(DefineData.FETCH_COMMISIONS,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("message")) {
                            msg = response.getString("message");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = response.getJSONArray("data");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    //Getting json
                                    json2 = jsonArray.getJSONObject(i);

                                    String commission = json2.getString("commission");

                                    if(request_type.equalsIgnoreCase("prepaid")||request_type.equalsIgnoreCase("postpaid")||request_type.equalsIgnoreCase("bill-payment")||request_type.equalsIgnoreCase("dth-booking")) {
                                        String operator_id = json2.getString("operatorId");
                                        String operator_name = json2.getString("operatorName");
                                        superHero = new Item(operator_name, operator_id, commission);
                                    }

                                    if(request_type.equalsIgnoreCase("mt")) {
                                        String start_range = json2.getString("rangeStart");
                                        String end_range = json2.getString("rangeEnd");
                                        superHero = new Item(start_range+" - "+end_range, end_range, commission);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setText("Error in parsing response");
                                    linear_container.setVisibility(View.GONE);
                                    rel_no_records.setVisibility(View.VISIBLE);
                                    progress_linear.setVisibility(View.GONE);
                                }

                                movieList.add(superHero);
                            }

                              /*if(request_type.equalsIgnoreCase("postpaid"))
                            {
                                Item superHero = new Item("BBPS", "25", "- Rs.5");
                                movieList.add(superHero);
                            }*/

                            mAdapter = new CommissionChartAdapter(movieList,getActivity(),request_type);
                            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                            linear_container.setVisibility(View.VISIBLE);
                            rel_no_records.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }else{
                            txt_err_msgg.setText("No Records Found!");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Error in parsing response");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onResume() {

        super.onResume();
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();
                    return true;
                }

                return false;
            }
        });
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
}
