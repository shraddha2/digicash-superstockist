package com.digicash.superstockist.Model;

public class Item implements Comparable<Item> {
        public int imageId;
        public String txt,txt_id;
        public String operator_name,commission,sale_amt, limitLeft,profit_amt,p_name,acno;
        public String label1,label2,label3,label4,label5,label6,label7,label8,label9,label10,dattime;
        public String trans_no,trans_type,trans_amount,trans_datetime,trans_update_balance,status,comment;
        public String type,amt,balance,debit_credit_type;
        int credit_type;

        public Item()
        {}

        public Item(String type,String amt,String balance,int credit_type,String dattime) {
                this.type=type;
                this.amt=amt;
                this.balance=balance;
                this.credit_type=credit_type;
                this.dattime=dattime;
        }

        public String getDebit_credit_type() {
                return debit_credit_type;
        }

        public Item(String type, String amt, String balance, String debit_credit_type, String dattime) {
                this.type=type;
                this.amt=amt;
                this.balance=balance;
                this.debit_credit_type=debit_credit_type;
                this.dattime=dattime;
        }

        public Item(String label1, String label2,String label3,String label4,String label5,String label6,String label7,String label8,String label9) {
                this.label1=label1;
                this.label2=label2;
                this.label3=label3;
                this.label4=label4;
                this.label5=label5;
                this.label6=label6;
                this.label7=label7;
                this.label8=label8;
                this.label9=label9;
        }

        public String getLabel10() {
                return label10;
        }

        public void setLabel10(String label10) {
                this.label10 = label10;
        }

        public Item(String label1, String label2, String label3, String label4, String label5, String label6, String label7, String label8, String label9, String label10) {
                this.label1=label1;
                this.label2=label2;
                this.label3=label3;
                this.label4=label4;
                this.label5=label5;
                this.label6=label6;
                this.label7=label7;
                this.label8=label8;
                this.label9=label9;
                this.label10=label10;
        }
        public void setTxt(String txt) {
                this.txt = txt;
        }

        public void setTxt_id(String txt_id) {
                this.txt_id = txt_id;
        }

        public Item(String label1, String label2, String label3, String dattime) {
                this.label1=label1;
                this.label2=label2;
                this.label3=label3;
                this.dattime=dattime;

        }
        @Override
        public int compareTo(Item o) {
                return getDattime().compareTo(o.getDattime());
        }
        public Item( int imageId, String text,String txt_id) {

                this.imageId = imageId;
                this.txt=text;
                this.txt_id=txt_id;
        }

        public int getCredit_type() {
                return credit_type;
        }

        public String getAmt() {
                return amt;
        }

        public String getBalance() {
                return balance;
        }

        public String getComment() {
                return comment;
        }

        public String getType() {
                return type;
        }

        public String getDattime() {
                return dattime;
        }

        public void setDattime(String dattime) {
                this.dattime = dattime;
        }

        public String getLabel8() {
                return label8;
        }

        public String getLabel9() {
                return label9;
        }

        public String getLabel1() {
                return label1;
        }

        public String getLabel2() {
                return label2;
        }

        public String getLabel3() {
                return label3;
        }

        public String getLabel4() {
                return label4;
        }

        public String getLabel5() {
                return label5;
        }

        public String getLabel6() {
                return label6;
        }

        public String getLabel7() {
                return label7;
        }

        public String getTxt_id() {
                return txt_id;
        }


        public Item(String operator_name, String commission) {

                this.operator_name = operator_name;
                this.commission=commission;
        }

        public Item( String operator_name, String sale_amt, String profit_amt) {

                this.operator_name = operator_name;
                this.sale_amt=sale_amt;
                this.profit_amt=profit_amt;
        }

        public Item( String label1, String label2, String label3,String label4,String label5,String label6,int a) {
                this.label1=label1;
                this.label2=label2;
                this.label3=label3;
                this.label4=label4;
                this.label5=label5;
                this.label6=label6;
        }

        public String getLimitLeft() {
                return limitLeft;
        }

        public Item(String trans_no, String trans_type, String trans_amount, String trans_datetime, String trans_update_balance, String status) {
                this.trans_no=trans_no;
                this.trans_type=trans_type;
                this.trans_amount=trans_amount;
                this.trans_datetime=trans_datetime;
                this.trans_update_balance=trans_update_balance;
                this.status=status;
        }

        public Item(String trans_no, String trans_type,String trans_amount,String trans_datetime,String trans_update_balance,String status,String p_name) {
                this.trans_no=trans_no;
                this.trans_type=trans_type;
                this.trans_amount=trans_amount;
                this.trans_datetime=trans_datetime;
                this.trans_update_balance=trans_update_balance;
                this.status=status;
                this.p_name=p_name;
        }

        public Item(String trans_no, String trans_type,String trans_amount,String trans_datetime,String trans_update_balance,String status,String p_name,String acno) {
                this.trans_no=trans_no;
                this.trans_type=trans_type;
                this.trans_amount=trans_amount;
                this.trans_datetime=trans_datetime;
                this.trans_update_balance=trans_update_balance;
                this.status=status;
                this.p_name=p_name;
                this.acno=acno;
        }

        public String getAcno() {
                return acno;
        }

        public String getCommission() {
                return commission;
        }

        public String getOperator_name() {
                return operator_name;
        }

        public String getStatus() {
                return status;
        }

        public int getImageId() {
                return imageId;
        }

        public String getTrans_amount() {
                return trans_amount;
        }

        public String getTrans_datetime() {
                return trans_datetime;
        }

        public String getTrans_no() {
                return trans_no;
        }

        public String getTrans_type() {
                return trans_type;
        }

        public String getTrans_update_balance() {
                return trans_update_balance;
        }

        public String getTxt() {
                return txt;
        }

        public String getProfit_amt() {
                return profit_amt;
        }

        public String getSale_amt() {
                return sale_amt;
        }

        public String getP_name() {
                return p_name;
        }


}
