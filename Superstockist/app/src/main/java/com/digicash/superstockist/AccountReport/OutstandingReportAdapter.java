package com.digicash.superstockist.AccountReport;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

public class OutstandingReportAdapter extends RecyclerView.Adapter<OutstandingReportAdapter.MyViewHolder> {

    private List<Item> outstandingList;
    Context ctx;
    //Typeface font ;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_id,txtMode,txtBalance,txtDate,txtBank;

        public MyViewHolder(View view) {
            super(view);
            txt_id = (TextView) view.findViewById(R.id.txt_id);
            txtMode = (TextView) view.findViewById(R.id.txtMode);
            txtBalance = (TextView) view.findViewById(R.id.txtBalance);
            txtDate = (TextView) view.findViewById(R.id.txtDate);
            txtBank = (TextView) view.findViewById(R.id.txtBank);
        }
    }

    public OutstandingReportAdapter(List<Item> outstandingList, Context ctx) {
        this.outstandingList = outstandingList;
        this.ctx = ctx;
    }

    @Override
    public OutstandingReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.distributor_payment_report_layout, parent, false);
        return new OutstandingReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OutstandingReportAdapter.MyViewHolder holder, int position) {
        Item movie = outstandingList.get(position);
        holder.txt_id.setText(movie.getType());
        holder.txtMode.setText(movie.getAmt());
        holder.txtBalance.setText(movie.getBalance());
        holder.txtDate.setText(movie.getDebit_credit_type());
        holder.txtBank.setText(movie.getDattime());
        //holder.txt_time.setText(DefineData.parseDateToddMMyyyyhh(movie.getLabel7()));
    }

    @Override
    public int getItemCount() {
        return outstandingList.size();
    }
}
