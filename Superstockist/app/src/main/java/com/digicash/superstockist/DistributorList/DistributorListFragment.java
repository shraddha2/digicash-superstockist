package com.digicash.superstockist.DistributorList;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Display Retailer List, Allows to  add balance to the retailers
  Mandatory Fields: Start and End Date*/

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Model.DividerItemDecoration;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DistributorListFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private DistributorAdapter mAdapter;
    String token;
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg;
    ConstraintLayout progress_linear,linear_container,rel_no_records,rel_no_internet;
    SwipeRefreshLayout mSwipeRefreshLayout;

    // Typeface font ;
    public DistributorListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_distributor_list, container, false);
        //String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle("DijiCash");
        setHasOptionsMenu(true);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_recharge_history);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);
        mSwipeRefreshLayout= (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        txt_title.setText("Distributor List");

        mAdapter = new DistributorAdapter(movieList,getActivity(),"Distributor List");
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchComplaints().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {


                refreshItems();
            }
        });

        return rootView;
    }

    void refreshItems() {
        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchComplaints().execute();

        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
    }

    private class FetchComplaints extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);


        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.FETCH_DISTRIBUTOR,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = response.getJSONArray("data");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    //Getting json
                                    json2 = jsonArray.getJSONObject(i);

                                    String id = json2.getString("id");
                                    // String admin_response = json2.getString("admin_response");
                                    String name = json2.getString("name");
                                   //String address = json2.getString("address");
                                    String phone = json2.getString("phone");
                                    String balance = json2.getString("balance");
                                    String business_name = json2.getString("business_name");
                                    String last_balance_refill = json2.getString("last_balance_refill");
                                    //superHero = new Item(recharge_no,admin_response,solved_date,complain_date,status,retailer_comment,0);
                                    superHero = new Item(id,business_name, "","Mobile No: "+ phone,"Last Balance Refill: "+ last_balance_refill, balance,0);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setText("Error in parsing");
                                    linear_container.setVisibility(View.GONE);
                                    rel_no_records.setVisibility(View.VISIBLE);
                                    progress_linear.setVisibility(View.GONE);

                                }

                                movieList.add(superHero);
                            }
                            mAdapter.notifyDataSetChanged();
                            linear_container.setVisibility(View.VISIBLE);
                            rel_no_records.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }else{
                            txt_err_msgg.setText("No Records Found!");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Error in parsing");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                txt_err_msgg.setText("Empty Server response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.retailer_list_menu, menu);
        Drawable drawable2 = menu.findItem(R.id.action_search).getIcon();
        drawable2 = DrawableCompat.wrap(drawable2);
        DrawableCompat.setTint(drawable2, ContextCompat.getColor(getActivity(),android.R.color.white));

        /*Drawable drawable = menu.findItem(R.id.action_add).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(getActivity(),android.R.color.white));*/

        MenuItem search = menu.findItem(R.id.action_search);
        //MenuItem add = menu.findItem(R.id.action_add);

        /*add.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                Intent i=new Intent(getActivity(),AddRetailerActivity.class);
                startActivity(i);

                return true;
            }
        });*/

        SearchView searchView = (SearchView) search.getActionView();
        search(searchView);


        super.onCreateOptionsMenu(menu, inflater);
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                break;
        }

        return false;
    }

    @Override
    public void onResume() {

        super.onResume();
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
}
