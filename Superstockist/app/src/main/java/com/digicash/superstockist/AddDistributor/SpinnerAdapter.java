package com.digicash.superstockist.AddDistributor;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<String>{

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<Item> items;
    private final int mResource;

    public SpinnerAdapter(@NonNull Context context, @LayoutRes int resource,
                          @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }



    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView offTypeTv = (TextView) view.findViewById(R.id.txt_operator_name);
        TextView numOffersTv = (TextView) view.findViewById(R.id.txt_id);
        //TextView maxDiscTV = (TextView) view.findViewById(R.id.max_discount_txt);

        Item offerData = items.get(position);

        offTypeTv.setText(offerData.getOperator_name());
        numOffersTv.setText(offerData.getCommission());
        //maxDiscTV.setText(offerData.getMaxDicount());

        return view;
    }
}
