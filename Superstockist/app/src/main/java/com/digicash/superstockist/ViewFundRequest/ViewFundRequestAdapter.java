package com.digicash.superstockist.ViewFundRequest;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by shraddha on 17-04-2018.
 */

public class ViewFundRequestAdapter extends RecyclerView.Adapter<ViewFundRequestAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String frg_name="", id="",name="",balance="";
    SharedPreferences sharedpreferences;
    String token;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_label5,txtTransactionRefId,txtRemark,txt_label6;
        Button txt_accept,txt_reject;

        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txtTransactionRefId = (TextView) view.findViewById(R.id.txtTransactionRefId);
            txtRemark = (TextView) view.findViewById(R.id.txtRemark);
            txt_label6 = (TextView) view.findViewById(R.id.txt_label6);
            txt_accept = (Button) view.findViewById(R.id.txt_accept);
            txt_reject = (Button) view.findViewById(R.id.txt_reject);

            //txt_accept.setVisibility(View.GONE);
            txt_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    id=txt_label1.getText().toString();
                    name=txt_label2.getText().toString();
                    balance=txt_label3.getText().toString();
                    Bundle bundle=new Bundle();
                    bundle.putString("id",id);
                    bundle.putString("name",name);
                    bundle.putString("balance",balance);
                    Fragment frg=new AcceptFundFragment();
                    frg.setArguments(bundle);
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
            });


            txt_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    id=txt_label1.getText().toString();

                    Bundle bundle=new Bundle();
                    bundle.putString("id",id);
                    Fragment frg=new RejectFundsFragment();
                    frg.setArguments(bundle);
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
            });

        }
    }


    public ViewFundRequestAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
        sharedpreferences = ctx.getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_fund_request_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_label1.setText(movie.getTrans_no()+"");
        holder.txt_label2.setText(movie.getTrans_type()+"");
        holder.txt_label3.setText(movie.getTrans_amount());
        holder.txt_label4.setText("Payment Mode: "+movie.getTrans_datetime()+"");
        holder.txt_label5.setText("Bank: "+movie.getTrans_update_balance()+"");
        holder.txtTransactionRefId.setText(movie.getStatus()+"");
        holder.txtRemark.setText(movie.getP_name()+"");
        holder.txt_label6.setText(movie.getAcno()+"");

            /*if (movie.getLabel3().equalsIgnoreCase("declined")) {
                holder.txt_label3.setTextColor(ContextCompat.getColor(Scom5GDistributor.getInstance(),R.color.status_fail));
            } else if (movie.getLabel3().equalsIgnoreCase("pending")){
                holder.txt_label3.setTextColor(ContextCompat.getColor(Scom5GDistributor.getInstance(),R.color.status_process));
                holder.txt_accept.setVisibility(View.VISIBLE);
            }else{
                holder.txt_label3.setTextColor(ContextCompat.getColor(Scom5GDistributor.getInstance(),R.color.status_success));
            }*/
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private class AcceptFunds extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("fundRequestId", id);
                this.response = new JSONObject(service.POST(DefineData.ACCEPT_FUNDS_REQUEST,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            String msg="";
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        msg=response.getString("message");
                    } else {
                        msg=response.getString("message");
                    }
                    Toast.makeText(ctx,msg+"", Toast.LENGTH_LONG).show();
                    Intent i=new Intent(ctx, HomeActivity.class);
                    ctx.startActivity(i);
                    ((Activity)ctx).finish();

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }else{

            }

        }
    }
}
