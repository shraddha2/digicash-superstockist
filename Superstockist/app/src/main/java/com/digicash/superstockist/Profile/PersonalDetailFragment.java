package com.digicash.superstockist.Profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class PersonalDetailFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edt_first_name,edt_last_name,edt_email,edt_phone,edt_address,edt_business_name;
    String first_name,last_name,email,phone,address,business_name;
    Button btn_submit;
    String token="";
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_network_msg,txt_error;
    ConstraintLayout progress_linear,linear_container,rel_no_internet;
    public PersonalDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_personal_detail, container, false);
        //String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle("DijiCash");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);
        edt_first_name= (EditText) rootView.findViewById(R.id.edt_first_name);
        edt_last_name= (EditText) rootView.findViewById(R.id.edt_last_name);
        edt_business_name= (EditText) rootView.findViewById(R.id.edt_business_name);
        edt_email= (EditText) rootView.findViewById(R.id.edt_email);
        edt_phone= (EditText) rootView.findViewById(R.id.edt_phone);
        edt_address= (EditText) rootView.findViewById(R.id.edt_address);

        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        edt_phone.setInputType(InputType.TYPE_NULL);

        if(checkConnection()) {
            linear_container.setVisibility(View.VISIBLE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchPersonalDetails().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                first_name=edt_first_name.getText().toString();
                last_name=edt_last_name.getText().toString();
                business_name=edt_business_name.getText().toString();
                email=edt_email.getText().toString();
                phone=edt_phone.getText().toString();
                address=edt_address.getText().toString();
                boolean isError=false;
                if(null==first_name||first_name.length()==0||first_name.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_first_name.setError("Field Cannot be Blank");
                }
                if(null==last_name||last_name.length()==0||last_name.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_last_name.setError("Field Cannot be Blank");
                }
                if(null==business_name||business_name.length()==0||business_name.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_business_name.setError("Field Cannot be Blank");
                }
                if(email.isEmpty()||!DefineData.isValidEmail(email))
                {
                    isError=true;
                    edt_email.setError("Field Cannot be Blank");
                }
                if(null==phone||phone.length()==0||phone.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_phone.setError("Field Cannot be Blank");
                }
                if(null==address||address.length()==0||address.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_address.setError("Field Cannot be Blank");
                }
                if(!isError) {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new EditPersonalDetails().execute();
                    }else{
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        return rootView;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


    }

    private class FetchPersonalDetails extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);


        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.FETCH_PERSONAL_DETAILS,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONObject json = response.getJSONObject("data");
                        if(json.length()!=0) {

                            try {
                                String firstName = json.getString("firstName");
                                String lastName = json.getString("lastName");
                                String phone = json.getString("phone");
                                String address = json.getString("address");
                                String businessName = json.getString("businessName");
                                String email = json.getString("email");
                                edt_first_name.setText(firstName+"");
                                edt_last_name.setText(lastName+"");
                                edt_business_name.setText(businessName+"");
                                edt_email.setText(email+ "");
                                edt_phone.setText(phone+"");
                                edt_address.setText(address+"");

                                linear_container.setVisibility(View.VISIBLE);
                                progress_linear.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                linear_container.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);

                            }


                        }else{
                            linear_container.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    linear_container.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                linear_container.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    private class EditPersonalDetails extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("firstName", first_name);
                parameters.put("lastName", last_name);
                parameters.put("businessName", business_name);
                parameters.put("email", email);
                parameters.put("phone", phone);
                parameters.put("address", address);
                this.response = new JSONObject(service.POST(DefineData.EDIT_PERSONAL_DETAILS,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {


            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("data");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    } else {
                        String msg=response.getString("data");

                        //Toast.makeText(getActivity(),"Complain Added Successfully", Toast.LENGTH_LONG).show();
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();

                        android.app.Fragment frg=new ProfileFragment();
                        ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg)
                                .addToBackStack(null)
                                .commit();

                      /*  txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText("Complaint Send Successfully");*/

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing responser");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {

        super.onResume();
        edt_first_name.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_first_name.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_last_name.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_last_name.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_business_name.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_business_name.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_email.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_email.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_phone.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_phone.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_address.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_address.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new ProfileFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }
                return false;
            }
        });
    }
}
