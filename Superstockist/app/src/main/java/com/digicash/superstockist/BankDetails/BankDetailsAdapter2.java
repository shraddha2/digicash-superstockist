package com.digicash.superstockist.BankDetails;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

/**
 * Created by shraddha on 17-04-2018.
 */

public class BankDetailsAdapter2 extends RecyclerView.Adapter<BankDetailsAdapter2.MyViewHolder>{
    private List<Item> moviesList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_label5,txt_label6,txt_label7,txt_label8,txt_label9,txt_raise_cmp;
        ImageView img_edit;
        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txt_label8 = (TextView) view.findViewById(R.id.txt_label8);
            txt_label9 = (TextView) view.findViewById(R.id.txt_label9);
            img_edit = (ImageView) view.findViewById(R.id.img_edit);

            img_edit.setVisibility(View.GONE);

        }
    }

    public BankDetailsAdapter2(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bank_detailsadapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_label1.setText(movie.getTrans_no()+"");
        holder.txt_label2.setText(movie.getTrans_type()+"");
        holder.txt_label3.setText(movie.getTrans_amount()+"");
        holder.txt_label4.setText(movie.getTrans_datetime()+"");
        holder.txt_label5.setText(movie.getTrans_update_balance()+"");
        holder.txt_label8.setText(movie.getStatus()+"");
        holder.txt_label9.setText(movie.getP_name()+"");


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
