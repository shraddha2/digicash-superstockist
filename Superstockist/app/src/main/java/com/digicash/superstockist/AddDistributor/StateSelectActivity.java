package com.digicash.superstockist.AddDistributor;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.superstockist.Model.DividerItemDecoration;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.ArrayList;

public class StateSelectActivity extends AppCompatActivity {
    private ArrayList<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private OperatorCircleAdapter mAdapter;
    private FirebaseAnalytics mFirebaseAnalytics;
    String source="";
    TextView txt_error, txt_network_msg, txt_err_msgg;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_no_records,rel_no_internet;
    Button btn_refresh;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_select);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Select State");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txt_error= (TextView) findViewById(R.id.txt_error);
        recyclerView = (RecyclerView) findViewById(R.id.rc_retailers);

        txt_err_msgg= (TextView) findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) findViewById(R.id.loding);
        linear_container= (LinearLayout) findViewById(R.id.container);
        rel_no_records= (RelativeLayout) findViewById(R.id.rel_no_records);
        rel_no_internet= (RelativeLayout) findViewById(R.id.rel_no_internet);
        btn_refresh= (Button) findViewById(R.id.btn_refresh);
        mSwipeRefreshLayout= (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        source=getIntent().getStringExtra("source");

        movieList = fill_with_data2();
        mAdapter = new OperatorCircleAdapter(movieList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(StateSelectActivity.this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        progress_linear.setVisibility(View.GONE);
        linear_container.setVisibility(View.VISIBLE);
        rel_no_records.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);
        txt_network_msg.setVisibility(View.GONE);
        txt_err_msgg.setVisibility(View.GONE);

    }

    public ArrayList<Item> fill_with_data2() {

        ArrayList<Item> data = new ArrayList<>();
        data.add(new Item( "ANDAMAN AND NICOBAR ISLANDS","1"));
        data.add(new Item( "ANDHRA PRADESH","2"));
        data.add(new Item( "ARUNACHAL PRADESH","3"));
        data.add(new Item( "ASSAM","4"));
        data.add(new Item( "BIHAR","5"));
        data.add(new Item( "CHATTISGARH","6"));
        data.add(new Item( "CHANDIGARH ","7"));
        data.add(new Item( "DAMAN AND DIU","8"));
        data.add(new Item( "DELHI","9"));
        data.add(new Item( "DADRA AND NAGAR HAVELI","10"));
        data.add(new Item( "GOA","11"));
        data.add(new Item( "GUJARAT","12"));
        data.add(new Item( "HIMACHAL PRADESH","13"));
        data.add(new Item( "HARYANA","14"));
        data.add(new Item( "JAMMU AND KASHMIR","15"));
        data.add(new Item( "JHARKHAND","16"));
        data.add(new Item( "KERALA","17"));
        data.add(new Item( "KARNATAKA ","18"));
        data.add(new Item( "LAKSHADWEEP","19"));
        data.add(new Item( "MEGHALAYA","20"));
        data.add(new Item( "MAHARASHTRA","21"));
        data.add(new Item( "MANIPUR ","22"));
        data.add(new Item( "MADHYA PRADESH","23"));
        data.add(new Item( "MIZORAM","24"));
        data.add(new Item( "NAGALAND","25"));
        data.add(new Item( "ORISSA ","26"));
        data.add(new Item( "PUNJAB","27"));
        data.add(new Item( "PONDICHERRY","28"));
        data.add(new Item( "RAJASTHAN","29"));
        data.add(new Item( "SIKKIM ","30"));
        data.add(new Item( "TAMIL NADU ","31"));
        data.add(new Item( "TRIPURA","32"));
        data.add(new Item( "UTTARAKHAND","33"));
        data.add(new Item( "UTTAR PRADESH","34"));
        data.add(new Item( "WEST BENGAL ","35"));
        return data;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        Drawable drawable = menu.findItem(R.id.search).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(this,android.R.color.white));

        MenuItem search = menu.findItem(R.id.search);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent();
        setResult(5,intent);
        finish();//finishing activity

    }

}
