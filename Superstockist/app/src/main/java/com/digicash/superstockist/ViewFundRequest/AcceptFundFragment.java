package com.digicash.superstockist.ViewFundRequest;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.DijicashSuperStockists;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AcceptFundFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edit_remark;
    TextView txt_retname,txt_balance,txt_label,txt_title,txt_error,txt_network_msg;
    String amount,remark;
    Button btn_submit;
    String token="", trans_type="";
    String id="", name="",balance="";
    SharedPreferences sharedpreferences;
    ConstraintLayout progress_linear,linear_container,rel_no_internet;
    RadioGroup radioGroup;
    RadioButton rb_rec,rb_credit;

    public AcceptFundFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_accept_fund, container, false);
        Bundle bundle = getArguments();
        getActivity().setTitle("DijiCash");

        id=bundle.getString("id");
        name=bundle.getString("name");
        balance = bundle.getString("balance");
        Log.d("result123",id+" "+name+" "+balance+" ");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_retname= (TextView) rootView.findViewById(R.id.txt_retname);
        txt_balance= (TextView) rootView.findViewById(R.id.txt_balance);
        txt_label= (TextView) rootView.findViewById(R.id.txt_label);
        radioGroup=(RadioGroup) rootView.findViewById(R.id.radioGroup);
        rb_rec=(RadioButton) rootView.findViewById(R.id.rb_rec);
        rb_credit=(RadioButton) rootView.findViewById(R.id.rb_credit);
        edit_remark= (EditText) rootView.findViewById(R.id.edit_remark);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);
        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        linear_container.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        txt_retname.setText(name + "");
        txt_balance.setText("Fund Requested: " +  balance + "");

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remark=edit_remark.getText().toString();
                int  selectedValueId = radioGroup.getCheckedRadioButtonId();
                if(selectedValueId == rb_rec.getId())
                {
                    trans_type="false";
                }
                else if(selectedValueId == rb_credit.getId())
                {
                    trans_type="true";
                }else{
                    trans_type="";
                }
                if (checkConnection()) {
                    boolean isError=false;
                    if(null==trans_type||trans_type.length()==0||trans_type.equalsIgnoreCase(""))
                    {
                        isError=true;
                        rb_credit.setError("Field Cannot be Blank");
                    }
                    if(null==remark||remark.length()==0||remark.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edit_remark.setError("Field Cannot be Blank");
                    }
                    if(!isError) {
                        new AcceptFunds().execute();
                    }
                }
                else{
                    linear_container.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.VISIBLE);
                }
            }
        });
        return rootView;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private class AcceptFunds extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("fundRequestId", id);
                parameters.put("type", trans_type);
                parameters.put("remark", remark);
                this.response = new JSONObject(service.POST(DefineData.ACCEPT_FUNDS_REQUEST,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {

            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg=response.getString("message");
                        txt_error.setText(msg+"");
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        String msg=response.getString("message");
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        android.app.Fragment frg=new ViewFundRequestFragment();
                        ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg)
                                .addToBackStack(null)
                                .commit();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setText("Error in parsing responser");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    txt_error.setVisibility(View.VISIBLE);
                }
            }else{
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
                txt_error.setVisibility(View.VISIBLE);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {

        super.onResume();
        radioGroup.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    radioGroup.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edit_remark.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edit_remark.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new ViewFundRequestFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;
                }
                return false;
            }
        });
    }

}
