package com.digicash.superstockist.NavigationDrawer;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Home Drawer Layout which includes: Home,Add Retailer,Profile,Search Recharges, My Bank Details,Add Complain,All Fund Request,Change Password, Contact Us */

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.superstockist.DijicashSuperStockists;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.superstockist.AddComplain.AddComplainFragment;
import com.digicash.superstockist.AddDistributor.AddDistributorFragment;
import com.digicash.superstockist.BankDetails.BankDetailsActivity;
import com.digicash.superstockist.CommissionChart.CommissionChartFragment;
import com.digicash.superstockist.ComplainList.ComplaintListFragment;
import com.digicash.superstockist.Contact.ContactUsFragment;
import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.FundRequest.FundRequestActivity;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Login.GenerateOtpActivity;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Profile.ProfileFragment;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;
import com.digicash.superstockist.ChangePassword.ChangePasswordFragment;
import com.digicash.superstockist.ViewFundRequest.ViewFundRequestFragment;

import org.json.JSONException;
import org.json.JSONObject;

import static com.digicash.superstockist.DefineData.LOGIN_MINKSPAY_PREFERENCE;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,ConnectivityReceiver.ConnectivityReceiverListener {
    private Menu menu;
    String balance="";

    private FirebaseAnalytics mFirebaseAnalytics;

    private FragmentManager fragmentManager;
    private Fragment fragment;

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    boolean loggedin=false;
    String rt_name="", rt_phoneno="",current_balance="",token;
    TextView txt_rt_name,txt_rt_phone,  txt_w_balance;
    TextView txt_balance,txt_dijicash;

    NavigationView navigationView;
    BottomNavigationView bottomNavigationView;

    Boolean doubleBackToExitPressedOnce = false;
    CoordinatorLayout coordinateLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        //getActionBar().setCustomView(R.layout.custom_action_bar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(false);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)
        txt_balance = (TextView) toolbar.findViewById(R.id.txt_balance);
        txt_dijicash = (TextView) toolbar.findViewById(R.id.txt_scom);

        coordinateLayout= (CoordinatorLayout)findViewById(R.id.coordinateLayout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        sharedpreferences = getSharedPreferences(LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        loggedin=sharedpreferences.getBoolean(DefineData.KEY_LOGIN_SUCCESS,false);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        rt_name=sharedpreferences.getString(DefineData.KEY_DISTRIBUTOR_NAME,"Guest");
        rt_phoneno =sharedpreferences.getString(DefineData.KEY_DISTRIBUTOR_PHONENO,"PhoneNo");
        current_balance=sharedpreferences.getString(DefineData.KEY_WALLET_BALANCE,"");

        View headerLayout = navigationView.getHeaderView(0);

        txt_w_balance = (TextView) headerLayout.findViewById(R.id.txt_w_balance);
        txt_rt_name = (TextView) headerLayout.findViewById(R.id.txt_rt_name);
        txt_rt_phone = (TextView) headerLayout.findViewById(R.id.txt_rt_phone);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        new FetchBalance().execute();

        fragmentManager = getFragmentManager();
        if (savedInstanceState == null) {
            fragment = new HomeFragment();
            fragmentManager.beginTransaction().replace(R.id.frg_replace, fragment, "" + "Home").commit();
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                //inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                //inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        //txt_rt_name.setText("Welcome SuperStockist"+" "+rt_name+"");
        txt_rt_name.setText(rt_name+"");
        txt_rt_phone.setText("+91 " + rt_phoneno + "");

        if (bottomNavigationView != null) {
            // Set action to perform when any menu-item is selected.
            bottomNavigationView.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_home:
                                    // Action to perform when Home Menu item is selected.
                                    changeFrg(new HomeFragment(), "Home");
                                    new FetchBalance().execute();
                                    break;
                                case R.id.action_add_retailer:
                                    changeFrg(new AddDistributorFragment(), "Add Distributor");
                                    break;
                                case R.id.action_profile:
                                    changeFrg(new ProfileFragment(), "Profile");
                                    break;

                               /* case R.id.action_notification:
                                    changeFrg(new NotificationFragment(), "Notifications");
                                    break;*/
                            }
                            return true;
                        }
                    });
        }

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                Fragment fragment = getFragmentManager().findFragmentById(R.id.frg_replace);
                String tag = (String) fragment.getTag();

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(bottomNavigationView.getSelectedItemId() == R.id.action_home){
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();
                } else if (!doubleBackToExitPressedOnce) {
                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this,"Please click BACK again to exit.", Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
                } else {
                    super.onBackPressed();
                    finish();
                    return;
                }
            }else{
                bottomNavigationView.setSelectedItemId(R.id.action_home);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
    }

    public void changeFrg(Fragment fragment, String frgname) {
        fragmentManager.beginTransaction().replace(R.id.frg_replace, fragment, "" + frgname).addToBackStack(null).commit();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch(id)
        {
            case R.id.nav_home:
                changeFrg(new HomeFragment(),"Home");
                break;
            case R.id.nav_list:
                changeFrg(new ComplaintListFragment(),"Complaint List");
                break;
            case R.id.nav_commision_chart:
                changeFrg(new CommissionChartFragment(),"Commission Chart");
                break;
            case R.id.nav_funds_request:
                Intent intent2=new Intent(getApplicationContext(), FundRequestActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.nav_bank_detail:
                Intent intent3=new Intent(getApplicationContext(), BankDetailsActivity.class);
                startActivity(intent3);
                finish();
                break;
          /*  case R.id.nav_add_complain:
                changeFrg(new AddComplainFragment(),"Add Complain");
                break;*/
            case R.id.nav_fund_request:
                changeFrg(new ViewFundRequestFragment(),"Distributor Fund Request");
                break;
            case R.id.nav_change_password:
                changeFrg(new ChangePasswordFragment(),"Change Password");
                break;
            case R.id.nav_contact_us:
                changeFrg(new ContactUsFragment(),"Contact Us");
                break;
            case R.id.nav_logout:
                logout();
                sharedpreferences.getBoolean(DefineData.KEY_REMEMBER_PIN,false);
                Intent i=new Intent(getApplicationContext(), GenerateOtpActivity.class);
                startActivity(i);
                finish();
                break;
            default:
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public void logout() {
        SharedPreferences sharedPreferences = HomeActivity.this.getSharedPreferences(LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(!isConnected)
        {
            Snackbar mSnackBar = Snackbar.make(coordinateLayout, "Internet not available", Snackbar.LENGTH_INDEFINITE);
            View view = mSnackBar.getView();
            view.setBackgroundColor(Color.RED);
            TextView mainTextView = (TextView) (view).findViewById(android.support.design.R.id.snackbar_text);
            mainTextView.setTextColor(Color.WHITE);
            mSnackBar.show();
        }
    }

    private class FetchBalance extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.FETCH_BALANCE,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                //String balance="";
                try {
                    if (response.getBoolean("error")) {
                        balance="Balance : ₹ 0";
                    } else {
                        balance=response.getString("balance");
                    }
                    txt_balance.setText("₹ "+balance+"");
                    txt_w_balance.setText("Balance : ₹ "+balance+"");
                    editor.putString(DefineData.KEY_WALLET_BALANCE, balance);
                    editor.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
            }
        }
    }

    public void selectNavigationDrawerItem(int position){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if(null!=navigationView) {
            if (position == 0) {
                navigationView.setCheckedItem(R.id.nav_home);
            }
        }
    }

    public void updateWalletBalance(String balance){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if(null!=navigationView) {
            View headerLayout = navigationView.getHeaderView(0);
            txt_w_balance = (TextView) headerLayout.findViewById(R.id.txt_w_balance);
            txt_w_balance.setText("Balance : ₹ "+balance+"");
            txt_balance.setText("₹ "+balance+"");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
