package com.digicash.superstockist.OutstandingReport;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

public class OutstandingReportAdapter extends RecyclerView.Adapter<OutstandingReportAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    //Typeface font ;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_name, txt_paid,txt_taken,txt_id;
        Button txt_action,txt_details;

        public MyViewHolder(View view) {
            super(view);
            txt_name = (TextView) view.findViewById(R.id.txt_name);
            txt_paid = (TextView) view.findViewById(R.id.txt_paid);
            txt_taken= (TextView) view.findViewById(R.id.txt_taken);
            txt_action= (Button) view.findViewById(R.id.txt_action);
            txt_details= (Button) view.findViewById(R.id.txt_details);
            txt_id = (TextView) view.findViewById(R.id.txt_id);

            txt_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id=txt_id.getText().toString();
                    String balance=txt_taken.getText().toString();
                    String name=txt_name.getText().toString();
                    Bundle bundle=new Bundle();
                    bundle.putString("id",id);
                    bundle.putString("name",name);
                    bundle.putString("balance",balance);
                    Fragment frg=new CollectOutstandingFragment();
                    frg.setArguments(bundle);
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();

                }
            });

            txt_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id=txt_id.getText().toString();
                    String balance=txt_taken.getText().toString();
                    String name=txt_name.getText().toString();
                    Bundle bundle=new Bundle();
                    bundle.putString("id",id);
                    bundle.putString("name",name);
                    bundle.putString("balance",balance);
                    Fragment frg=new DetailsOutstandingFragment();
                    frg.setArguments(bundle);
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();

                }
            });

        }
    }


    public OutstandingReportAdapter(List<Item> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.outstanding_report_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);
        holder.txt_name.setText(movie.getLabel1());
        holder.txt_paid.setText(movie.getLabel2());
        holder.txt_taken.setText(movie.getLabel3());
        holder.txt_id.setText(movie.getDattime());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
