package com.digicash.superstockist.ViewFundRequest;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Display All Fund Request and can search the fund request based on the start and end dates*/

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.superstockist.DijicashSuperStockists;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.HomePage.HomeFragment;
import com.digicash.superstockist.Model.DividerItemDecoration;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.NavigationDrawer.HomeActivity;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ViewFundRequestFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ViewFundRequestAdapter mAdapter;
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg;
    String token;
    SharedPreferences sharedpreferences;
    ConstraintLayout progress_linear,linear_container,rel_img_bg,rel_no_records,rel_no_internet;
    private FirebaseAnalytics mFirebaseAnalytics;
    EditText edt_search_number;
    ImageView img_search;
    TextView txt_from_date,txt_to_date;
    DateFormat inputFormat,outputFormat;
    String start_date,end_date,query;
    Date from_date,to_date;
    ImageView btn_search;

    public ViewFundRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView= inflater.inflate(R.layout.fragment_view_fund_request, container, false);
        getActivity().setTitle("DijiCash");
        //setHasOptionsMenu(true);

        edt_search_number=(EditText)rootView.findViewById(R.id.edt_search_number);
        img_search=(ImageView)rootView.findViewById(R.id.img_search);
        txt_from_date = (TextView) rootView.findViewById(R.id.txt_from_date);
        txt_to_date = (TextView) rootView.findViewById(R.id.txt_to_date);
        btn_search= (ImageView) rootView.findViewById(R.id.btn_search);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijicashSuperStockists.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_recharge_history);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_img_bg= (ConstraintLayout) rootView.findViewById(R.id.rel_img_bg);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        outputFormat = new SimpleDateFormat("dd MMM yyyy");
        txt_title.setText("All Fund Request");

        txt_to_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        start_date=DefineData.getCurrentDate();
        end_date=start_date;

        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        rel_img_bg.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        txt_to_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        start_date=DefineData.getCurrentDate();
        end_date=start_date;

        mAdapter = new ViewFundRequestAdapter(movieList,getActivity(),"Search Name");
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_date=DefineData.parseDateToyyyMMdd(txt_from_date.getText().toString());
                end_date=DefineData.parseDateToyyyMMdd(txt_to_date.getText().toString());

                if(checkConnection()) {
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.GONE);
                    rel_img_bg.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.GONE);
                    new ViewFundRequest().execute(start_date,end_date);
                }else{
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.GONE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_internet.setVisibility(View.VISIBLE);
                }


            }
        });

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query=edt_search_number.getText().toString();
                boolean isError=false;

                if(null==query||query.length()==0||query.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_search_number.setError("Invalid Retailer Name");
                }


                if(!isError) {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new ViewFundRequest().execute(query);
                    }else{
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        txt_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),R.style.MyCalendarStyle,new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;

                        txt_from_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));


                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("Start Date");
                mDatePicker.show();  }


        });


        txt_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),R.style.MyCalendarStyle,new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;

                        txt_to_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));


                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("End Date");
                mDatePicker.show();  }


        });
        movieList.clear();
        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.VISIBLE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new ViewFundRequest().execute(query,start_date,end_date);
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

        return rootView;
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private class ViewFundRequest extends AsyncTask<String, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);


        }
        @Override
        protected Void doInBackground(String... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("startDate", start_date);
                parameters.put("endDate",  end_date);
                //parameters.put("retailerName", query);
                this.response = new JSONObject(service.POST(DefineData.VIEW_FUND_REQUEST,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = response.getJSONArray("data");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    //Getting json
                                    json2 = jsonArray.getJSONObject(i);
                                    String id = json2.getString("id");
                                    String distributor = json2.getString("distributor");
                                    String amount = json2.getString("amount");
                                    String paymentmode = json2.getString("paymentMethod");
                                    String bank = json2.getString("bank");
                                    String transactionRefId = json2.getString("transactionRefId");
                                    String remark = json2.getString("remark");
                                    String paymentDate = json2.getString("paymentDate");
                                    superHero = new Item(id, distributor,"₹ " + amount, paymentmode, bank,"Bank Ref Id: "+transactionRefId,"Remark: "+remark, paymentDate);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setText("Error in parsing response");
                                    linear_container.setVisibility(View.GONE);
                                    rel_no_records.setVisibility(View.VISIBLE);
                                    rel_img_bg.setVisibility(View.GONE);
                                    progress_linear.setVisibility(View.GONE);

                                }

                                movieList.add(superHero);
                            }
                            mAdapter.notifyDataSetChanged();
                            linear_container.setVisibility(View.VISIBLE);
                            rel_no_records.setVisibility(View.GONE);
                            rel_img_bg.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }else{
                            txt_err_msgg.setText("No Records Found!");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            rel_img_bg.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Error in parsing response");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                rel_img_bg.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onResume() {

        super.onResume();
        edt_search_number.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_search_number.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        DijicashSuperStockists.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }
    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
}