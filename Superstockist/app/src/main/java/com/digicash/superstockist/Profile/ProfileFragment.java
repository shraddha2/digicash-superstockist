package com.digicash.superstockist.Profile;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Display the Details of the user profile and Edit User Details */

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.Model.HTTPURLConnection;
import com.digicash.superstockist.R;
import com.digicash.superstockist.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener  {

    TextView txt_title,txt_label1,txt_label2,txt_label3,txt_label4, txt_label5;
    String token;
    SharedPreferences sharedpreferences;
    TextView txt_err_msgg,txt_network_msg,txtAadharCard,txtPanNumber;
    ConstraintLayout progress_linear,linear_container,rel_no_records,rel_no_internet;
    ImageView img_edit;
    Context ctx;

    public ProfileFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_profile, container, false);
        getActivity().setTitle("DijiCash");

        txt_label1 = (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2 = (TextView) rootView.findViewById(R.id.txt_label2);
        txt_label3 = (TextView) rootView.findViewById(R.id.txt_label3);
        txt_label4 = (TextView) rootView.findViewById(R.id.txt_label4);
        txt_label5 = (TextView) rootView.findViewById(R.id.txt_label5);
        img_edit=(ImageView) rootView.findViewById(R.id.img_edit);

        txt_title = (TextView) rootView.findViewById(R.id.txt_title);

        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        txtAadharCard = (TextView) rootView.findViewById(R.id.txtAadharCard);
        txtPanNumber = (TextView) rootView.findViewById(R.id.txtPanNumber);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        ctx=getActivity();

        img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment frg=new PersonalDetailFragment();
                ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                        .replace(R.id.frg_replace, frg)
                        .addToBackStack(null)
                        .commit();
            }
        });

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchProfileDetails().execute();
            new FetchKYCDetails().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }


        return rootView;
    }



    private class FetchProfileDetails extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);


        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.FETCH_PROFILE,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONObject json = response.getJSONObject("data");
                        if(json.length()!=0) {

                            try {
                                String contact_person = json.getString("contactPerson");
                                String mobileno = json.getString("mobile");
                                String address = json.getString("address");
                                String business_name = json.getString("business_name");
                                String email = json.getString("email");
                                txt_label1.setText(contact_person+"");
                                txt_label2.setText("+91 "+mobileno+"");
                                txt_label3.setText(email+"");
                                txt_label4.setText(business_name+ "");
                                txt_label5.setText(address+"");

                                linear_container.setVisibility(View.VISIBLE);
                                rel_no_records.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                txt_err_msgg.setText("Error in parsing response");
                                linear_container.setVisibility(View.GONE);
                                rel_no_records.setVisibility(View.VISIBLE);
                                progress_linear.setVisibility(View.GONE);

                            }


                        }else{
                            txt_err_msgg.setText("No Records Found!");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Error in parsing response");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    private class FetchKYCDetails extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);


        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.FETCH_KYC_DETAILS,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {

            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONObject json = response.getJSONObject("data");
                        if(json.length()!=0) {

                            try {
                                String aadharNumber = json.getString("aadharNumber");
                                String panNumber = json.getString("panNumber");
                                txtAadharCard.setText(aadharNumber+"");
                                txtPanNumber.setText(panNumber+"");

                                linear_container.setVisibility(View.VISIBLE);
                                progress_linear.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                linear_container.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);

                            }


                        }else{
                            linear_container.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    linear_container.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                linear_container.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    public void EditProfileDetail() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(),R.style.DialogTheme);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View dialogView = inflater.inflate(R.layout.cust_personal_category_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("Which details you want to update ? ");
        final LinearLayout linear_personal = (LinearLayout) dialogView.findViewById(R.id.linear_personal);
        final LinearLayout linear_kyc = (LinearLayout) dialogView.findViewById(R.id.linear_kyc);
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        final AlertDialog can = dialogBuilder.create();

        linear_personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                can.dismiss();
                android.app.Fragment frg=new PersonalDetailFragment();
                ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                        .replace(R.id.frg_replace, frg)
                        .addToBackStack(null)
                        .commit();
            }
        });

        linear_kyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                can.dismiss();
                android.app.Fragment frg=new KycFragment();
                ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                        .replace(R.id.frg_replace, frg)
                        .addToBackStack(null)
                        .commit();

            }
        });
        can.show();
    }

    @Override
    public void onResume() {

        super.onResume();

    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
