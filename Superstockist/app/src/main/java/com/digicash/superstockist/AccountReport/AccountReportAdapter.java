package com.digicash.superstockist.AccountReport;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.superstockist.DefineData;
import com.digicash.superstockist.Model.Item;
import com.digicash.superstockist.R;

import java.util.List;

public class AccountReportAdapter extends RecyclerView.Adapter<AccountReportAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_type,txt_mode,txt_remark,txt_time,txt_credit,txt_debit,txt_balance;

        public MyViewHolder(View view) {
            super(view);
            txt_type = (TextView) view.findViewById(R.id.txt_type);
            txt_mode = (TextView) view.findViewById(R.id.txt_mode);
            txt_remark = (TextView) view.findViewById(R.id.txt_remark);
            txt_credit = (TextView) view.findViewById(R.id.txt_credit);
            txt_debit = (TextView) view.findViewById(R.id.txt_debit);
            txt_time = (TextView) view.findViewById(R.id.txt_time);
            txt_balance= (TextView) view.findViewById(R.id.txt_balance);
        }
    }

    public AccountReportAdapter(List<Item> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.account_report_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);
        holder.txt_type.setText(movie.getTrans_no());
        holder.txt_balance.setText(movie.getTrans_type());
        String type=movie.getTrans_datetime();
        if(type.equalsIgnoreCase("debit"))
        {
            /* holder.txt_debit.setText(movie.getLabel6());
            holder.txt_credit.setText("-");*/
            holder.txt_debit.setText(movie.getTrans_amount());
            holder.txt_credit.setText("-");

        }else if(type.equalsIgnoreCase("credit"))
        {
            holder.txt_credit.setText(movie.getTrans_amount());
            holder.txt_debit.setText("-");
        }else{
        }
        holder.txt_time.setText(movie.getTrans_update_balance());
        holder.txt_mode.setText(movie.getStatus());
        holder.txt_remark.setText(movie.getP_name());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
