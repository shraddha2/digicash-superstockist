package com.digicash.superstockist;

import android.app.Application;

import com.digicash.superstockist.Receiver.ConnectivityReceiver;
import com.google.firebase.analytics.FirebaseAnalytics;

public class DijicashSuperStockists extends Application {

    private static DijicashSuperStockists mInstance;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public static synchronized DijicashSuperStockists getInstance() {
        return mInstance;

    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

}

